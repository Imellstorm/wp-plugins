<?php
/*
Plugin Name: GC Booking form
Description: Adds booking form 
Author: Alex Karev
*/

/**
 * Include CSS and JS files for Booking Plugin.
 */
function booking_js_css() {
	wp_enqueue_style( 'bookin_styles',  plugin_dir_url( __FILE__ ) . 'includes/css/booking_styles.css?'.time() );
	wp_enqueue_style( 'bootstrap_styles',  plugin_dir_url( __FILE__ ) . 'includes/css/bootstrap.css' );
	wp_enqueue_style( 'boots_styles_datapicker',  plugin_dir_url( __FILE__ ) . 'includes/css/jquery_datetimepicker.css' );

	wp_enqueue_script( 'stripe_js', 'https://js.stripe.com/v2/', null, null, false);
	wp_enqueue_script( 'bootstrap_js', plugin_dir_url( __FILE__ ) . 'includes/js/bootstrap.js',  array( 'jquery' ), null, true);
	wp_enqueue_script( 'custom_js', plugin_dir_url( __FILE__ ) . 'includes/js/booking_general.js',  array( 'jquery' ), null, true);
	wp_enqueue_script( 'js_datapicker', plugin_dir_url( __FILE__ ) . 'includes/js/jquery_datetimepicker_full.js',  array( 'jquery' ), null, false);
	wp_enqueue_script( 'validation_jquery', plugin_dir_url( __FILE__ ) . 'includes/js/jquery.validate.min.js',  array( 'jquery' ), null, false);
}
add_action( 'wp_enqueue_scripts', 'booking_js_css' );


// Create shortcode to summon ooking form
function show_booking_form(){
	get_template_part('../../wp-content/plugins/booking_form/includes/view/form');
}
add_shortcode( 'booking_form', 'show_booking_form' );


function create_pt_order_form() {
	register_post_type( 'order_form', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		array('labels' => array(
			'name' => __('Orders'), /* This is the Title of the Group */
			'singular_name' => __('Order Form'), /* This is the individual type */
			'all_items' => __('All order forms'), /* the all items menu item */
			'add_new' => __('Add New order form'), /* The add new menu item */
			'add_new_item' => __('Add New order form'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit order form'), /* Edit Display Title */
			'new_item' => __('New order form'), /* New Display Title */
			'view_item' => __('View order form'), /* View Display Title */
			'search_items' => __('Search order form'), /* Search Product Title */
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Nothing found in Trash'),  /*This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			),
			'description' => __( 'order form for '.get_bloginfo() ), /* Product Description */
			'public' => false,
			'menu_icon' => 'dashicons-cart',
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'order form', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title','post-options','page-attributes', 'thumbnail', 'author',),
			'taxonomies' => array( 'post_tag' ),
		) /* end of options */
	); /* end of register post type */
}
add_action( 'init','create_pt_order_form' );

function order_form_meta_boxs(){
	add_meta_box( 'order_form_meta_box',
		'Order Form settings',
		'display_order_form',
		'order_form',
		'normal',
		'high'
		);
}
add_action( 'add_meta_boxes','order_form_meta_boxs' );


function display_order_form(){
	global $post;
	$client_name = get_post_meta($post->ID, 'client_name', true);
	$client_email = get_post_meta($post->ID, 'client_email', true);
	$client_adress = get_post_meta($post->ID, 'client_adress', true);
	$client_phone = get_post_meta($post->ID, 'client_phone', true);
	$client_zip = get_post_meta($post->ID, 'client_zip', true);
	$appointment_date = get_post_meta($post->ID, 'appointment_date', true);
	$gutter_length = get_post_meta($post->ID, 'gutter_length', true);
	$gutter_gurads = get_post_meta($post->ID, 'gutter_gurads', true);
	$building_height = get_post_meta($post->ID, 'building_height', true);
	$general_order_price_hidden = get_post_meta($post->ID, 'general_order_price_hidden', true);
	$stripe_token = get_post_meta($post->ID, 'stripe_token', true);
	?>
	<style type="text/css">
		.order_details th label{
			padding-left: 10px;
		}
	</style>
	<table class="form-table striped order_details">
		<tr>
			<th><label>Name</label></th>
			<td>
				<span><?php echo $client_name ?></span>
			</td>
		</tr>
		<tr>
			<th><label>Email</label></th>
			<td>
				<span><?php echo $client_email ?></span>
			</td>
		</tr>
		<tr>
			<th><label>Adress</label></th>
			<td>
				<span><?php echo $client_adress ?></span>
			</td>
		</tr>
		<tr>
			<th><label>Phone</label></th>
			<td>
				<span><?php echo $client_phone ?></span>
			</td>
		</tr>
		<tr>
			<th><label>ZIP</label></th>
			<td>
				<span><?php echo $client_zip ?></span>
			</td>
		</tr>
		<tr>
			<th><label>Date of Appointment</label></th>
			<td>
				<span><?php echo $appointment_date ?></span>
			</td>
		</tr>
		<tr>
			<th><label>Gutter Length</label></th>
			<td>
				<span><?php echo $gutter_length ?></span>
			</td>
		</tr>
		<tr>
			<th><label>Gutter Guards Instaleld?</label></th>
			<td>
				<span><?php echo $gutter_gurads ?></span>
			</td>
		</tr>
		<tr>
			<th><label>Building Height</label></th>
			<td>
				<span><?php echo $building_height ?></span>
			</td>
		</tr>
		<tr>
			<th><label>Total Order Price</label></th>
			<td>
				<span><?php echo $general_order_price_hidden ?>$</span>
			</td>
		</tr>
	</table>
	<input type="hidden" value="<?php echo $post->ID?>" name="post_id">
	<?php if($stripe_token!='paid'): ?>
		<input type="submit" value="Stripe charge" name="stripe_charge_submit" class="button button-primary button-large" style="margin-top: 20px;">
	<?php else: ?>
		<h2 style="color:green">Charged!</h2>
	<?php endif ?>
<?php
}


function save_order_form( $id, $item ) {
	if( $item->post_type == 'order_form' ){
		if(isset($_POST['client_name'])) {
			update_post_meta( $id, 'client_name',$_POST['client_name'] );
		}
		if(isset($_POST['client_email'])) {
			update_post_meta( $id, 'client_email',$_POST['client_email'] );
		}
		if(isset($_POST['client_adress'])) {
			update_post_meta( $id, 'client_adress',$_POST['client_adress'] );
		}
		if(isset($_POST['client_phone'])) {
			update_post_meta( $id, 'client_phone',$_POST['client_phone'] );
		}
		if(isset($_POST['client_zip'])) {
			update_post_meta( $id, 'client_zip',$_POST['client_zip'] );
		}
		if(isset($_POST['appointment_date'])) {
			update_post_meta( $id, 'appointment_date',$_POST['appointment_date'] );
		}
		if(isset($_POST['gutter_length'])) {
			update_post_meta( $id, 'gutter_length',$_POST['gutter_length'] );
		}
		if(isset($_POST['gutter_gurads'])) {
			update_post_meta( $id, 'gutter_gurads',$_POST['gutter_gurads'] );
		}
		if(isset($_POST['building_height'])) {
			update_post_meta( $id, 'building_height',$_POST['building_height'] );
		}
		if(isset($_POST['general_order_price_hidden'])) {
			update_post_meta( $id, 'general_order_price_hidden',$_POST['general_order_price_hidden'] );
		}
		if(isset($_POST['stripeToken'])) {
			update_post_meta( $id, 'stripe_token',$_POST['stripeToken'] );
		}
	}
}
add_action( 'save_post','save_order_form',10,2 );


// Insert the post into the database

function save_form(){
	if(isset($_POST['form_order_id']) && $_POST['form_order_id']=='order_form'){
		// Autosave, do nothing
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return;
		// AJAX? Not used here
		if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) 
			return;

		$post_arr = array(
			'post_type'  =>  'order_form',
			'post_status'   => 'publish',
			'post_title'    =>  wp_strip_all_tags ( 'Order from '.$_POST['client_name'] ),
		);
		wp_insert_post( $post_arr );
		wp_mail($_POST['client_email'],'Order info','Some text');
		wp_redirect( '/thank-you' );
		exit;
	}
}
add_action( 'init','save_form' );

// Stripe charge

function stripe_charge(){
	if(isset($_POST['stripe_charge_submit']) && $_POST['stripe_charge_submit']=='Stripe charge' && is_admin()){
		require dirname(__FILE__) . "/includes/stripe/init.php";
		$post_id = $_POST['post_id'];

		$client_name = get_post_meta($post_id, 'client_name', true);
		$client_email = get_post_meta($post_id, 'client_email', true);
		$client_adress = get_post_meta($post_id, 'client_adress', true);
		$client_phone = get_post_meta($post_id, 'client_phone', true);
		$client_zip = get_post_meta($post_id, 'client_zip', true);
		$appointment_date = get_post_meta($post_id, 'appointment_date', true);
		$gutter_length = get_post_meta($post_id, 'gutter_length', true);
		$gutter_gurads = get_post_meta($post_id, 'gutter_gurads', true);
		$building_height = get_post_meta($post_id, 'building_height', true);
		$price = get_post_meta($post_id, 'general_order_price_hidden', true);
		$token = get_post_meta($post_id, 'stripe_token', true);

		$description = 'Name: '.$client_name.', Email: '.$client_email.', Adress: '.$client_adress.', Phone: '.$client_phone.', Zip: '.$client_zip.', Apointment date: '.$appointment_date.', Gutter length: '.$gutter_length.', Gutter gurads: '.$gutter_gurads.', Building height '.$building_height;

		// Set your secret key: remember to change this to your live secret key in production
		// See your keys here https://dashboard.stripe.com/account/apikeys	
		\Stripe\Stripe::setApiKey(get_site_option('stripe_secret_key'));

		// Create the charge on Stripe's servers - this will charge the user's card
		try {
		  $charge = \Stripe\Charge::create(array(
		    "amount" => $price*100, // amount in cents, again
		    "currency" => "usd",
		    "source" => $token,
		    "description" => $description
		    ));
		  	update_post_meta( $post_id, 'stripe_token','paid' );
		} catch(\Stripe\Error\Card $e) {
		  	var_dump($e); exit;
		}
	}
}
add_action( 'init','stripe_charge' );


function add_new_columns( $columns ) {
	global $post;
	return array_merge($columns,
		array(
			'charged' => __('Charged'),
		)
	);
}
add_filter( 'manage_order_form_posts_columns', 'add_new_columns');

function custom_order_form_column( $column, $post_id ) {
	switch ( $column ) {
		case 'charged':
		$token = get_post_meta( $post_id , 'stripe_token' , true );
		echo $token=='paid'?'Yes':'No';
		break;
	}
}
add_action( 'manage_order_form_posts_custom_column' , 'custom_order_form_column', 10, 2 );

//Menu settings (Stripe credentials)
function order_form_menu() {
	add_options_page( 'Order form settings', 'Stripe credentials', 'manage_options', 'order-form-stripe-credentials', 'order_form_options' );
}
function order_form_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	$publishable_key = get_site_option('stripe_publishable_key');
	$secret_key 	 = get_site_option('stripe_secret_key');
	?>
	<div class="wrap">
		<h1>Stripe credentials</h1>
		<form method="POST">
			<table class="form-table">
				<tbody>
					<tr>
						<th><label>Secret key</label></th>
						<td><input type="text" name="stripe_secret_key" class="regular-text" value="<?php echo $secret_key ?>"></td>
					</tr>
					<tr>
						<th><label>Publishable key</label></th>
						<td><input type="text" name="stripe_publishable_key" class="regular-text" value="<?php echo $publishable_key ?>"></td>
					</tr>					
				</tbody>
			</table>
			<p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>
		</form>
	</div>
	<?php
}
add_action( 'admin_menu', 'order_form_menu' );

function save_stripe_credentials(){
	if(is_admin()){
		if(isset($_POST['stripe_publishable_key'])){
			update_option( 'stripe_publishable_key', $_POST['stripe_publishable_key'] );
		}
		if(isset($_POST['stripe_secret_key'])){
			update_option( 'stripe_secret_key', $_POST['stripe_secret_key'] );
		}
	}
}
add_action('init','save_stripe_credentials');

