jQuery(document).ready(function($) {

	//Only Numbers
	$(document).on('keydown','input[type="number"]',function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

	var gutter_length_price = 125
	var gutter_installed_price = 0
	var building_height = 0

	function update_price(price_value, old_value){
		var general_price = parseInt($('#general_order_price').text())
		var reveted_price = general_price - old_value
		var new_price 	  = parseInt(reveted_price) + parseInt(price_value)
		$('#general_order_price').text(new_price)
		$('#general_order_price_hidden').val(new_price)
		$('#general_order_price').show()
	}

	function escapeEmpty(value){
		if(value=='' || value==undefined){
			value = 0
		}
		return value
	}



	//Gutter Length
	$(document).on('click','.gutter_length_button',function(){
		$('.gutter_length_button').removeClass('active');
		$(this).addClass('active');
	});

	$(document).on('click','.gutter',function(){
		var old_value = gutter_length_price
		gutter_length_price = $(this).attr('data-price')
		$('#show_price_id_gutter_length').val($(this).attr('data-length'))
		update_price(gutter_length_price, old_value)
		$('#custom_input_gutter_length').remove()
	});

	$(document).on('change',"#custom_input_gutter_length", function(){
		// Check if length of cleaning terrtorie more than 200 fts
		var check_length = $('#custom_input_gutter_length').val()
		if( check_length < 200 ){
			$('#custom_input_gutter_length').val('')
			$('#gutter_length_alert').show()
		} else {
			var old_value = gutter_length_price
			gutter_length_price = parseInt(escapeEmpty($('#custom_input_gutter_length').val()))*1.25
			$('#show_price_id_gutter_length').val(gutter_length_price/1.25)
			update_price(gutter_length_price, old_value)
			$('#gutter_length_alert').hide()

		}
	});

	$(document).on('click','#add_field_button',function(e){  
		e.preventDefault();
		if(! $('#custom_input_gutter_length').length ){  
			$("#input_fields_wrap").append('<input type="number"  id="custom_input_gutter_length" min="200" class="form-control"  placeholder="Your fts" style="margin-top:55px;"><div class="col-md-12" id="gutter_length_alert" style="color:red;font-size=7px;clear:both;" ><small>With this option gutter length must be more than 200+ fts</small></div>');			
		}
	});

	//Gutter Guards Instaleld
	$(document).on('click','#add_gutt_guard_btn',function(e){  
		e.preventDefault()
		if(! $('#custom_input_gutter_guard').length ){  
			$("#add_gutt_guard").append('<input type="number" name="gutter_gurads" id="custom_input_gutter_guard"   min="1" class="form-control"  placeholder="Length of gutter guards" style="margin-top:55px;">') 
		}
	});

	$(document).on('click','#remove_gutter_guards',function(){
		var old_value = gutter_installed_price
		gutter_installed_price = 0
		update_price(gutter_installed_price, old_value)
		$('#custom_input_gutter_guard').remove()
	});

	$(document).on('change',"#custom_input_gutter_guard", function(){
		var old_value = gutter_installed_price
		gutter_installed_price = parseInt(escapeEmpty($('#custom_input_gutter_guard').val()))*0.5
		update_price(gutter_installed_price, old_value)
	});

	//Building Height
	$(document).on('change',"#slect_floor_price", function(){
		var old_value = building_height
		if($(this).val()==3){
			building_height = 30
		} else {
			building_height = 0
		}
		update_price(building_height, old_value)
	});

	//Show right form part
	$(document).on('click',  '#show_right_form_part', function(){
		var show_right_form = true;

		$('.field-error').remove();
		$("#form_validation :input").each(function(){
		 	if (!$(this).val()) {
		 		$(this).after('<span class="text-danger field-error">Please fill out this field!</span>');
		 		show_right_form = false;
		 	}
		});

		if (show_right_form) { $("#right_form_part").show(); }
	});

	// Initialize tooltips
	$('[data-toggle="tooltip"]').tooltip()

	//STRIPE
	var $form = $('#payment-form');
	$form.submit(function(event) {
		// Disable the submit button to prevent repeated clicks:
		$form.find('.submit').prop('disabled', true);
		// Request a token from Stripe:
		Stripe.card.createToken($form, stripeResponseHandler);
		// Prevent the form from being submitted:
		return false;
	});

	function stripeResponseHandler(status, response) {
	  // Grab the form:
	  var $form = $('#payment-form');

	  if (response.error) { // If problem!
	    // Show the errors on the form:
	    $form.find('.payment-errors').text(response.error.message);
	    $form.find('.submit').prop('disabled', false); // Re-enable submission
	  } else { // Token was created!
	    // Get the token ID:
	    var token = response.id;
	    // Insert the token ID into the form so it gets submitted to the server:
	    $form.append($('<input type="hidden" name="stripeToken">').val(token));
	    // Submit the form:
	    $form.get(0).submit();
	  }
	};

}); // END DOCUMENT.READY

// Initialize Datapickers
jQuery('#datetimepicker').datetimepicker({	
	format: 'Y-m-d A g:i',
    formatTime: 'A g:i',
    minDate: '+1970/01/04',
    allowTimes:[
	  'AM 08:00',
	  'AM 09:00',
	  'AM 10:00',
	  'AM 11:00',
	  'AM 12:00',
	  'PM 01:00',
	  'PM 02:00',
	  'PM 03:00',
	  'PM 04:00',
	  'PM 05:00'
	]
    
});



