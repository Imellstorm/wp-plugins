<script type="text/javascript">Stripe.setPublishableKey('<?php echo get_site_option('stripe_publishable_key') ?>')</script>
<div class="container-fluid">
	<form method="post" class="form-horizontal" id="payment-form">
		<input type="hidden" value="order_form" name="form_order_id" >
		<div class="row">
			<!-- LEFT FORM PART -->
			<div class="col-md-4" id="form_validation">
				<h3>User Information</h3>
				<div class="form-group">
					<label   class="col-sm-4 control-label">Name</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="client_name" placeholder="Your name" required>
					</div>
				</div>
				<div class="form-group">
					<label   class="col-sm-4 control-label">Email</label>
					<div class="col-sm-8">
						<input type="email" class="form-control"  name="client_email" placeholder="Your Email" required>
					</div>
				</div>
				<div class="form-group">
					<label   class="col-sm-4 control-label">Address</label>
					<div class="col-sm-8">
						<input type="text" class="form-control"  name="client_adress" placeholder="Your Address" required>
					</div>
				</div>
				<div class="form-group">
					<label   class="col-sm-4 control-label">Phone</label>
					<div class="col-sm-8">
						<input type="number" class="form-control numeric" name="client_phone"  placeholder="Phone number" required>
					</div>
				</div>
				<div class="form-group">
					<label   class="col-sm-4 control-label">Zip</label>
					<div class="col-sm-8">
						<input type="number" class="form-control numeric" name="client_zip"  placeholder="Zip code" required>
					</div>
				</div>
				<div class="form-group show_right_form_part_cont">
						<span  id="show_right_form_part" class="btn btn-warning">Select the Date & Type of Service</span>	
				</div>
			</div>
			<!-- RIGHT FORM PART -->
			<div id="right_form_part" class="col-md-8 ">
				<h3 class="booking-detail-header">Gutter Cleainng Details</h3>
				<div class="form-group">
					<label   class="col-sm-4 control-label">Date of Appointment</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="appointment_date" style="border-radius:4px; width:90%; float:left;" value="" id="datetimepicker" /> 
						<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="You can only select the appointment in the next 2 days. This is done to ensure we can deliver the highest quality of service & prepare all the necessary equipment." style="float:left;padding:7px;"></i>
					</div>
				</div>
				<input type="hidden" id="show_price_id_gutter_length"  name="gutter_length" min="0" value="100"  >
				<div class="form-group">
					<label   class="col-sm-4 control-label">Gutter Length</label>
					<div class="col-sm-8">
						<div class="btn-group" id="input_fields_wrap" role="group" aria-label="..."  style="border-radius:4px; width:90%; float:left;">
							<span type="button" id="btn_125" data-price="125" data-length="100" class="gutter gutter_length_button remove_added_field btn btn-default active" >Less than 100ft</span>
							<span type="button" data-price="185" data-length="200" id="btn_185" class="gutter gutter_length_button remove_added_field btn btn-default">  101ft - 200ft </span>
							<span type="button"   id="add_field_button" class="gutter_length_button btn btn-default">More than 200ft</span>
						</div>

						<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="Note that in case if the actual gutter length is bigger than you’ve selected – additional charges apply." style="float:left;padding:7px;"></i>
					</div>
				</div>
				 
				<div class="form-group">
					<label class="col-sm-4 control-label">Gutter Guards Installed?</label>
					<div class="col-sm-8">
						<div class="btn-group" id="add_gutt_guard" role="group" aria-label="..."  style="border-radius:4px; width:90%; float:left;">
							<button type="button" id="add_gutt_guard_btn" class="btn btn-default">Yes</button>
							<button type="button"  id="remove_gutter_guards" class="btn btn-default">No</button>
						</div>
						<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="If you have Gutter Guards installed – there’s an extra-charge for them (since we’ll have to remove them first and then put back on)." style="float:left;padding:7px;"></i>
					</div>
				</div>
				<input type="hidden" id="show_price_id_building_height"   min="0" value="0" name="show_cust_input" >
				<div class="form-group">
					<label class="col-sm-4 control-label">Building Height</label>
					<div class="col-sm-8">
						<div class="dropdown" style="border-radius:4px; width:90%; float:left;">
							<select id="slect_floor_price" name="building_height" class="form-control" >
								<option disabled value="0" selected="selected">Choose your house height</option>
								<option value="1">1 floor</option>
								<option value="2">2 floor</option>
								<option value="3">3 floor</option>
							</select>
						</div>
						<i class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="bottom" title="There’s an extra-charge if you have a 3-story house - $30. It’s always safer to be closer to the ground." style="float:left;padding:7px;"></i>
					</div>
				</div>
				<div class="col-md-12">
					<h3 class="booking-billing-header">Billing Information</h3>
					<div class="form-group">
						<label   class="col-sm-4 control-label">Card Number</label>
						<div class="col-sm-8">
							<input type="number" class="form-control"  name="card_number" placeholder="Card Number" data-stripe="number">
						</div>
					</div>
					<div class="form-group">
						<label   class="col-sm-4 control-label">CVC</label>
						<div class="col-sm-8">
							<input type="number" class="form-control"  name="client_cvc" placeholder="Card CVC" data-stripe="cvc">
						</div>
					</div>
					<div class="form-group">
						<label   class="col-sm-4 control-label">Expiration Date</label>
						<div class="col-sm-8">
							 
							<div class="dropdown"  style="border-radius:4px; width:50%; float:left; padding:0 2px;">
								<select id="select_card_month" name="card_exp_date_month" class="form-control" data-stripe="exp_month">
									<option disabled value="0" selected="selected">Month</option>
									<option value="01">01</option>
									<option value="02">02</option>
									<option value="03">03</option>
									<option value="04">04</option>
									<option value="05">05</option>
									<option value="06">06</option>
									<option value="07">07</option>
									<option value="08">08</option>
									<option value="09">09</option>
									<option value="10">10</option>
									<option value="11">11 </option>
									<option value="12">12</option>
								</select>
							</div>
							<div class="dropdown" style="border-radius:4px; width:50%; float:left;padding:0 2px;">
								<select id="select_card_year" name="card_exp_date_year" class="form-control" data-stripe="exp_year">
									<option disabled value="0" selected="selected">Year</option>
									<?php 
									$current_year = date("y"); 
									$future_years = $current_year + 10;
									for( $current_year; $current_year <= $future_years; ++$current_year ){
										echo "
										<option value='" . $current_year . "'>" . $current_year . "</option>
										";
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label   class="col-sm-4 control-label">Coupon Code</label>
						<div class="col-sm-8">
							<input type="number" class="form-control" name="discount_code" placeholder="Discount">
						</div>
					</div>
					<div class="form-group payment-errors"></div>
					
					<div>
						<input type="hidden" name="general_order_price_hidden" id="general_order_price_hidden" value="">
						<div class="col-md-6 order_price_block">
							<span class="general_order_price_cont">Estimated Price: <span id="general_order_price"> 125 </span> $</span>
						</div>
						<div class="col-md-6 order_finish_block">
							<button type="submit" class="btn btn-success submit finish_booking">Finish Booking my Gutter Cleaning</button>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- End .row -->
	</form>
</div>
<div class="text-center after_form_text" style="padding:20px 0;">
	<b>
		You will not be charged a penny until we finish our work. You can cancel your order any time by calling us at <a href="tel+:18002595879">1(800) 259-5879</a>
	</b> 
</div>
<div class="line"></div>
<div class="container-fluid bottom-img">
	<div class="row">
		<div class="col-md-7 text-center">
			<?php echo '<img src="' . plugins_url( '../img/payments.png', __FILE__ ) . '"> ';?>
		</div>
		<div class="col-md-5 text-center">
			<?php echo '<img src="' . plugins_url( '../img/1.jpg', __FILE__ ) . '" > ';?>
			<?php echo '<img src="' . plugins_url( '../img/2.png', __FILE__ ) . '" > ';?>
			<?php echo '<img src="' . plugins_url( '../img/3.jpg', __FILE__ ) . '" > ';?>
			<?php echo '<img src="' . plugins_url( '../img/4.png', __FILE__ ) . '" > ';?>
		</div>
	</div>
</div>