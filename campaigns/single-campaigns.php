<?php get_header(); ?>

<?php if(post_type_exists('ideas')): ?>

   <?php  add_idea_modal(get_the_id()); ?>

   <script>
    jQuery(document).ready(function($){
        $('.popup_comment_link').on('click',function(){
            $('#add_comment_form .post_id').val($(this).find('.post_id').val())
            coments_count = $(this).find('.comments_count_number')
        })

        $('.submit-comment-button').on('click',function(){
            var form = new FormData($('#add_comment_form').get(0))
            form.append( 'action', 'add_comment' )
            $.ajax({            
              type: "POST",
              url: '<?php echo admin_url("admin-ajax.php") ?>',
              data: form,
              contentType: false,
              processData: false,
              dataType: "json",
              success: function ( response ) {
                $('#commentModal').modal('hide') 
                $('#add_comment_form').get(0).reset()
                $('.idea_info_block span').html(response.html)
                $('.idea_info_block').show()
                coments_count = coments_count.text(parseInt(coments_count.text())+1)
              }
            })
        })

        $('.campaign_idea_favorite').on('click',function(){
            elem = $(this)
            if(elem.find('.favorite-star').hasClass('fa-star-o')){
                var status = 1
            } else {
                var status = 0
            }
            var data = {
                action: 'idea_favorits',
                status: status,
                post_id: elem.find('.post_id').val()
            };
            jQuery.post( "<?php echo admin_url('admin-ajax.php') ?>" , data, function(response) {
                if(response.success){
                    var favorites_count = elem.find('.favorites_count').text()
                    console.log(favorites_count)
                    if(elem.find('.favorite-star').hasClass('fa-star-o')){
                        elem.find('.favorite-star').removeClass('fa-star-o')
                        elem.find('.favorite-star').addClass('fa-star')
                        elem.find('.favorites_count').text(parseInt(favorites_count)+1)
                    } else {
                        elem.find('.favorite-star').addClass('fa-star-o')
                        elem.find('.favorite-star').removeClass('fa-star')
                        elem.find('.favorites_count').text(favorites_count-1)
                    }
                }
            }, 'json');
        })
    })
    </script>

    <div class="modal fade" id="commentModal" tabindex="-1" role="dialog"  style="display:none">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Add Comment</h4>
          </div>
          <div class="modal-body">
            <?php if(is_user_logged_in ()): ?>
                <form id="add_comment_form">
                    <div class="form-group">
                        <lavel>Comment text</lavel>
                        <textarea name="text" id="comment_text" class="form-control" style="max-width:100%"></textarea>
                        <input type='hidden' name="post_id" class="post_id">
                    </div>
                </form>
            <?php else: ?>
                <div class="form-group">
                    To post a comment please login
                </div>
            <?php endif ?>
          </div>
          <div class="modal-footer" style="margin-top:0">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <?php if(is_user_logged_in ()): ?>
                <button type="button" class="btn btn-primary submit-comment-button">Add</button>
            <?php endif ?>
          </div>
        </div>
      </div>
    </div>
<?php endif ?>

<?php
//Specific class for post listing */
if ( kleo_postmeta_enabled() ) {
    $meta_status = ' with-meta';
    add_filter( 'kleo_main_template_classes', create_function( '$cls','$cls .= "'.$meta_status.'"; return $cls;' ) );
}

/* Related posts logic */
$related = sq_option( 'related_posts', 1 );
if ( ! is_singular('post') ) {
    $related = sq_option( 'related_custom_posts', 0 );
}
//post setting
if(get_cfield( 'related_posts') != '' ) {
    $related = get_cfield( 'related_posts' );
}
?>

<?php get_template_part( 'page-parts/general-title-section' ); ?>

<?php get_template_part( 'page-parts/general-before-wrap' );?>

<?php $post_meta = get_post_meta(get_the_id()); ?>

<?php 
    $likes_total = 0;
    $comments_total = 0;
    $ideas_total = 0;
    $cmp_ideas = get_post_meta(get_the_id(),'campaign_ideas');

    if($cmp_ideas){
        foreach ($cmp_ideas as $idea_id) {
            $likes_total = $likes_total+get_post_meta($idea_id,'_item_likes',true);
            $comments_total = $comments_total+wp_count_comments($idea_id)->total_comments;
            $ideas_total++;
        }
    }
?>

<div class="row top_box">
    <div class="col-md-4">
        <div class="row text-center campaign_stats">
            <div class="col-md-4">
                <div><i class="fa fa-lightbulb-o"></i> <?php echo $ideas_total ?></div>
            </div>
            <div class="col-md-4">
                <div><i class="fa fa-comments"></i> <?php echo $comments_total ?></div>
            </div>
            <div class="col-md-4">
                <div><i class="fa fa-thumbs-up"></i> <?php echo $likes_total ?></div>
            </div>
        </div>
        <div class="cmp_add_idea_cont">
            <?php if(post_type_exists('ideas')): ?>
                <div style="margin-top: 10px;"><?php add_idea_button() ?></div>
            <?php endif ?>
        </div>
    </div>
    <div class="col-md-8 text-center">
        <?php if(isset($post_meta['campaign_end_date'][0]) && !empty($post_meta['campaign_end_date'][0]) ): ?>
            <?php 
                $end_time = strtotime($post_meta['campaign_end_date'][0]);
                $now_time = time();
                $interval = $end_time - $now_time;
            ?>
            <?php if($interval>0): ?>
                <div class="flipclock"></div>
                <script type="text/javascript">
                    var clock = jQuery('.flipclock').FlipClock(<?php echo $interval ?>,{
                        clockFace: 'DailyCounter',
                        countdown: true
                    });
                </script>  
            <?php else: ?>
                <h2>Campaign ended</h2>
            <?php endif ?>
        <?php endif ?> 
    </div>
</div>
<hr>
<div class="row single_campaign">

    <div class="col-md-5">
        <?php if(isset($post_meta['campaign_video_url'][0]) && !empty($post_meta['campaign_video_url'][0]) ): ?>
            <?php $youtube_identifier = preg_replace('/https:\/\/youtu.be/', '', $post_meta['campaign_video_url'][0]) ?>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo $youtube_identifier ?>" frameborder="0" allowfullscreen></iframe>
        <?php elseif(isset($post_meta['campaign_image_url'][0]) && !empty($post_meta['campaign_image_url'][0])): ?>
            <img src="<?php echo $post_meta['campaign_image_url'][0] ?>">
        <?php endif ?>
        <?php while ( have_posts() ) : the_post(); ?>
            <div style="text-align:justify">
                <?php the_content(); ?>
            </div>
        <?php endwhile; ?>
    </div>
    <div class="col-md-7 campaign_ideas">
        <div style="margin-bottom: 20px">
            Sort by:
            <a href="?sort=recent">Recent activity</a> | <a href="?sort=newest">Date</a> | <a href="?sort=voted">Most voted</a>
        </div>
        <?php if(isset($cmp_ideas) && !empty($cmp_ideas)): ?>
            <?php foreach ($cmp_ideas as $idea_id): ?>
                <?php $idea = get_post($idea_id) ?>
                <?php if($idea && $idea->post_status=='publish'): ?>
                    <?php $idea_meta = get_post_meta($idea->ID); ?>
                    <div style="padding:10px" class="sabai-entity sabai-entity-type-content sabai-entity-bundle-name-questions sabai-entity-bundle-type-questions sabai-entity-mode-summary sabai-questions-novotes sabai-clearfix">
                        <div class="sabai-row">
                            <div class="sabai-col-xs-3 sabai-questions-side">
                                <?php if(function_exists('kleo_item_likes')): ?>
                                    <div class="sabai-questions-vote-count idea_vote" user_id=<?php echo $idea->post_author; ?>>
                                        <?php $item_votes = get_post_meta($idea->ID,'_item_likes') ?>
                                        <?php kleo_item_likes($idea->ID) ?>            
                                    </div>
                                <?php endif ?>
                                <div class="sabai-questions-answer-count">
                                    <a data-toggle="modal" class="popup_comment_link" data-target="#commentModal" style="cursor:pointer">
                                        <?php $comments = get_comments(array('author__in'=>array($idea->post_author),'post_id'=>$idea->ID)); ?>
                                        <span class="sabai-number" <?php echo count($comments)?'style="color:#00E000"':'' ?> >
                                            <div class="comments_count_number" style="display:inline"><?php echo count($comments) ?></div>
                                            <i class="fa fa-comments"></i>
                                            <input type='hidden' class="post_id" value="<?php echo $idea->ID ?>">
                                        </span>
                                    </a>
                                </div>
                                <?php 
                                    $idea_status = get_post_meta($idea->ID, 'idea_status', true);
                                    switch ($idea_status) {
                                        case 'in discussion':
                                            $idea_status_class = 'btn-info';
                                            break;
                                        case 'selected':
                                            $idea_status_class = 'btn-primary';
                                            break;
                                        case 'rejected':
                                            $idea_status_class = 'btn-danger';
                                            break;
                                        case 'in project':
                                            $idea_status_class = 'btn-success';
                                            break;
                                        default:
                                            $idea_status_class = '';
                                    } 
                                ?>
                                <?php if($idea_status): ?>
                                    <div class="text-center <?php echo $idea_status_class ?> status_tag"><?php echo $idea_status ?></div> 
                                <?php endif ?>
                                <div class="text-center">
                                        <?php $views_count = get_post_meta($idea->ID,'views_count')[0] ?>
                                        <span class="sabai-number"><?php echo $views_count?$views_count:0 ?></span> views
                                        <?php if(is_user_logged_in()): ?>
                                            <span class="campaign_idea_favorite">
                                                <?php $favorites_meta = get_post_meta($idea->ID,'idea_favorites') ?>
                                                <i class="favorite-star fa-lg fa <?php echo in_array(get_current_user_id(),$favorites_meta)?'fa-star':'fa-star-o' ?>"></i>
                                                <span class="favorites_count">
                                                    <?php echo count($favorites_meta) ?>  
                                                </span>  
                                                <input type="hidden" class="post_id" value="<?php echo $idea->ID ?>"> 
                                            </span>
                                        <?php endif ?>  
                                </div>
                            </div>
                            <div class="sabai-col-xs-9 sabai-questions-main">
                                        <span class="idea-avatar"><?php echo get_avatar( $idea->post_author, '32' );  ?></span>
                                        <?php include_once('wp-admin/includes/plugin.php'); ?>
                                        <?php if(is_plugin_active('mycred/mycred.php')): ?>
                                            <span class="idea_user_points"><?php echo mycred_get_users_cred( $idea->post_author ) ?> <?php echo mycred()->plural() ?>
                                            </span>
                                        <?php endif ?>

                                        <a href="<?php echo site_url().'/Ideas/'.$idea->post_name ?>" class="cmp_idea_title">
                                            <div style="margin-bottom:0px"><?php echo $idea->post_title ?></div>
                                        </a>        

                                <div class="sabai-questions-activity sabai-questions-activity-inline" style="margin-top:10px">
                                    <ul class="sabai-entity-activity">
                                        <li>
                                            <a href="<?php echo bp_core_get_user_domain( $idea->post_author ); ?>" class="sabai-user sabai-user-with-thumbnail" rel="nofollow" data-popover-url="http://marylink.appteka.cc/sabai/user/profile/jcantenot">
                                                <?php echo bp_core_get_user_displayname($idea->post_author) ?>
                                            </a> 
                                            posted 
                                            <span title="<?php $idea->post_date ?>">
                                            <?php echo human_time_diff( strtotime( $idea->post_date), time() ); ?>
                                            ago
                                            </span>
                                        </li>
                                        <li>
                                            <i class="fa fa-clock-o"></i>
                                            last active 
                                            <?php echo human_time_diff( strtotime( get_user_meta( $idea->post_author, 'last_activity', true )), time() ); ?>
                                            ago
                                        </li>
                                    </ul>
                                    <div class="pull-right">
                                        <?php $tags = wp_get_post_tags( get_the_ID()) ?>
                                        <?php if(count($tags)):?>
                                            <?php foreach($tags as $tag):?>
                                                <div class="idea_tag"><?php echo $tag->name ?></div>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </div>                                          
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                <?php endif ?>
            <?php endforeach;?>
        <?php endif; ?>
    </div>
</div>



<?php get_template_part('page-parts/general-after-wrap');?>

<?php get_footer(); ?>