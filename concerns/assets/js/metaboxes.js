jQuery(document).ready(function($){	
	var treatmentsList = $("#treatments_list").select2();
	$("#galleries_list").select2();
	

	$('#add_treatment_button').on('click',function(){
		var newTreament = $('#add_treatment_input').val();
		var addedTreatments = treatmentsList.val();
			
		if( $('#add_treatment').is(":visible") && newTreament!='' && $('#treatments_list option:contains("'+newTreament+'")' ).length == 0 ){
			if( $('#treatments_list option[value="'+newTreament+'"]' ).length == 0){
				$('#treatments_list').append('<option value="'+newTreament+'">'+newTreament+'</option>');
			}
			if(addedTreatments!=null){
				addedTreatments.push(newTreament)
			} else {
				addedTreatments = newTreament;
			}
			treatmentsList.val(addedTreatments).trigger("change");
		} else {
			$('#add_treatment').show();
			$('#add_treatment_button').addClass('button-primary');
			$('#add_treatment_button').val('Create New Treatment');
		}
	});
});