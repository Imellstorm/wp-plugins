<?php
/*
Plugin Name: Alex Karev Concerns
Description: Custom content types for themes developed by Alex Karev
Version: 1.0
Author: Alex Karev
*/

function concerns_load_assets(){
	wp_enqueue_style( 'concerns_metabox-styles', plugins_url('/assets/css/metaboxes.css?v='.time(),__FILE__));
	wp_enqueue_style( 'concerns_jquery-select2', plugins_url('/assets/css/select2.css?v='.time(),__FILE__));
	wp_enqueue_script( 'concerns_select2-js', plugins_url('/assets/js/select2.min.js?v='.time(),__FILE__), array('jquery'), time(), false);
	wp_enqueue_script( 'concerns_custom-js', plugins_url('/assets/js/metaboxes.js?v='.time(),__FILE__), 'jquery-ui-core', '1.0', true);
}

function create_pt_concerns() {
	register_post_type( 'concerns', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	array('labels' => array(
			'name' => __('Concerns'), /* This is the Title of the Group */
			'singular_name' => __('Concern'), /* This is the individual type */
			'all_items' => __('All Concerns'), /* the all items menu item */
			'add_new' => __('Add New Concern'), /* The add new menu item */
			'add_new_item' => __('Add New Concern'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit Concern'), /* Edit Display Title */
			'new_item' => __('New Concern'), /* New Display Title */
			'view_item' => __('View Concern'), /* View Display Title */
			'search_items' => __('Search Concerns'), /* Search Product Title */ 
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), 
			'description' => __( 'Concerns for '.get_bloginfo() ), /* Product Description */
			'public' => true,
			'menu_icon' => 'dashicons-plus',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'concerns', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title','post-options','editor','page-attributes', 'thumbnail', 'author',),
			'taxonomies' => array( 'post_tag' ),

	 	) /* end of options */
	); /* end of register post type */
}

add_action( 'init', 'create_concerns_cat_tax' );

function create_concerns_cat_tax() {
	$labels = array(
			'name'              => _x( 'Concerns Сategories', 'taxonomy general name' ),
			'singular_name'     => _x( 'Concerns Сategory', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Concerns Сategories' ),
			'all_items'         => __( 'All Concerns Сategories' ),
			'parent_item'       => __( 'Parent Concerns Сategory' ),
			'parent_item_colon' => __( 'Parent Concerns Сategory:' ),
			'edit_item'         => __( 'Edit Concerns Сategory' ),
			'update_item'       => __( 'Update Concerns Сategory' ),
			'add_new_item'      => __( 'Add New Concerns Сategory' ),
			'new_item_name'     => __( 'New Concerns Сategory Name' ),
			'menu_name'         => __( 'Concerns Сategories' ),
		);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'concerns_category' ),
	);

	register_taxonomy( 'concerns_category', array( 'concerns' ), $args );
}

function concerns_meta_boxes(){
	add_meta_box( 'events_meta_box',
        'Concern information',
        'display_concerns_meta_box',
        'concerns', 
        'normal', 
        'high'
    );
}

function display_concerns_meta_box(){
	global $post;
	$concernTreatments = get_post_meta($post->ID, 'concern_treatments', true);
	$treatments = get_posts( array (
		'post_type'	=> 'treatments',
		'posts_per_page' => -1
	));
	$concernGalleries = get_post_meta($post->ID, 'concern_galleries', true);
	$galleries = get_posts( array (
		'post_type'	=> 'galleries',
		'posts_per_page' => -1
	));
?>
	<table class="form-table">
		<?php if( post_type_exists('treatments') ): ?>
			<tr>
				<th><label for="treatments_list">Treatments</label></th>
				<td>
					<select name="concern_treatments[]" id="treatments_list" multiple="multiple">
						<option value="">Select One</option>
						<?php foreach($treatments as $child ): ?>
							<option value="<?php echo $child->ID ?>", <?php echo $concernTreatments&&in_array($child->ID,$concernTreatments)?' selected="selected"':''?> ><?php echo $child->post_title ?></option>;
						<?php endforeach ?>
					</select>
					<br /><span class="description">The specific treatments that are used to treat this particular concern.</span>
					<div style="margin-top:20px">
						<div id="add_treatment" style="display:none; margin-bottom:20px;">
							<input id="add_treatment_input" type="text" class="large-text" placeholder="New Treatment" />
						</div>
						<input id="add_treatment_button" class="button" type="button" value="Add New Treatment" />
					</div>
				</td>
			</tr>
		<?php endif ?>

		<?php if( post_type_exists('galleries') ): ?>
			<tr>
				<th><label for="galleries_list">Galleries</label></th>
				<td>
					<select name="concern_galleries[]" id="galleries_list" multiple="multiple">
						<option value="">Select One</option>
						<?php foreach($galleries as $child ): ?>
							<option value="<?php echo $child->ID ?>", <?php echo $concernGalleries&&in_array($child->ID,$concernGalleries)?' selected="selected"':''?> ><?php echo $child->post_title ?></option>;
						<?php endforeach ?>
					</select>
					<br /><span class="description"></span>
				</td>
			</tr>
		<?php endif ?>

	</table>
<?php 
}

function save_concerns_custom_meta( $id, $item ) {
	if( $item->post_type == 'concerns' ){	
        if ( post_type_exists('treatments') ) {
        	if(!empty($_POST['concern_treatments'])) {
            	update_post_meta( $id, 'concern_treatments',$_POST['concern_treatments'] );
            	updateconcernLinkedResources($_POST['concern_treatments'],$id,'treatment_concerns');
			} else {
	    		delete_post_meta($id, 'concern_treatments');
	    	}
        } 
        if ( post_type_exists('galleries') ) {
        	if(!empty($_POST['concern_galleries'])) {
		        update_post_meta( $id, 'concern_galleries',$_POST['concern_galleries'] );
		        updateconcernLinkedResources($_POST['concern_galleries'],$id,'gallery_concerns');
		    } else {
		    	delete_post_meta($id, 'concern_galleries');
		    }
	    }
	}
}

function updateconcernLinkedResources($linkedIds,$currentId,$type){
    foreach ($linkedIds as $id) {
    	if( !is_numeric($id) && !empty($id) ){
    		$id = createNewTreatment($id,$currentId);
    	}
    	$meta = get_post_meta($id,$type,true);
    	if(empty($meta) || !in_array($currentId,$meta)){
    		$meta[] = $currentId;
    		update_post_meta($id,$type,$meta);
    	} 	
    }
}

function createNewTreatment($title,$currentId){
	$id = wp_insert_post(array(
		'post_title'	=> $title,
		'post_type'		=> 'treatments',
		'post_status'   => 'publish',
	));
	$meta = get_post_meta($currentId,'concern_treatments',true);
	$meta[] = $id;
	update_post_meta($currentId,'concern_treatments',$meta);
	return $id;
}

function concerns_shortcode( $atts ){ 
	$args = array(
		'post_type'       => 'concerns',
		'posts_per_page'  => -1,
	);
	if($_GET['category']){
		$categories = explode(';', $_GET['category']);
		foreach ($categories as $item) {
			$terms[] = $item;
		}
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'concerns_category',
				'field'    => 'slug',
				'terms'    => $terms,
			),
		);
	}
	new WP_Query( $args );
	$loop = new WP_Query( $args ); ?>
	<?php if ( $loop->have_posts() ):  ?>
		<ul class=“elements-list”>
			<?php while ( $loop->have_posts() ) : $loop->the_post() ?>
				<li><a href="<?php echo the_permalink(); ?>"><?php the_title() ?></a></li>
			<?php endwhile ?>
		</ul>
	<?php else: ?>
		'No concerns found'
	<?php endif ?>
<?php }

function related_treatments_shortcode(){
	$postId = get_the_ID();
	$treatments = get_post_meta($postId, 'concern_treatments', true);
	if(!empty($treatments)): ?>
		<ul>
			<?php foreach ($treatments as $id): ?>
				<?php $item = get_post($id) ?>
				<li><a href="<?php echo site_url().'/'.$item->post_name?>"><?php echo $item->post_title ?></a></li>
			<?php endforeach ?>
		</ul>
	<?php endif ?>
<?php }


add_action( 'admin_enqueue_scripts','concerns_load_assets' );
add_action( 'init','create_pt_concerns' );
add_action( 'add_meta_boxes','concerns_meta_boxes' );
add_action( 'save_post','save_concerns_custom_meta',10,2 );
add_shortcode( 'concerns', 'concerns_shortcode' );
add_shortcode( 'related_treatments', 'related_treatments_shortcode' );

if(!function_exists('related_galleries_shortcode')){
	function related_galleries_shortcode(){
		$galleries = get_post_meta(get_the_ID(),'treatment_galleries'); ?>
		<?php if(isset($galleries[0]) && !empty($galleries[0])): ?>
			<div vlass="row">
				<?php foreach ($galleries[0] as $val): ?>
					<?php $gallery = get_post($val); ?>
					<div class="col-md-4">
						<a href="<?php echo site_url() ?>/galleries/<?php echo $gallery->post_name ?>" class="btn btn-blue"><?php echo $gallery->post_title ?></a>
					</div>
				<?php endforeach ?>
			</div>
		<?php endif;
	}
	add_shortcode( 'related_galleries', 'related_galleries_shortcode' );
}


