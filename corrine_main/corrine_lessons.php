<?php


function create_pt_corrine_lessons() {
	register_post_type( 'corrine_lessons', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	array('labels' => array(
			'name' => __('Lessons'), /* This is the Title of the Group */
			'singular_name' => __('Lesson'), /* This is the individual type */
			'all_items' => __('All Lessons'), /* the all items menu item */
			'add_new' => __('Add New Lesson'), /* The add new menu item */
			'add_new_item' => __('Add New Lesson'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit Lesson'), /* Edit Display Title */
			'new_item' => __('New Lesson'), /* New Display Title */
			'view_item' => __('View Lesson'), /* View Display Title */
			'search_items' => __('Search Lessons'), /* Search Product Title */ 
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), 
			'description' => __( 'Lessons for '.get_bloginfo() ), /* Product Description */
			'public' => true,
			'menu_icon' => 'dashicons-exerpt-view',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'lessons', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title','post-options','editor','thumbnail','excerpt'),
	 	) /* end of options */
	); /* end of register post type */
}

add_theme_support( 'post-thumbnails' ); 

add_action( 'init', 'create_lessons_tax' );

function create_lessons_tax() {
	$args = array(
		'label'				=> 'Module',
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		//'rewrite'           => array( 'slug' => 'lessons_category' ),
	);
	register_taxonomy( 'module', array( 'corrine_lessons' ), $args );
}

function corrine_lessons_meta_boxes(){
	add_meta_box( 'corrine_lessons_meta_box',
        'Lesson Media',
        'display_corrine_lessons_meta_box',
        'corrine_lessons', 
        'normal', 
        'high'
    );
}

function display_corrine_lessons_meta_box($post){
	$lesson_video = esc_html( get_post_meta( $post->ID, 'lesson_video', true ) );
	$lesson_pdfs = get_post_meta( $post->ID, 'lesson_pdf' );
	$lesson_worksheets = get_post_meta( $post->ID, 'lesson_worksheet' );
    $lesson_handouts = get_post_meta( $post->ID, 'lesson_handout' );
	$lesson_audio = esc_html( get_post_meta( $post->ID, 'lesson_audio', true ) );
	$lesson_order = esc_html( get_post_meta( $post->ID, 'lesson_order', true ) );

	$lesson_image_urls = esc_html( get_post_meta( $post->ID, 'lesson_image_urls', true ) );
    $lesson_image_id 	= esc_html( get_post_meta( $post->ID, 'lesson_image_id', true ) );
    //$lesson_image_title= esc_html( get_post_meta( $post->ID, 'lesson_image_title', true ) );
    $lesson_image_desc = esc_html( get_post_meta( $post->ID, 'lesson_image_desc', true ) );

    $lesson_image_urls = json_decode(htmlspecialchars_decode($lesson_image_urls));
    $lesson_image_id 	= json_decode(htmlspecialchars_decode($lesson_image_id));
    //$lesson_image_title= json_decode(htmlspecialchars_decode($lesson_image_title));
    $lesson_image_desc = json_decode(htmlspecialchars_decode($lesson_image_desc));
    ?>
    <script>
	    function ajaxFiles(attachment,element){
	    	attachment = attachment.toJSON()
		    element.val(attachment.url)
	    }

	    function ajaxImages(attachment,element){
	      	attachment = attachment.toJSON()
	      	block_html = jQuery.parseHTML( '<?php include( plugin_dir_path( __FILE__ ).'/image-block.php') ?>' )
	        jQuery(block_html).find('img').attr('src',attachment.url)
	        jQuery(block_html).find('.lesson_image_url').attr('value',attachment.url)
	        jQuery(block_html).find('.lesson_image_id').attr('value',attachment.id)
	        element.prepend(block_html)
	    }

    	function showMediaUploader(element,action){
		    var mediaUploader;
		    // If the uploader object has already been created, reopen the dialog
		     if (mediaUploader) {
		      mediaUploader.open();
		      return;
		    }
		    // Extend the wp.media object
		    mediaUploader = wp.media.frames.file_frame = wp.media(
		      {
		        title: 'Choose Attachment',
		        button: {
		        text: 'Choose Attachment'
		      }, 
		      multiple: true 
		    });
		    mediaUploader.on('select', function() {
		      	selection = mediaUploader.state().get('selection')
		      	selection.map( function( attachment ) {
		      		if(action=='image'){
		      			ajaxImages(attachment,element)		      			
		      		} else {
		      			ajaxFiles(attachment,element)
		      		}		      		
		       	});
		    });
		    // Open the uploader dialog
		    mediaUploader.open();
		}

       	jQuery(document).ready(function($){
       		$('.add_pdf_btn').on('click',function(){
       			$(this).parent().after('<?php include( plugin_dir_path( __FILE__ ).'/pdf-block.php') ?>')
       		})
       		$('.add_worksheet_btn').on('click',function(){
       			$(this).parent().after('<?php include( plugin_dir_path( __FILE__ ).'/worksheet-block.php') ?>')
       		})
            $('.add_handout_btn').on('click',function(){
                $(this).parent().after('<?php include( plugin_dir_path( __FILE__ ).'/handout-block.php') ?>')
            })


            $('#add_lesson_video').on('click',function(){               
                showMediaUploader($('#lesson_video'),'file');
            });
            $('body').on('click','.add_lesson_pdf',function(){               
                showMediaUploader($(this).parent().find('.lesson_pdf'),'file');
            });
            $('body').on('click','.add_lesson_worksheet',function(){               
                showMediaUploader($(this).parent().find('.lesson_worksheet'),'file');
            });
            $('body').on('click','.add_lesson_handout',function(){               
                showMediaUploader($(this).parent().find('.lesson_handout'),'file');
            });
            $('#add_lesson_audio').on('click',function(){               
                showMediaUploader($('#lesson_audio'),'file');
            });
            $('#remove_lesson_video').on('click',function(){
            	$('#lesson_video').val('')
            })
            $('body').on('click','.remove_lesson_pdf',function(){
            	$(this).parent().remove()
            })
            $('body').on('click','.remove_lesson_worksheet',function(){
            	$(this).parent().remove()
            })
            $('body').on('click','.remove_lesson_handout',function(){
                $(this).parent().remove()
            })
            $('#remove_lesson_audio').on('click',function(){
            	$('#lesson_audio').val('')
            })

            // $('#add_lesson_image').on('click',function(){               
            //     showMediaUploader($('#lesson_images_list'),'image');
            // });
            // $('body').on('click','.remove_lesson_image_ico',function(){
            //     $(this).parent().parent().remove();
            // })

            //lesson_images_list = document.getElementById("lesson_images_list");
			//var sortable = Sortable.create(lesson_images_list);
        });
    </script>
	<p>You can upload media by clicking the buttons, or paste the external URL applicable to the media.</p>
	<hr>
	
    <input id="add_lesson_video" type="button" value="Lesson video" style="margin:20px 0px;">
    <input id="remove_lesson_video" type="button" value="-" style="margin:20px 0px;">    
    <input id="lesson_video" name="lesson_video" type="text" style="width:75%" value="<?php echo $lesson_video ?>">
    <p style="margin: 0;padding: 0;"><strong>Video</strong> <em>Must be in .mp4</em></p>
    <hr>
    <input id="add_lesson_audio" type="button" value="Lesson Audio" style="margin:20px 0px;">
    <input id="remove_lesson_audio" type="button" value="-" style="margin:20px 0px;">
    <input id="lesson_audio" name="lesson_audio" type="text" style="width:75%" value="<?php echo $lesson_audio ?>">
    <p style="margin: 0;padding: 0;"><strong>Audio</strong> <em>Must be in .mp3</em></p>
    <hr>
    <div>
    	<label>Add PDF</label> 
    	<input type="button" class="add_pdf_btn" value="+">
    </div>
    <?php if(!empty($lesson_pdfs)): ?>
    	<?php foreach ($lesson_pdfs as $lesson_pdf) {
    		include('pdf-block.php');
    	} ?>
	<?php endif ?>
    <!-- <p style="margin: 0;padding: 0;"><strong>Slides PDF</strong></p> -->
    <hr>
    <div>
    	<label>Add Worksheets</label> 
    	<input type="button" class="add_worksheet_btn" value="+">
    </div>
    <?php if(!empty($lesson_worksheets)): ?>
    	<?php foreach ($lesson_worksheets as $lesson_worksheet) {
    		include('worksheet-block.php');
    	} ?>
	<?php endif ?>
    <!-- <p style="margin: 0;padding: 0;"><strong>Worksheets PDF</strong> <em>If multiple, combine into one PDF</em></p> -->
    <hr>
    <div>
        <label>Add Handouts</label> 
        <input type="button" class="add_handout_btn" value="+">
    </div>
    <?php if(!empty($lesson_handouts)): ?>
        <?php foreach ($lesson_handouts as $lesson_handout) {
            include('handout-block.php');
        } ?>
    <?php endif ?>
    <hr>
    <p>Lesson order</p>
    <input id="lesson_order" name="lesson_order" type="text" style="width:75%" value="<?php echo $lesson_order ?>">
    
    <?php if(false):?>
	    <input id="add_lesson_image" type="button" value="Add lesson image" style="margin:20px 10px; display:block">
	    <div id="lesson_images_list" class="list-group">
		    <?php  
		    if(count($lesson_image_urls)):?>
		        <?php foreach ($lesson_image_urls as $key => $lesson_image_url): ?>
		            <?php if(!empty($lesson_image_url) && isset($lesson_image_id[$key])): ?>
		            	<?php include( plugin_dir_path( __FILE__ ).'/image-block.php') ?>
		        	<?php endif ?>
		        <?php endforeach ?>
		    <?php endif ?>
		</div>
	<?php endif ?>
<?php }

function save_corrine_lessons_custom_meta( $id, $item ) {
	if( $item->post_type == 'corrine_lessons' ){
        // delete_post_meta($id,'lesson_image_urls');
        // delete_post_meta($id,'lesson_image_id');
        // //delete_post_meta($id,'lesson_image_title');
        // delete_post_meta($id,'lesson_image_desc');
        // if ( isset( $_POST['lesson_image_urls'] ) && !empty($_POST['lesson_image_urls']) ) {
        //     update_post_meta( $id, 'lesson_image_urls', json_encode($_POST['lesson_image_urls']) );
        //     update_post_meta( $id, 'lesson_image_id', json_encode($_POST['lesson_image_id']) );
        //     //update_post_meta( $id, 'lesson_image_title', json_encode($_POST['lesson_image_title']) );
        //     update_post_meta( $id, 'lesson_image_desc', json_encode($_POST['lesson_image_desc']) );
        // }

        delete_post_meta($id,'lesson_pdf');
        if ( isset( $_POST['lesson_pdf'] ) && !empty($_POST['lesson_pdf']) ) {
        	foreach ($_POST['lesson_pdf'] as $pdf) {
        		if($pdf){
        			add_post_meta( $id, 'lesson_pdf', $pdf );
        		}
        	}
        }
        delete_post_meta($id,'lesson_worksheet');
        if ( isset( $_POST['lesson_worksheet'] ) && !empty($_POST['lesson_worksheet']) ) {
        	foreach ($_POST['lesson_worksheet'] as $worksheet) {
        		if($worksheet){
        			add_post_meta( $id, 'lesson_worksheet', $worksheet );
        		}
        	}
        }
        delete_post_meta($id,'lesson_handout');
        if ( isset( $_POST['lesson_handout'] ) && !empty($_POST['lesson_handout']) ) {
            foreach ($_POST['lesson_handout'] as $handout) {
                if($handout){
                    add_post_meta( $id, 'lesson_handout', $handout );
                }
            }
        }
        if ( isset( $_POST['lesson_video'] ) ) {
            update_post_meta( $id, 'lesson_video', $_POST['lesson_video'] );
        }
        if ( isset( $_POST['lesson_audio'] ) ) {
            update_post_meta( $id, 'lesson_audio', $_POST['lesson_audio'] );
        }
        if ( isset( $_POST['lesson_order'] ) ) {
            update_post_meta( $id, 'lesson_order', $_POST['lesson_order'] );
        }
	}
}

function lessons_load_assets(){
	wp_enqueue_style( 'lessons_metabox-styles', plugins_url('/assets/css/metaboxes.css?v='.time(),__FILE__));
	wp_enqueue_script('lessons_sortable', plugins_url('/assets/js/sortable.min.js?v='.time(),__FILE__), '', '1.0', false);
}

add_action( 'init','create_pt_corrine_lessons' );
add_action( 'add_meta_boxes','corrine_lessons_meta_boxes' );
add_action( 'save_post','save_corrine_lessons_custom_meta',10,2 );
add_action( 'admin_enqueue_scripts','lessons_load_assets' );
