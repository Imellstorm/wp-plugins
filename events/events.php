<?php
/*
Plugin Name: Alex Karev Events
Description: Custom content types for themes developed by Alex Karev
Version: 1.0
Author: Alex Karev
*/

function events_load_assets(){
	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_style( 'events_metabox-styles', plugins_url('/assets/css/metaboxes.css?v='.time(),__FILE__));
	wp_enqueue_style( 'events_jquery-ui-custom',plugins_url('/assets/css/jquery-ui-flat/jquery-ui-1.10.4.custom.min.css?v=.time()',__FILE__));
	wp_enqueue_script( 'events_datetimepicker-js',plugins_url('/assets/js/DateTimePicker.min.js',__FILE__), array('jquery'), time(), false);
	wp_enqueue_style( 'events_date-time-picker-styles', plugins_url('/assets/css/DateTimePicker.min.css?v='.time(),__FILE__));
	wp_enqueue_script('events_custom-js', plugins_url('/assets/js/metaboxes.js?v='.time(),__FILE__), 'jquery-ui-core', '1.0', true);
}

function create_pt_events() {
	register_post_type( 'events', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	array('labels' => array(
			'name' => __('Events'), /* This is the Title of the Group */
			'singular_name' => __('Event'), /* This is the individual type */
			'all_items' => __('All Events'), /* the all items menu item */
			'add_new' => __('Add New Event'), /* The add new menu item */
			'add_new_item' => __('Add New Event'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit Event'), /* Edit Display Title */
			'new_item' => __('New Event'), /* New Display Title */
			'view_item' => __('View Event'), /* View Display Title */
			'search_items' => __('Search Event'), /* Search Product Title */ 
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), 
			'description' => __( 'Events for '.get_bloginfo() ), /* Product Description */
			'public' => true,
			'menu_icon' => 'dashicons-calendar',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'events', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title','post-options','editor','page-attributes', 'author',),
	 	) /* end of options */
	); /* end of register post type */
}

function events_meta_boxes(){
	add_meta_box( 'events_meta_box',
        'Events Information',
        'display_events_meta_box',
        'events', 
        'normal', 
        'high'
    );
}

function display_events_meta_box(){
	global $post;
	$eventNotify 	= get_post_meta($post->ID, 'event_email_to', true);
	$eventAddress 	= get_post_meta($post->ID, 'event_address', true);
	$eventDate 		= get_post_meta($post->ID, 'event_date', true);
	$eventStartTime = get_post_meta($post->ID, 'event_start_time', true);
	$eventEndTime = get_post_meta($post->ID, 'event_end_time', true);
?>
	<div id="date-time-box"></div>
	<table class="form-table">
		<tr>
			<th><label for="event_email_to">Notify</label></th>
			<td>
				<input type="text" name="event_email_to" id="event_email_to" value="<?php echo $eventNotify ?>" size="30">
				<br><span class="description">The email address RSVP's will be sent to for this event. If none is set it will be sent to info@email.com</span>
			</td>
		</tr>
		<tr>
			<th><label for="event_address">Address</label></th>
			<td>
				<input type="text" name="event_address" id="event_address" value="<?php echo $eventAddress ?>" size="30">
				<br><span class="description">The complete address of the event. For example: 7906 Santa Monica Blvd Ste. 201, West Hollywood CA, 90501</span>
			</td>
		</tr>
		<tr>
			<th><label for="event_date">Date</label></th>
			<td>
				<input type="text" name="event_date" id="event_date" class="datepicker" value="<?php echo $eventDate ?>" size="30">
				<br><span class="description">The time the event starts.</span>
			</td>
		</tr>
		<tr>
			<th><label for="event_start_time">Start Time</label></th>
			<td>
				<input type="text" class="time" name="event_start_time" id="event_start_time" value="<?php echo $eventStartTime ?>" size="30" data-field="time" readonly="">
				<br><span class="description">The day of the event.</span>
			</td>
		</tr>
		<tr>
			<th><label for="event_end_time">End Time</label></th>
			<td>
				<input type="text" class="time" name="event_end_time" id="event_end_time" value="<?php echo $eventEndTime ?>" size="30" data-field="time" readonly="">
				<br><span class="description">The day of the event.</span>
			</td>
		</tr>
	</table>
<?php 
}


function save_events_custom_meta( $id, $item ) {
    // Check post type
    if ( $item->post_type == 'events' ) {
        // Store data in post meta table if present in post data
        if ( isset( $_POST['event_email_to'] )  ) {
            update_post_meta( $id, 'event_email_to',$_POST['event_email_to'] );
        }
        if ( isset( $_POST['event_address'] )  ) {
            update_post_meta( $id, 'event_address',$_POST['event_address'] );
        }
        if ( isset( $_POST['event_date'] )  ) {
            update_post_meta( $id, 'event_date',$_POST['event_date'] );
        }
        if ( isset( $_POST['event_start_time'] ) ) {
            update_post_meta( $id, 'event_start_time',$_POST['event_start_time'] );
        }
        if ( isset( $_POST['event_end_time'] ) ) {
            update_post_meta( $id, 'event_end_time',$_POST['event_end_time'] );
        }
    }
}

add_action( 'admin_enqueue_scripts','events_load_assets' );
add_action( 'init','create_pt_events' );
add_action( 'add_meta_boxes','events_meta_boxes' );
add_action( 'save_post','save_events_custom_meta',10,2 );



