<?php

/*
Plugin Name: UI Facebook Feeds
Description: UI Facebook Feeds
Version: 1.0.0
Author: 
Author URI:

*/

define( 'ui_FB_FEED_VERSION', '1.0.0' );


class ui_fb_feed{

    public $auth_token = '';

    private $init = false;

    public function __construct() {

        add_action( 'init', array( $this, 'ui_register_session' ) );

        $this->setting_page = array(
            'page-title'        => __( 'Facebook Feed Settings', 'ilsh' ),
            'menu-title'        => __( 'Facebook Feed', 'ilsh' ),
            'menu-slug'         => __( 'ui_fb', 'ilsh' ),
            'button-text'       => __( 'Save changes', 'ilsh' ),
            'page-setting-info' => __( 'example info' ),

        );

        $this->ui_data['ui_app_id'] = get_option( 'ui_app_id' );

        $this->ui_data['ui_app_key'] = get_option( 'ui_app_key' );

        $this->ui_data['ui_fb_page_id'] = get_option( 'ui_fb_page_id' );

        $this->ui_data['ui_fb_limit'] = get_option( 'ui_fb_limit' ) > 0 ? get_option( 'ui_fb_limit' ) : 5;


        /*
         * create pludin admin settings page
         */

        add_action( 'admin_menu', array( $this, 'ui_register_menu_page' ) );


//        add_action( 'wp_enqueue_scripts', array( $this, 'ui_enqueue_scripts_styles' ) );
//
//        add_action( 'admin_enqueue_scripts', array( $this, 'ui_enqueue_scripts_styles' ) );

        $this->init();
    }

    public function init() {

        if( count( array_filter( $this->ui_data ) ) == count( $this->ui_data ) ) {

            $this->auth_token = $this->get_auth_token();

            $this->init = 1;

        } else {

            $this->init = false;
        }

        add_action( 'admin_enqueue_scripts', array( $this, 'events_load_assets' ) );
        add_action( 'wp_enqueue_scripts', array( $this, 'events_load_assets' ) );


        add_shortcode( 'fb_feed', array( $this, 'fb_feed_display' ) );

    }

    public function fb_feed_display() {

        return $this->html_feed();
    }


    public function events_load_assets(){
        wp_enqueue_style( 'fb_styles', plugins_url('/assets/css/fb_feed.css', __FILE__));
    }

    public function ui_register_menu_page() {

        add_menu_page(
            $this->setting_page['page-title'],
            $this->setting_page['menu-title'],
            'manage_options',
            $this->setting_page['menu-slug'],
            array( $this, 'ui_menu_page' ),
            'dashicons-list-view'
        );
    }

    function ui_register_session() {

        if ( ! session_id() ) {

            session_start();
        }
    }

    public function ui_menu_page() {

        $ui_feed = new ui_fb_feed();

        //save action
        $this->ui_save_settings();

        ?>
        <div style="float:left;width: 70%;">
            <h1><?php echo $this->setting_page['page-title'];?></h1>

            <div class="setting-info-div">
                <?php echo $this->setting_page['page-setting-info'];?>
            </div>

            <form method='POST' action=''>

                <input type="hidden" name="ui_action" value="settings">

                <table class="form-table">
                    <tbody>
                    <tr>
                        <th scope="row"><?php _e( 'App ID' );?></th>
                        <td>
                            <input required="required" type="text" name="ui_app_id" value="<?php echo get_option( 'ui_app_id' );?>" class="regular-text">
                            <p class="description"><?php _e( 'Enter your Facebook App ID' );?></p>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?php _e( 'App Secret Key' );?></th>
                        <td>
                            <input required="required" type="text" name="ui_app_key" value="<?php echo get_option( 'ui_app_key' );?>" class="regular-text">
                            <p class="description"><?php _e( 'Enter your Facebook App Secret Key' );?></p>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?php _e( 'Facebook Page ID' );?></th>
                        <td>
                            <input required="required" type="text" name="ui_fb_page_id" value="<?php echo get_option( 'ui_fb_page_id' );?>" class="regular-text">
                            <p class="description"><?php _e( 'Enter Facebook Page ID' );?></p>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><?php _e( 'Facebook Feeds Limit' );?></th>
                        <td>
                            <input min="1" required="required" type="number" name="ui_fb_limit" value="<?php echo get_option( 'ui_fb_limit' );?>" class="regular-text">
                            <p class="description"><?php _e( 'Enter Facebook Feeds Limit' );?></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type='submit' name='ui_settings' class='button button-primary' value='<?php echo $this->setting_page['button-text'];?>'>
                        </td>
                    </tr>

                    </tbody>

                </table>

            </form>
        </div>
        <div style="float:left;width: 30%;">
            <?php $ui_feed->html_feed();?>
        </div>
        <?php
    }

    public function ui_save_settings() {

        if( isset( $_POST['ui_action'] ) && $_POST['ui_action'] == 'settings' ) {

            if( isset( $_POST['ui_app_id'] ) ) {

                update_option( 'ui_app_id', $_POST['ui_app_id'] );

            } else {

                delete_option( 'ui_app_key' );
            }

            if( isset( $_POST['ui_app_key'] ) ) {

                update_option( 'ui_app_key', $_POST['ui_app_key'] );

            } else {

                delete_option( 'ui_app_key' );
            }

            if( isset( $_POST['ui_fb_page_id'] ) ) {

                update_option( 'ui_fb_page_id', $_POST['ui_fb_page_id'] );

            } else {

                delete_option( 'ui_fb_page_id' );
            }

            if( isset( $_POST['ui_fb_limit'] ) ) {

                $limit = (int) $_POST['ui_fb_limit'] > 0 ? (int) $_POST['ui_fb_limit'] : $this->ui_data['ui_fb_limit'];

                update_option( 'ui_fb_limit', $limit );

            } else {

                delete_option( 'ui_fb_limit' );
            }
        }
    }


    private function get_auth_token() {

        if( ! $this->auth_token ) {

            return $this->fetch_url( "https://graph.facebook.com/oauth/access_token?"
                . "grant_type=client_credentials"
                . "&client_id={$this->ui_data['ui_app_id']}"
                . "&client_secret={$this->ui_data['ui_app_key']}");
        }

        return $this->auth_token;
    }


    private function fetch_url( $url ){

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_TIMEOUT, 20 );
        // You may need to add the line below
         curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);

        $data = curl_exec( $ch );
        curl_close( $ch );

        return $data;
    }

    public function get_cover() {

        return $this->fetch_url( "https://graph.facebook.com/{$this->ui_data['ui_fb_page_id']}?{$this->auth_token}&fields=cover");
    }


    public function html_feed() {

        $feeds = $this->get_feed();

        if( ! $feeds || ! isset( $feeds->data ) || empty( $feeds->data ) ) return;

        $cover = array();

        $cover = json_decode( $this->get_cover(), true );
        ?>

        <div class="feed-preview">
            <?php if( is_admin() ):?>
                <h1>
                    Feed Preview
                </h1>
            <?php endif;?>
            <ul>
                <?php if( $cover && isset( $cover['cover']['source'] ) && ! empty( $cover['cover']['source'] ) ):?>
                    <div class="feed-wrapp" id="feed-cover">
                        <li>
                            <div class="feed-row">
                                <div class="feed-img" style="background-image: url(<?php echo $cover['cover']['source'];?>);"></div>
                            </div>
                        </li>
                    </div>
                <?php endif;?>
                <?php foreach( $feeds->data as $key => $feed ):?>
                    <li>
                        <div class="feed-wrapp" id="feed-<?php echo $feed->id;?>">
                            <div class="feed-row">
                                <div class="feed-left feed-ava"><img class="" src="<?php echo 'https://graph.facebook.com/' .$this->ui_data['ui_fb_page_id'] . '/picture"/';?>"></div>
                                <div class="feed-left feed-title">
                                    <h3>
                                        <a href="<?php echo 'https://www.facebook.com/' . $this->ui_data['ui_fb_page_id'];?>" class="feed-link-sub">
                                            <?php echo $feed->from->name;?>
                                        </a>
                                    </h3>
                                    <div class="feed_date"><?php echo date('Y-m-d',strtotime($feed->created_time)) ?></div>
                                </div>
                            </div>
                            <div>
                                <?php echo $feed->message ?>
                            </div>
                            <?php if( isset( $feed->full_picture ) ):?>
                                <div class="feed-row">
                                    <a href="<?php echo $feed->link;?>" class="feed-link">
                                        <div class="feed-img" style="background-image: url(<?php echo $feed->full_picture;?>);"></div>
                                    </a>
                                </div>
                            <?php endif;?>
                            <div class="feed-row">
                                <h3><a href="<?php echo $feed->link;?>" class="feed-link"><?php echo $feed->name;?></a></h3>
                            </div>
                            <?php if($feed->description): ?>
                                <div class="feed-row feed-description">
                                    <?php echo $feed->description;?>
                                </div>
                            <?php endif ?>
                            <div class="feed_links">
                                <a href="<?php $link = explode( '_', $feed->id ); echo 'https://www.facebook.com/' . $this->ui_data['ui_fb_page_id'] . '/posts/' . $link[1];?>" class="fb_view">View on Facebook</a>
                                <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/<?php echo $this->ui_data['ui_fb_page_id'] ?>/posts/<?php echo $link[1] ?>" class="fb_share">Share</a>
                            </div>
                        </div>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
        <?php
    }

    public function get_feed() {

        if( ! $this->init ) return null;

        $feeds = array();

        $fields = "id,message,attachments,full_picture,picture,link,name,description,type,icon,created_time,from,object_id";

        $feeds = $this->fetch_url( "https://graph.facebook.com/{$this->ui_data['ui_fb_page_id']}/feed"
            . "?{$this->auth_token}"
            . "&fields={$fields}"
            . "&limit={$this->ui_data['ui_fb_limit']}" );


        return $feeds ? json_decode( $feeds ) : null;
    }


}

new ui_fb_feed();
