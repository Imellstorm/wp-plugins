<script type="text/javascript">
    jQuery(document).ready(function($){
        $('#home_contacts_form').on('submit',function(e){
            e.preventDefault();
            var form = $(this);
            var form_results = $('#form-results');

            form_results.html(' ');
            form_results.removeClass('alert');
            form_results.removeClass('alert-danger');
            form_results.removeClass('alert-success');

            form.find('.btn').prop('disabled', true);

            var errors = [];

            if( form.find('input[name=firstname]').val()    == "" ) { errors.push('The first name field is required'); }
            if( form.find('input[name=lastname]').val()     == "" ) { errors.push('The last name field is required'); }
            if( form.find('input[name=phone]').val() == "" ) { errors.push('The phone field is required'); }
            if( form.find('input[name=email]').val() == "" ) { errors.push('The email field is required'); }
            if( !$('input[name=agree]').is(':checked') ) { errors.push('The agree check is required'); }
            if( errors.length > 0 ){

                var error_html = '<ul>';
                form_results.addClass('alert');
                form_results.addClass('alert-danger');

                $.each(errors, function( index, value ) {
                    error_html += '<li>' +value+ '</li>';
                });
                error_html += '</ul>';

                form_results.html(error_html);
                form.find('.btn').prop('disabled', false);
                return false;
            }

            var data = {
                action: 'submit_home_contacts',
                data: form.serialize()
            };

            jQuery.post( '/dev/aac/wp-admin/admin-ajax.php' , data, function(response) {
                form.find('.btn').prop('disabled', false);
                $('#form-results').html('<div class="alert alert-success">'+response+'</div>');
            }, 'json');
        });
    })
</script>

<form id="home_contacts_form" data-sr="enter wait 0.05s">
    <div class="col-sm-8">
        <div class="row">
            <div class="form-group col-sm-6">
                <label for="">First Name *</label>
                <input type="text" class="form-control" name="firstname" />
            </div>
            <div class="form-group col-sm-6">
                <label for="">Last Name *</label>
                <input type="text" class="form-control" name="lastname"/>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-sm-6">
                <label for="">Phone *</label>
                <input type="text" class="form-control" name="phone"/>
            </div>
            <div class="form-group col-sm-6">
                <label for="">Email *</label>
                <input type="text" class="form-control" name="email"/>
            </div>
        </div>
        <div class="form-group">
            <label for="">Your Message</label>
            <textarea rows="2" class="form-control" name="message"></textarea>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group agree-terms">
                <label for=""><input type="checkbox" name="agree"> I agree to Advanced Aesthetic Center’s Terms & Conditions and I would like to receive future e-mail communications.</label>
        </div>
        <div class="form-group">
            <div class="g-recaptcha" data-sitekey="6LffcxgTAAAAAA1gl-iR9c5I8xf2ueSivIliMbZ4"></div>
        </div>
        <div id="form-results"></div>
        <button type="submit" class="btn btn-block btn-blue">Send Message</button>
    </div>
</form>
