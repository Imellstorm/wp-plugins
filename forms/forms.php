<?php
/*
Plugin Name: Alex Karev Forms
Description: Create forms for themes developed by Alex Karev
Version: 1.0
Author: Alex Karev
*/

add_action('wp_ajax_submit_sidebar_contacts','submit_sidebar_contacts');
add_action('wp_ajax_nopriv_submit_sidebar_contacts','submit_sidebar_contacts');

function submit_sidebar_contacts(){
    parse_str($_POST['data'],$formData);
    global $wpdb;
    $results = $wpdb->query( 'INSERT INTO '.$wpdb->prefix.'sidebar_contacts (firstname,lastname,phone,email,message) VALUES ("'.$formData['firstname'].'","'.$formData['lastname'].'","'.$formData['phone'].'","'.$formData['email'].'","'.$formData['message'].'")');
    if($results){
        $response = 'Your message has been sent!';
    } else {
        $response = 'Form data not saved!';
    }

    $message = mailHtml($formData);
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers.= "Content-type:text/html;charset=UTF-8" . "\r\n";
    wp_mail( 'joey@urgeinteractive.com','Sidebar contacts',$message,$headers);

    echo json_encode(array($response));
    exit;
}
    

add_action('wp_ajax_submit_home_contacts','submit_home_contacts');
add_action('wp_ajax_nopriv_submit_home_contacts','submit_home_contacts');

function submit_home_contacts(){
    parse_str($_POST['data'],$formData);
    global $wpdb;
    $results = $wpdb->query( 'INSERT INTO '.$wpdb->prefix.'home_contacts (firstname,lastname,phone,email,message) VALUES ("'.$formData['firstname'].'","'.$formData['lastname'].'","'.$formData['phone'].'","'.$formData['email'].'","'.$formData['message'].'")');
    if($results){
        $response = 'Your message has been sent!';
    } else {
        $response = 'Form data not saved!';
    }

    $message = mailHtml($formData);
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers.= "Content-type:text/html;charset=UTF-8" . "\r\n";
    wp_mail( 'joey@urgeinteractive.com','Home contacts',$message,$headers);

    echo json_encode(array($response));
    exit;
}

add_action( 'admin_menu', 'register_forms_menu_page', 999 );

function register_forms_menu_page() {
    add_menu_page( 'Forms', 'Forms', 'manage_options', 'forms', 'forms_custom_menu_page', 'dashicons-list-view', 6 );
}

function forms_custom_menu_page(){
    if(isset($_GET['action']) && $_GET['action']=='delete' && isset($_GET['id'])){
        delete_forms_item();
    }
    if(!isset($_GET['type']) || $_GET['type']=='sidebar_contacts'){
        include plugin_dir_path(__FILE__).'/page-sidebar_contacts.php';
    } else if(isset($_GET['type']) && $_GET['type']=='home_contacts') {
        include plugin_dir_path(__FILE__).'/page-home_contacts.php';
    }
}

function delete_forms_item(){
    global $wpdb;
    if(isset($_GET['type']) && $_GET['type']=='sidebar_contacts'){
        $wpdb->query('DELETE FROM '.$wpdb->prefix.'sidebar_contacts WHERE '.$wpdb->prefix.'sidebar_contacts.id = '.$_GET['id']);
    } else if(isset($_GET['type']) && $_GET['type']=='home_contacts') {
        $wpdb->query('DELETE FROM '.$wpdb->prefix.'home_contacts WHERE '.$wpdb->prefix.'home_contacts.id = '.$_GET['id']);
    }
}

//export to CSV

add_action('init','csv');
function csv() {
	switch ($_SERVER['REQUEST_URI']) {
		case '/dev/aac/sidebar_contacts.csv':
			exportCsv('sidebar_contacts');
			break;
		case '/dev/aac/home_contacts.csv':
			exportCsv('home_contacts');
			break;
	}
}

function exportCsv($type){
	global $wpdb; 
    $query = 'SELECT * FROM '.$wpdb->prefix.$type;
    $items = $wpdb->get_results($query, ARRAY_A );
    if(!empty($items)){
        header( 'Content-Type: text/csv' );
        header( 'Content-Disposition: attachment;filename='.$type.'.csv');
        $csvExportFile = fopen('php://output', 'w');

        foreach ($items as $key => $val) {
            foreach ($val as $k => $v) {
                $csvHeader[] = $k;
                $csvRow[] = $v;
            }
            if($key==0){
                fputcsv($csvExportFile, $csvHeader);
            }
            fputcsv($csvExportFile, $csvRow);
            $csvRow = array();
            $csvHeader = array();
        }
        fclose($csvExportFile);
    }        
    exit();
}

//shortcode

add_shortcode( 'uiform', 'uiform_func' );
function uiform_func( $atts ){
    if(isset($atts['type'])){
        if($atts['type']=='sidebar_contacts'){
            $form = file_get_contents(plugin_dir_path(__FILE__).'/form-sidebar_contacts.php');
        }
        if($atts['type']=='home_contacts'){
            $form = file_get_contents(plugin_dir_path(__FILE__).'/form-home_contacts.php');
        }
        return $form;
    }
}

//install tables

function createTables(){
    global $wpdb;
    $table_registr = $wpdb->prefix . home_contacts;
    $sql = "
        CREATE TABLE IF NOT EXISTS `" . $table_registr . "` (
          `id` int(10) NOT NULL AUTO_INCREMENT,
          `firstname` varchar(250) NOT NULL,
          `lastname` varchar(250) NOT NULL,
          `phone` varchar(250) NOT NULL,
          `email` varchar(250) NOT NULL,
          `message` text NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ";
    $wpdb->query($sql);

    $table_registr = $wpdb->prefix . sidebar_contacts;
    $sql = "
        CREATE TABLE IF NOT EXISTS `" . $table_registr . "` (
          `id` int(10) NOT NULL AUTO_INCREMENT,
          `firstname` varchar(250) NOT NULL,
          `lastname` varchar(250) NOT NULL,
          `phone` varchar(250) NOT NULL,
          `email` varchar(250) NOT NULL,
          `message` text NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ";
   $wpdb->query($sql);
}
register_activation_hook( __FILE__, 'createTables');

//mail message creation

function mailHtml($data){
    $message = '<table style="width:100%;border:solid 1px #EAEAEA">';
    if(isset($data['firstname'])){
        $message.= '<tr style="background:#EAF3FA;"><td style="font-weight:bold; padding:5px">First Name:</td></tr><tr><td style="padding:5px 5px 5px 20px">'.$data['firstname'].'</td></tr>';
    }
    if(isset($data['lastname'])){
        $message.= '<tr style="background:#EAF3FA;"><td style="font-weight:bold; padding:5px">Last Name:</td></tr><tr><td style="padding:5px 5px 5px 20px">'.$data['lastname'].'</td></tr>';
    }
    if(isset($data['phone'])){
        $message.= '<tr style="background:#EAF3FA;"><td style="font-weight:bold; padding:5px">Phone:</td></tr><tr><td style="padding:5px 5px 5px 20px;">'.$data['phone'].'</td></tr>';
    }
    if(isset($data['email'])){
        $message.= '<tr style="background:#EAF3FA;"><td style="font-weight:bold; padding:5px">Email:</td></tr><tr><td style="padding:5px 5px 5px 20px">'.$data['email'].'</td></tr>';
    }
    if(isset($data['message'])){
        $message.= '<tr style="background:#EAF3FA;"><td style="font-weight:bold; padding:5px">Message:</td></tr><tr><td style="padding:5px 5px 5px 20px;">'.$data['message'].'</td></tr>';
    }
    $message.= '</table>';
    return $message;
}

?>