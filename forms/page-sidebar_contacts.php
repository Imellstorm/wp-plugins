<?php
    if( ! class_exists( 'WP_List_Table' ) ) {
        require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
    }

    class UI_Forms_Table extends WP_List_Table {

        private $per_page = 20;

        function get_columns(){
          $columns = array(
            'firstname'     =>  'First name',
            'lastname'      =>  'Last name',
            'phone'         =>  'Phone',
            'email'         =>  'Email',
            'message'       =>  'Message',
          );
          return $columns;
        }

        function get_items(){
            global $wpdb; 
            $query = 'SELECT * FROM '.$wpdb->prefix.'sidebar_contacts';

            if(isset($_GET['s']) && isset($_GET['sc'])){
                $query.= ' WHERE '.$_GET['sc'].' LIKE "%'.$_GET['s'].'%"';
            }
            if(isset($_GET['orderby'])){
                $query.= ' ORDER BY '.$_GET['orderby'];
            }
            if(isset($_GET['order'])){
                $query.= ' '.$_GET['order'];
            }
            //$current_page = $this->get_pagenum();
            if(!isset($_GET['paged'])){
                $offset = 0;
            } else {
                $offset = ($_GET['paged']-1)*$this->per_page;
            }
            $query.= ' LIMIT '.$this->per_page.' OFFSET '.$offset;
            $result = $wpdb->get_results($query, ARRAY_A );
            return $result;
        }

        function get_total(){
            global $wpdb;
            $query = 'select count(*) as total from '.$wpdb->prefix.'sidebar_contacts';
            if(isset($_GET['s']) && isset($_GET['sc'])){
                $query.= ' WHERE '.$_GET['sc'].' LIKE "%'.$_GET['s'].'%"';
            }
            $queryResult = $wpdb->get_results($query,ARRAY_A);
            return $queryResult[0]['total'];
        }

        function prepare_items() {
            $this->items = $this->get_items();

            $hidden = array();
            $columns = $this->get_columns();
            $sortable = $this->get_sortable_columns();
            $this->_column_headers = array($columns, $hidden, $sortable);

            $this->get_search($columns);
            $this->paginate();
        }

        function column_firstname($item) {
          $actions = array(
                'delete' => sprintf('<a href="?page=%s&type=sidebar_contacts&action=%s&id='.$item['id'].'">Delete</a>',$_REQUEST['page'],'delete',$item['id']),
            );
          return sprintf('%1$s %2$s', $item['firstname'], $this->row_actions($actions) );
        }

        function get_search($columns){
            echo '<form position:relative; top:30px;">';
            if(count($columns)){
                echo '<select name="sc">';
                foreach ($columns as $key => $val) {
                    echo '<option value="'.$key.'">'.$val.'</option>';
                }
                echo '</select>';
            }
            echo '<input type="hidden" name="page" value="forms" />';
            echo '<input type="hidden" name="type" value="sidebar_contacts" />';
            $this->search_box('search', 'search_id');
            echo '</form>';
        }

        function paginate(){
            $this->set_pagination_args( array(
                'total_items' => $this->get_total(),
                'per_page'    => $this->per_page,
            ) );
        }

        function column_default( $item, $column_name ) {
            switch( $column_name ) { 
                case 'firstname':
                case 'lastname':
                case 'phone':
                case 'email':
                case 'message':
                  return $item[ $column_name ];
                default:
                  return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes
            }
        }

        function get_sortable_columns() {
          $sortable_columns = array(
            'firstname'    => array('firstname',false),
            'lastname'     => array('lastname',false),
            'phone'        => array('phone',false),
            'email'        => array('email',false),
            'message'      => array('message',false),
          );
          return $sortable_columns;
        }
    }

    $UIFormsTable = new UI_Forms_Table();
?>       

<div class="wrap">
    <h2>UI Forms Tables</h2>
    <?php include plugin_dir_path(__FILE__).'/tab-menu.php'; ?>
    <div style="margin:10px 0">
        Form shortcode: <strong>[uiform type="sidebar_contacts"]</strong>
    </div>
    <div style="margin:10px 0">
        <a href="<?php echo site_url(); ?>/sidebar_contacts.csv" class="button">Export CSV</a>
    </div>
    <?php 
        $UIFormsTable->prepare_items(); 
        $UIFormsTable->display(); 
    ?>
</div> 
