<?php
/*
Plugin Name: PL Requests
Description: Custom content types for themes developed by Urge Interactive
Version: 1.0
Author: Urge Interactive
*/


function create_pt_requests() {
	register_post_type( 'requests', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	array('labels' => array(
			'name' => __('Requests'), /* This is the Title of the Group */
			'singular_name' => __('Request'), /* This is the individual type */
			'all_items' => __('All Requests'), /* the all items menu item */
			'add_new' => __('Add New Request'), /* The add new menu item */
			'add_new_item' => __('Add New Request'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit Request'), /* Edit Display Title */
			'new_item' => __('New Request'), /* New Display Title */
			'view_item' => __('View Request'), /* View Display Title */
			'search_items' => __('Search Request'), /* Search Product Title */ 
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), 
			'description' => __( 'requests for '.get_bloginfo() ), /* Product Description */
			'public' => true,
			'menu_icon' => 'dashicons-format-status',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'requests', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title'),
	 	) /* end of options */
	); /* end of register post type */
}

function requests_meta_boxes(){
	add_meta_box( 'requests_meta_box',
        'requests Information',
        'display_requests_meta_box',
        'requests', 
        'normal', 
        'high'
    );
}

function display_requests_meta_box(){
	global $post;
	$email 	= get_post_meta($post->ID, 'email', true);
	$phone 	= get_post_meta($post->ID, 'phone', true);
	$desc 	= get_post_meta($post->ID, 'desc', true);
	$type 	= get_post_meta($post->ID, 'type', true);
?>
	<div id="date-time-box"></div>
	<table class="form-table">
		<tr>
			<th><label>Email</label></th>
			<td>
				<input type="text" name="email" value="<?php echo $email ?>" size="30">
			</td>
		</tr>
		<tr>
			<th><label>Phone</label></th>
			<td>
				<input type="text" name="phone" class="datepicker" value="<?php echo $phone ?>" size="30">
			</td>
		</tr>
		<tr>
			<th><label>Desc</label></th>
			<td>
				<textarea name="desc"><?php echo $desc ?></textarea>				
			</td>
		</tr>
		<tr>
			<th><label>Type</label></th>
			<td>
				<select name="type">
					<option value="contact" <?php selected( $type, 'contact', true); ?>>Contact</option>
					<option value="dat-e-base" <?php selected( $type, 'dat-e-base', true); ?>>Dat-e-base</option>
					<option value="consulting" <?php selected( $type, 'consulting', true); ?>>Consulting</option>
					<option value="best-of" <?php selected( $type, 'best-of', true); ?>>Best-of</option>
					<option value="turnkey" <?php selected( $type, 'turnkey', true); ?>>Turnkey</option>
					<option value="embed" <?php selected( $type, 'embed', true); ?>>Embed</option>
				</select>				
			</td>
		</tr>
	</table>
<?php 
}


function save_requests_custom_meta( $id, $item ) {
    // Check post type
    if ( $item->post_type == 'requests' ) {
        // Store data in post meta table if present in post data
        if ( isset( $_POST['email'] )  ) {
            update_post_meta( $id, 'email',$_POST['email'] );
        }
        if ( isset( $_POST['phone'] )  ) {
            update_post_meta( $id, 'phone',$_POST['phone'] );
        }
        if ( isset( $_POST['desc'] ) ) {
            update_post_meta( $id, 'desc',$_POST['desc'] );
        }
        if ( isset( $_POST['type'] ) ) {
            update_post_meta( $id, 'type',$_POST['type'] );
        }
    }
}

function form_submit() {
	if(is_plugin_active('pl-requests/pl-requests.php') && isset($_POST['form_data'])){
		parse_str($_POST['form_data'], $data);
		if(empty($data['name'])){
			echo 'Empty Name';
			exit;
		}
		$text = 'Name: '.$data['name'].', Email: '.$data['email'].', Phone: '.$data['phone'];
		$text.= isset($data['desc'])?', Description: '.$data['desc']:'';

		wp_mail( array(get_option('admin_email')),'New Request', $text );
		$id = wp_insert_post(array(
			'post_title' 	=> $data['name'],
			'post_status'	=> 'publish',
			'post_type'		=> 'requests'
		));
		if($id){
			if ( isset( $data['email'] )  ) {
	            update_post_meta( $id, 'email',$data['email'] );
	        }
	        if ( isset( $data['phone'] )  ) {
	            update_post_meta( $id, 'phone',$data['phone'] );
	        }
	        if ( isset( $data['desc'] ) ) {
	            update_post_meta( $id, 'desc',$data['desc'] );
	        }
	        if ( isset( $data['type'] ) ) {
	            update_post_meta( $id, 'type',$data['type'] );
	        }
		}
	}
	exit;
}
add_action( 'wp_ajax_form_submit', 'form_submit' );
add_action( 'wp_ajax_nopriv_form_submit', 'form_submit' );

add_action( 'init','create_pt_requests' );
add_action( 'add_meta_boxes','requests_meta_boxes' );
add_action( 'save_post','save_requests_custom_meta',10,2 );



add_action( 'admin_menu', 'register_requests_menu_page', 999 );

function register_requests_menu_page() {
    add_menu_page( 'Requests', 'Requests', 'manage_options', 'request', 'requests_custom_menu_page', 'dashicons-list-view', 27 );
}

function requests_custom_menu_page(){
	include plugin_dir_path(__FILE__).'/page-admin_table.php';
}



