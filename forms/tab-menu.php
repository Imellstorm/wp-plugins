<style type="text/css">
    .nav-tab:focus{
        box-shadow:none;
    }
    p.search-box{
        margin-left: 20px;
    }
</style>
<h3 class="nav-tab-wrapper" id="wpseo-tabs" style="margin-bottom:0">
    <?php 
        if(!isset($_GET['type'])||$_GET['type']=='sample_request'){
            $tabActive = 'nav-tab-active';
        } else {
            $tabActive = '';
        }
    ?>
    <a class="nav-tab <?php echo !isset($_GET['type'])||$_GET['type']=='sidebar_contacts'?'nav-tab-active':'' ?>" id="ui_sidebar_contacts" href="<?php echo  admin_url() ?>admin.php?page=forms&type=sidebar_contacts">Sidebar Contacts</a>
    <a class="nav-tab <?php echo isset($_GET['type'])&&$_GET['type']=='home_contacts'?'nav-tab-active':'' ?>" id="ui_home_contacts" href="<?php echo  admin_url() ?>admin.php?page=forms&type=home_contacts">Home Contacts</a>
</h3>
