jQuery(document).ready(function($){	
	$("#concerns_list").select2();
  $("#treatments_list").select2();
});

function showGalleryMediaUploader(element){
    var mediaUploader;

    // If the uploader object has already been created, reopen the dialog
     if (mediaUploader) {
      mediaUploader.open();
      return;
    }

    // Extend the wp.media object
    mediaUploader = wp.media.frames.file_frame = wp.media(
      {
        title: 'Choose Image',
        button: {
        text: 'Choose Image'
      }, 
      multiple: true 
    });

    mediaUploader.on('select', function() {
      selection = mediaUploader.state().get('selection');
      selection.map( function( attachment ) {
        attachment = attachment.toJSON();
        element.after('<span><img src="' +attachment.url+'" style="max-height:200px;margin:10px" class="gallery_image"><input value="'+attachment.url+'" type="hidden" class="gallery_image_url" name=gallery_image_urls[]><input value="'+attachment.id+'" type="hidden" class="gallery_image_id" name=gallery_image_id[]><i style="cursor:pointer;" class="remove_gallery_image_ico wp-menu-image dashicons-before dashicons-trash"></i></span>');
       });
    });

    // Open the uploader dialog
    mediaUploader.open();
}