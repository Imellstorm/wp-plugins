<?php
/*
Plugin Name: Alex Karev Galleries
Description: Custom content types for themes developed by Alex Karev
Version: 1.2
Author: Alex Karev
*/

function galleries_load_assets(){
	wp_enqueue_style( 'galleries_metabox-styles', plugins_url('/assets/css/metaboxes.css?v='.time(),__FILE__));
	wp_enqueue_style( 'galleries_jquery-select2', plugins_url('/assets/css/select2.css?v='.time(),__FILE__));
	wp_enqueue_script( 'galleries_select2-js', plugins_url('/assets/js/select2.min.js?v='.time(),__FILE__), array('jquery'), time(), false);
	wp_enqueue_script('galleries_custom-js', plugins_url('/assets/js/metaboxes.js?v='.time(),__FILE__), 'jquery-ui-core', '1.0', true);
}

function galleries_front_assets(){
	wp_enqueue_script( 'galleries_lightbox-js', plugins_url('/assets/js/lightbox.js?v='.time(),__FILE__), array('jquery'), time(), true);
	wp_enqueue_style( 'galleries_lightbox-styles', plugins_url('/assets/css/lightbox.css?v='.time(),__FILE__));
}

add_action( 'wp_footer', 'lightbox_options', 100 );

function lightbox_options(){
?>
	<script>
	    lightbox.option({
	      'resizeDuration': 200,
	      'wrapAround': true
	    })
	</script>
<?php
}

function create_pt_galleries() {
	register_post_type( 'galleries', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	array('labels' => array(
			'name' => __('Galleries'), /* This is the Title of the Group */
			'singular_name' => __('Gallery'), /* This is the individual type */
			'all_items' => __('All Galleries'), /* the all items menu item */
			'add_new' => __('Add New Gallery'), /* The add new menu item */
			'add_new_item' => __('Add New Gallery'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit Gallery'), /* Edit Display Title */
			'new_item' => __('New Gallery'), /* New Display Title */
			'view_item' => __('View Gallery'), /* View Display Title */
			'search_items' => __('Search Galleries'), /* Search Product Title */ 
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), 
			'description' => __( 'Galleries for '.get_bloginfo() ), /* Product Description */
			'public' => true,
			'menu_icon' => 'dashicons-format-gallery',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'galleries', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title','post-options','page-attributes', 'thumbnail', 'author',),
			'taxonomies' => array( 'post_tag' ),

	 	) /* end of options */
	); /* end of register post type */
}

function galleries_meta_boxes(){
	add_meta_box( 'galleries_meta_box',
        'Gallery content',
        'display_galleries_meta_box',
        'galleries', 
        'normal', 
        'high'
    );
}

function display_galleries_meta_box($post){
	$gallery_image_urls = esc_html( get_post_meta( $post->ID, 'gallery_image_urls', true ) );
    $gallery_image_id 	= esc_html( get_post_meta( $post->ID, 'gallery_image_id', true ) );
    $gallery_image_urls = json_decode(htmlspecialchars_decode($gallery_image_urls));
    $gallery_image_id 	= json_decode(htmlspecialchars_decode($gallery_image_id));

    $galleryConcerns = get_post_meta($post->ID, 'gallery_concerns', true);
	$concerns = get_posts( array (
		'post_type'	=> 'concerns',
		'posts_per_page' => -1
	));

	$galleryTreatments = get_post_meta($post->ID, 'gallery_treatments', true);
	$treatments = get_posts( array (
		'post_type'	=> 'treatments',
		'posts_per_page' => -1
	));
    ?>
    <script>
       jQuery(document).ready(function($){
            $('#add_gallery_image').on('click',function(){               
                showGalleryMediaUploader($(this));
            });
            
            $('body').on('click','.remove_gallery_image_ico',function(){
                $(this).parent().remove();
            })
        });
    </script>
    <span>Shortcode [related-gallery id="<?php echo $post->ID ?>"]</span>
    <input id="add_gallery_image" type="button" value="Add gallery image" style="margin:20px 10px; display:block">
    <?php  
    if(count($gallery_image_urls)){
        foreach ($gallery_image_urls as $key => $gallery_image_url) {
            if(!empty($gallery_image_url) && isset($gallery_image_id[$key])){ ?>
            <span>
                <img style="max-height:200px;margin:10px" class="gallery_image" src="<?php echo $gallery_image_url ?>">
                <input type="hidden" class="gallery_image_url" name=gallery_image_urls[] value="<?php echo $gallery_image_url ?>">
                <input type="hidden" class="gallery_image_id" name=gallery_image_id[] value="<?php echo $gallery_image_id[$key] ?>">
                <i style="cursor:pointer" class="remove_gallery_image_ico wp-menu-image dashicons-before dashicons-trash"></i>
            </span>
        <?php } 
        }
    } ?>
    <table class="form-table">
		<?php if( post_type_exists('concerns') ): ?>
			<tr>
				<th><label for="concerns_list">Concerns</label></th>
				<td>
					<select name="gallery_concerns[]" id="concerns_list" multiple="multiple">
						<option value="">Select One</option>
						<?php foreach($concerns as $child ): ?>
							<option value="<?php echo $child->ID ?>", <?php echo $galleryConcerns&&in_array($child->ID,$galleryConcerns)?' selected="selected"':''?> ><?php echo $child->post_title ?></option>;
						<?php endforeach ?>
					</select>
					<br /><span class="description">The specific concerns that are treated by this gallery.</span>
				</td>
			</tr>
		<?php endif ?>

		<?php if( post_type_exists('treatments') ): ?>
			<tr>
				<th><label for="treatments_list">Treatments</label></th>
				<td>
					<select name="gallery_treatments[]" id="treatments_list" multiple="multiple">
						<option value="">Select One</option>
						<?php foreach($treatments as $child ): ?>
							<option value="<?php echo $child->ID ?>", <?php echo $galleryTreatments&&in_array($child->ID,$galleryTreatments)?' selected="selected"':''?> ><?php echo $child->post_title ?></option>;
						<?php endforeach ?>
					</select>
					<br /><span class="description">The specific treatments that are treated by this gallery.</span>
				</td>
			</tr>
		<?php endif ?>
	</table>

<?php }

function save_galleries_custom_meta( $id, $item ) {
	if( $item->post_type == 'galleries' ){
        delete_post_meta($id,'gallery_image_urls');
        delete_post_meta($id,'gallery_image_id');
        if ( isset( $_POST['gallery_image_urls'] ) && !empty($_POST['gallery_image_urls']) ) {
            update_post_meta( $id, 'gallery_image_urls', json_encode($_POST['gallery_image_urls']) );
            update_post_meta( $id, 'gallery_image_id', json_encode($_POST['gallery_image_id']) );
        }
		if (post_type_exists('concerns')) {
	        if (!empty($_POST['gallery_concerns'])) {
		        update_post_meta( $id, 'gallery_concerns',$_POST['gallery_concerns'] );
		        updateGalleriesLinkedResources($_POST['gallery_concerns'],$id,'concern_galleries');
		    } else {
	    		delete_post_meta($id, 'gallery_concerns');
	    	}
	    }
	    if (post_type_exists('treatments')) {
	    	if (!empty($_POST['gallery_treatments'])) {
		        update_post_meta( $id, 'gallery_treatments',$_POST['gallery_treatments'] );
		        updateGalleriesLinkedResources($_POST['gallery_treatments'],$id,'treatment_galleries');
		    } else {
	    		delete_post_meta($id, 'gallery_treatments');
	    	}
	    }
	}
}

function updateGalleriesLinkedResources($linkedIds,$currentId,$type){
    foreach ($linkedIds as $id) {
    	$meta = get_post_meta($id,$type,true);
    	if(empty($meta) || !in_array($currentId,$meta)){
    		$meta[] = $currentId;
    		update_post_meta($id,$type,$meta);
    	}
    }
}

function galleries_shortcode( $atts ){ 
	if(isset($atts['id']) && !empty($atts['id'])){
		$images_ids = get_post_meta($atts['id'], 'gallery_image_id', true);
		$images_ids = json_decode($images_ids);
	} ?>
	<?php if(count($images_ids)): ?>
		<?php foreach ($images_ids as $image_id): ?>
			<a href="" class="btn btn-blue" style="margin:10px">
				<img src="<?php echo wp_get_attachment_image_src( $image_id )[0] ?>">
			</a>
		<?php endforeach ?>
 	<?php endif ?>	
<?php }

add_action( 'admin_enqueue_scripts','galleries_load_assets' );
add_action( 'wp_enqueue_scripts','galleries_front_assets' );
add_action( 'init','create_pt_galleries' );
add_action( 'add_meta_boxes','galleries_meta_boxes' );
add_action( 'save_post','save_galleries_custom_meta',10,2 );
add_shortcode( 'related-gallery', 'galleries_shortcode' );
