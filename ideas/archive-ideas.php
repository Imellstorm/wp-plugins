<?php get_header(); ini_set('display_errors',1);?>

<?php add_idea_modal() ?>

<script>
jQuery(document).ready(function($){
    $('.popup_comment_link').on('click',function(){
        $('#add_comment_form .post_id').val($(this).find('.post_id').val())
        coments_count = $(this).find('.comments_count_number')
    })

    $('.submit-comment-button').on('click',function(){
        var form = new FormData($('#add_comment_form').get(0))
        form.append( 'action', 'add_comment' )
        $.ajax({            
          type: "POST",
          url: '<?php echo admin_url("admin-ajax.php") ?>',
          data: form,
          contentType: false,
          processData: false,
          dataType: "json",
          success: function ( response ) {
            $('#commentModal').modal('hide') 
            $('#add_comment_form').get(0).reset()
            $('.idea_info_block span').html(response.html)
            $('.idea_info_block').show()
            coments_count = coments_count.text(parseInt(coments_count.text())+1)
          }
        })
    })

    $('.idea_popup').on('click',function(){
        $('#idea_modal').modal('show');
        var idea_id = $(this).data('ideaid');
        var idea_text = $('.idea_text_'+idea_id).html();
        tinyMCE.get("editor_idea").setContent(idea_text);

        $('.edit-idea-button').on('click',function(){
           $.ajax({
                type: "POST",
                url: '<?php echo admin_url("admin-ajax.php") ?>',
                data: {
                    action: 'edit_ideas',
                    idea_id: idea_id,
                    idea_text: tinyMCE.get("editor_idea").getContent()
                },
                success: function() {
                    $('#idea_modal').modal("hide");
                    $('.idea_text_'+idea_id).html(tinyMCE.get("editor_idea").getContent());
                }
            })
        });
    })

    $('.idea_favorite').on('click',function(){
        elem = $(this)
        if(elem.find('.favorite-star').hasClass('fa-star-o')){
            var status = 1
        } else {
            var status = 0
        }
        var data = {
            action: 'idea_favorits',
            status: status,
            post_id: elem.find('#post_id').val()
        };
        jQuery.post( "<?php echo admin_url('admin-ajax.php') ?>" , data, function(response) {
            if(response.success){
                var favorites_count = elem.find('.favorites_count').text()
                if(elem.find('.favorite-star').hasClass('fa-star-o')){
                    elem.find('.favorite-star').removeClass('fa-star-o')
                    elem.find('.favorite-star').addClass('fa-star')
                    elem.find('.favorites_count').text(parseInt(favorites_count)+1)
                } else {
                    elem.find('.favorite-star').addClass('fa-star-o')
                    elem.find('.favorite-star').removeClass('fa-star')
                    elem.find('.favorites_count').text(favorites_count-1)
                }
            }
        }, 'json');
    })
})
</script>

<div class="modal fade" id="commentModal" tabindex="-1" role="dialog"  style="display:none">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Add Comment</h4>
      </div>
      <div class="modal-body">
        <?php if(is_user_logged_in ()): ?>
            <form id="add_comment_form">
                <div class="form-group">
                    <lavel>Comment text</lavel>
                    <textarea name="text" id="comment_text" class="form-control" style="max-width:100%"></textarea>
                    <input type='hidden' name="post_id" class="post_id">
                </div>
            </form>
        <?php else: ?>
            <div class="form-group">
                To post a comment please login
            </div>
        <?php endif ?>
      </div>
      <div class="modal-footer" style="margin-top:0">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <?php if(is_user_logged_in ()): ?>
            <button type="button" class="btn btn-primary submit-comment-button">Add</button>
        <?php endif ?>
      </div>
    </div>
  </div>
</div>

<?php if(is_user_logged_in () && is_author_idea() ): ?>
    <div class="modal fade" id="idea_modal" tabindex="-1" role="dialog"  style="display:none; outline: none;">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit Idea</h4>
          </div>
          <div class="modal-body">
            <?php wp_editor( "", "editor_idea" ); ?>
          </div>
          <div class="modal-footer" style="margin-top:0">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary edit-idea-button">Save</button>
          </div>
        </div>
      </div>
    </div>
<?php endif; ?>

<?php
//Specific class for post listing */
$blog_type = sq_option('blog_type','masonry');
$blog_type = apply_filters( 'kleo_blog_type', $blog_type );

$template_classes = $blog_type . '-listing';
if ( sq_option( 'blog_meta_status', 1 ) == 1 ) {
    $template_classes .= ' with-meta';
} else {
    $template_classes .= ' no-meta';
}
if ( $blog_type == 'standard' && sq_option('blog_standard_meta', 'left' ) == 'inline' ) {
    $template_classes .= ' inline-meta';
}
add_filter('kleo_main_template_classes', create_function('$cls','$cls .=" posts-listing ' . $template_classes . '"; return $cls;'));
?>

<?php get_template_part('page-parts/general-title-section'); ?>

<?php get_template_part('page-parts/general-before-wrap'); ?>

<?php if ( category_description() ) : ?>
    <div class="archive-description"><?php echo category_description(); ?></div>
<?php endif; ?>

<div class="row sabai-questions-search idea_search_box">
    <div class="col-md-3">
        <div class="idea_search">
            <select class="idea_sort" onchange="window.location.href='?sort='+jQuery('.idea_sort option:selected').val()">
                <option value="newest" <?php echo $_GET['sort']=='newest'?'selected':''?> >Newest First</option>
                <option value="oldest" <?php echo $_GET['sort']=='oldest'?'selected':''?> >Oldest First</option>
                <option value="recent" <?php echo $_GET['sort']=='recent'?'selected':''?> >Recently Active</option>
                <option value="voted" <?php echo $_GET['sort']=='voted'?'selected':''?> >Most Votes</option>
                <option value="answers" <?php echo $_GET['sort']=='answers'?'selected':''?> >Most Answers</option>
            </select>
        </div>
    </div>
    <form>
        <div class="col-md-5">
            <input type="text" name="s" class="idea_search_input">
            <input type="hidden" name="type" value="ideas">
        </div>
        <div class="col-md-2">
            <button type="submit" class="btn btn-info idea_search" style="width:100%">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </form>
    <div class="col-md-2"><?php add_idea_button() ?></div>
</div>

<?php if ( have_posts() ) : ?>

    <div class="sabai-row ideas-list">
        <div class="sabai-questions-questions sabai-col-md-12">

            <?php while ( have_posts() ) : the_post(); ?>

                <?php $author_id = get_the_author_meta('ID'); ?>

                <div style="padding:10px" class="sabai-entity sabai-entity-type-content sabai-entity-bundle-name-questions sabai-entity-bundle-type-questions sabai-entity-mode-summary sabai-questions-novotes sabai-clearfix">
                    <div class="sabai-row">
                        <div class="sabai-col-xs-2 sabai-questions-side">
                            <?php if(function_exists('kleo_item_likes')): ?>
                            <div class="sabai-questions-vote-count idea_vote" user_id=<?php the_author_meta( 'ID' ); ?>>
                                <?php $item_votes = get_post_meta(get_the_ID(),'_item_likes') ?>  
                                    <?php kleo_item_likes(get_the_ID()) ?>        
                                </div>
                            <?php endif ?>
                            <div class="sabai-questions-answer-count">
                                <a data-toggle="modal" class="popup_comment_link" data-target="#commentModal" style="cursor:pointer">
                                    <?php $user_comments = get_comments(array('author__in'=>array(get_current_user_id()),'post_id'=>get_the_ID())); ?>
                                    <span class="sabai-number" <?php echo count($user_comments)?'style="color:#00E000"':'' ?> >
                                        <div class="comments_count_number" style="display:inline"><?php echo get_comments_number() ?></div>
                                        <i class="fa fa-comments"></i>
                                        <input type='hidden' class="post_id" value="<?php echo get_the_ID() ?>">
                                    </span>
                                </a>
                            </div>
                            <?php 
                                $idea_status = get_post_meta($post->ID, 'idea_status', true);
                                switch ($idea_status) {
                                    case 'in discussion':
                                        $idea_status_class = 'btn-info';
                                        break;
                                    case 'selected':
                                        $idea_status_class = 'btn-primary';
                                        break;
                                    case 'rejected':
                                        $idea_status_class = 'btn-danger';
                                        break;
                                    case 'in project':
                                        $idea_status_class = 'btn-success';
                                        break;
                                    default:
                                        $idea_status_class = '';
                                } 
                            ?>
                            <?php if($idea_status): ?>
                                <div class="text-center <?php echo $idea_status_class ?> status_tag"><?php echo $idea_status ?></div> 
                            <?php endif ?>
                            <div class="sabai-questions-view-count">
                                <?php $views_count = get_post_meta(get_the_ID(),'views_count')[0] ?>
                                <span class="sabai-number"><?php echo $views_count?$views_count:0 ?></span> views  
                                <?php if(is_user_logged_in()): ?>
                                    <span class="idea_favorite">
                                        <?php $favorites_meta = get_post_meta(get_the_id(),'idea_favorites') ?>
                                        <span class="text-center"><i class="favorite-star fa-lg fa <?php echo in_array(get_current_user_id(),$favorites_meta)?'fa-star':'fa-star-o' ?>"></i>
                                        </span>
                                        <span class="text-center favorites_count"><?php echo count($favorites_meta) ?></span>
                                        <input type="hidden" id="post_id" value="<?php the_ID() ?>"> 
                                    </span>
                                <?php endif ?>
                            </div>
                        </div>
                        <div class="sabai-col-xs-10 sabai-questions-main">
                            <div class="sabai-questions-title">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class=" sabai-entity-permalink sabai-entity-id-424 sabai-entity-type-content sabai-entity-bundle-name-questions sabai-entity-bundle-type-questions">
                                    <span class="idea-avatar"><?php echo get_avatar( $author_id, '32' );  ?></span>
                                    <?php include_once('wp-admin/includes/plugin.php'); ?>
                                    <?php if(is_plugin_active('mycred/mycred.php')): ?>
                                        <span class="idea_user_points"><?php echo mycred_get_users_cred( $author_id ) ?> <?php echo mycred()->plural() ?></span>
                                    <?php endif ?>
                                    <?php the_title(); ?>
                                </a> 
                                <?php $idea_image_id = get_post_meta(get_the_ID(),'idea_image_id',true) ?>
                                <?php $idea_file_id = get_post_meta(get_the_ID(),'idea_file_id',true) ?>
                                <?php if($idea_image_id || $idea_file_id): ?>
                                    <i class="fa fa-file-o" style="margin-left: 10px"></i>
                                <?php endif ?>           
                            </div>
                          
                            <div class="sabai-questions-body idea_text_<?php the_ID(); ?>">
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="sabai-questions-custom-fields">
                            </div>

                            <div class="sabai-questions-activity sabai-questions-activity-inline">
                                <ul class="sabai-entity-activity">
                                    <li>
                                        <a href="<?php echo bp_core_get_user_domain( $author_id ); ?>" class="sabai-user sabai-user-with-thumbnail" rel="nofollow" data-popover-url="http://marylink.appteka.cc/sabai/user/profile/jcantenot">
                                            <?php echo bp_core_get_user_displayname($author_id) ?>
                                        </a> 
                                        posted 
                                        <span title="<?php get_the_date(); ?>">
                                        <?php echo human_time_diff( strtotime( get_the_date('Y-m-d H:i:s')), time() ); ?>
                                        ago
                                        </span>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o"></i>
                                        last active 
                                        <?php echo human_time_diff( strtotime( get_user_meta( $author_id, 'last_activity', true )), time() ); ?>
                                        ago
                                    </li>
                                    <?php if(is_user_logged_in () && is_author_idea() ): ?>
                                    <li>
                                        <a class="idea_popup" data-ideaid="<?php the_ID();?>" style="cursor:pointer">
                                            <i class="fa fa-edit"></i> Edit
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                </ul>
                                <div class="pull-right">
                                    <?php $tags = wp_get_post_tags( get_the_ID()) ?>
                                    <?php if(count($tags)):?>
                                        <?php foreach($tags as $tag):?>
                                            <div class="idea_tag"><?php echo $tag->name ?></div>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </div>                                          
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>

        </div>
    </div>


    <?php
    // page navigation.
    kleo_pagination();

else :
    // If no content, include the "No posts found" template.
    get_template_part( 'content', 'none' );

endif;
?>

<?php get_template_part('page-parts/general-after-wrap'); ?>

<?php get_footer(); ?>