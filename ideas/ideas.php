<?php
/*
Plugin Name: Ideas plugin
Description: Custom Ideas plugin
Version: 1.0
Author: Alex Karev
*/

function ideas_load_assets(){
	wp_enqueue_style( 'ideas_metabox-styles', plugins_url('/assets/css/metaboxes.css?v='.time(),__FILE__));
	wp_enqueue_script('ideas_custom-js', plugins_url('/assets/js/metaboxes.js?v='.time(),__FILE__), 'jquery-ui-core', '1.0', true);
}

function ideas_front_assets(){
	wp_enqueue_style( 'ideas_front-styles', plugins_url('/assets/css/style.css?v='.time(),__FILE__));
	wp_enqueue_script('ideas_front-js', plugins_url('/assets/js/ideas-scripts.js?v='.time(),__FILE__), 'jquery', '1.0', true);
}
add_action( 'wp_enqueue_scripts','ideas_front_assets' );

add_action('wp_head','hook_js');
function hook_js() { ?>
	<script type="text/javascript">
		jQuery(document).ready(function($){   
			$('.idea_vote .item-likes').on('click',function(){
					console.log('123s')
				var user_id = $(this).parent().attr('user_id') 
				if(!$(this).hasClass('liked')){
					$.ajax({            
						type: "POST",
						url: '<?php echo admin_url("admin-ajax.php") ?>',
						data: {
							'action': 'add_like_points',
							'user_id': user_id
						},
						success: function ( response ) {
						}
			        })
				}
			})
		});
	</script>
<?php }

add_action( 'wp_ajax_add_like_points', 'add_like_points_callback' );
//add_action( 'wp_ajax_nopriv_add_idea', 'add_idea_callback' );
function add_like_points_callback(){
	if($_POST['user_id']){
		include_once('wp-admin/includes/plugin.php');
        if(is_plugin_active('mycred/mycred.php')){
        	$myCred_points_for_like = 15;
			mycred_add( 'idea_like', $_POST['user_id'], $myCred_points_for_like, 'Idea like %plural%!' );
		}
	}
	exit;
}

function add_views_count($single_template){
	global $post;
	if($post->post_type == 'ideas'){
		$views_count = get_post_meta($post->ID,'views_count');

		if(!isset($views_count[0])){
			add_post_meta($post->ID, 'views_count', 0);
		} else {
			update_post_meta($post->ID, 'views_count', $views_count[0]+1, $views_count[0]);
		}
		//delete_post_meta($post->ID, 'views_count');
	}
	return $single_template;
}
add_filter( "single_template", "add_views_count" );

function create_pt_ideas() {
	register_post_type( 'ideas', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	array('labels' => array(
			'name' => __('Ideas'), /* This is the Title of the Group */
			'singular_name' => __('Idea'), /* This is the individual type */
			'all_items' => __('All Ideas'), /* the all items menu item */
			'add_new' => __('Add New Idea'), /* The add new menu item */
			'add_new_item' => __('Add New Idea'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit Idea'), /* Edit Display Title */
			'new_item' => __('New Idea'), /* New Display Title */
			'view_item' => __('View Idea'), /* View Display Title */
			'search_items' => __('Search Idea'), /* Search Product Title */ 
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), 
			'description' => __( 'Homepage Ideas for '.get_bloginfo() ), /* Product Description */
			'public' => true,
			'menu_icon' => 'dashicons-format-status',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'Ideas', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title','post-options','editor','page-attributes','comments'),
			'taxonomies' => array( 'post_tag'),
	 	) /* end of options */
	); /* end of register post type */
}

add_action( 'init', 'create_ideas_cat_tax' );

function create_ideas_cat_tax() {
	$args = array(
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'ideas_category' ),
	);
	register_taxonomy( 'ideas_category', array( 'ideas' ), $args );
}

function ideas_meta_boxes(){
	add_meta_box( 'ideas_meta_box',
        'Idea settings',
        'display_ideas_meta_box',
        'ideas', 
        'normal', 
        'high'
    );
}

function display_ideas_meta_box(){
	global $post;
	$idea_image_url = get_post_meta($post->ID, 'idea_image_url', true);
	$idea_image_id = get_post_meta($post->ID, 'idea_image_id', true);
	$idea_file_url = get_post_meta($post->ID, 'idea_file_url', true);
	$idea_file_id = get_post_meta($post->ID, 'idea_file_id', true);
	$idea_status = get_post_meta($post->ID, 'idea_status', true);
	$idea_campaign = get_post_meta($post->ID, 'idea_campaign', true);
	$idea_youtube = get_post_meta($post->ID, 'idea_youtube', true);
?>
    <script>
       jQuery(document).ready(function($){
            $('#add_image_button').on('click',function() {
                showMediaUploader($('#idea_image_url'),$('#idea_image_id'),$('.idea_image_img'));
            });
            $('#clear_image_button').on('click',function() {
            	$('#idea_image_url').attr('value','');
            	$('#idea_image_id').attr('value','');
            	$('.idea_image_img').attr('src','<?php echo plugins_url('/assets/img/noimage.png',__FILE__) ?>' );
            })

            $('#add_file_button').on('click',function() {
                showMediaUploader($('#idea_file_url'),$('#idea_file_id'),false);
            });
            $('#clear_file_button').on('click',function() {
            	$('#idea_file_url').attr('value','');
            })
        });
    </script>
	<table class="form-table">
		<tr>
			<th><label for="ideas_list">Image</label></th>
			<td>
				<img src="<?php echo $idea_image_url?$idea_image_url:plugins_url('/assets/img/noimage.png',__FILE__) ?>" class="idea_image_img" style="float:left; margin-right:20px; max-width: 100px;">
				<label for="idea_image_url">
					<input id="idea_image_url" type="text" size="36" name="idea_image_url" value="<?php echo $idea_image_url ?>" readonly/>
					<div style="margin:10px 0">Enter a URL or upload an image</div>
					<input id="idea_image_id" type="hidden" size="36" name="idea_image_id" value="<?php echo $idea_image_id ?>" />
					<input id="add_image_button" class="button button-primary" type="button" value="Add Image" />
					<input id="clear_image_button" class="button button-default" type="button" value="Clear" />
				</label>
			</td>
		</tr>
		<tr>
			<th><label for="ideas_list">Attachment</label></th>
			<td>
				<label for="upload_Idea">
					<input id="idea_file_url" type="text" size="36" name="idea_file_url" value="<?php echo $idea_file_url ?>" readonly/>
					<input id="add_file_button" class="button button-primary" type="button" value="Add File" />
					<input id="idea_file_id" type="hidden" size="36" name="idea_file_id" value="<?php echo $idea_file_id ?>" />
					<input id="clear_file_button" class="button button-default" type="button" value="Clear" />
				</label>
			</td>
		</tr>
		<tr>
			<th><label for="ideas_list">Status</label></th>
			<td>
				<select name="idea_status">
					<option value="" >No status</option>
					<option value="in discussion" <?php echo $idea_status=='in discussion'?'selected':'' ?> >Idea in discussion</option>
					<option value="selected" <?php echo $idea_status=='selected'?'selected':'' ?> >Idea selected</option>
					<option value="rejected" <?php echo $idea_status=='rejected'?'selected':'' ?> >Idea rejected</option>
					<option value="in project" <?php echo $idea_status=='in project'?'selected':'' ?> >Idea in project</option>
				</select>
			</td>
		</tr>
		<tr>
			<th><label for="idea_youtube">Video (youtube)</label></th>
			<td>
				<input id="idea_youtube" type="text" size="36" name="idea_youtube" value="<?php echo $idea_youtube ?>" />
			</td>
		</tr>
	</table>
<?php 
}


function save_ideas_custom_meta( $id, $item ) {
    // Check post type
    if ( $item->post_type == 'ideas' ) {
        // Store data in post meta table if present in post data
        if ( isset($_POST['idea_image_url']) ) {
            update_post_meta( $id, 'idea_image_url',$_POST['idea_image_url'] );
        }
        if ( isset($_POST['idea_image_id']) ) {
            update_post_meta( $id, 'idea_image_id',$_POST['idea_image_id'] );
        }
        if ( isset($_POST['idea_file_url']) ) {
            update_post_meta( $id, 'idea_file_url',$_POST['idea_file_url'] );
        }
        if ( isset($_POST['idea_file_id']) ) {
            update_post_meta( $id, 'idea_file_id',$_POST['idea_file_id'] );
        }
        if ( isset($_POST['idea_status']) ) {
            update_post_meta( $id, 'idea_status',$_POST['idea_status'] );
        }
        if ( isset($_POST['idea_youtube']) ) {
            update_post_meta( $id, 'idea_youtube',$_POST['idea_youtube'] );
        }
    }
}

add_action( 'admin_enqueue_scripts','ideas_load_assets' );
add_action( 'init','create_pt_ideas' );
add_action( 'add_meta_boxes','ideas_meta_boxes' );
add_action( 'save_post','save_ideas_custom_meta',10,2 );

function ideas_custom_excerpt_length( $length ) {
	return 25;
}
add_filter( 'excerpt_length', 'ideas_custom_excerpt_length', 999 );

//insert new idea
add_action( 'wp_ajax_add_idea', 'add_idea_callback' );
add_action( 'wp_ajax_nopriv_add_idea', 'add_idea_callback' );
function add_idea_callback(){
	if($_POST){
		$postarr = array(
			'post_title'	=> $_POST['title'],
			'post_content'	=> $_POST['text'],			
			'post_type' 	=> 'ideas',
		);
		if(is_user_logged_in ()){
			$postarr['post_status'] = 'publish';
		}
		$post_id = wp_insert_post($postarr);

		if($post_id){
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			if($_FILES['image']['name']){
				add_attachment($_FILES['image'],$post_id,'image');
			}
			if($_FILES['attachment']['name']){
				add_attachment($_FILES['attachment'],$post_id,'file');
			}
			if($_POST['idea_campaign']){
				add_post_meta($_POST['idea_campaign'],'campaign_ideas',$post_id);
			}
			if($_POST['youtube']){
				add_post_meta( $post_id, 'idea_youtube', $_POST['youtube'], true );
			}
			if(!is_user_logged_in()){
				$result = 'Thank you. Idea will appear after moderation.';
			} else {
				$result = '<a href="'.get_permalink($post_id).'">Thank you. Idea successfully added.</a>';
			}
			echo json_encode(array('html'=>$result));
		}
	} else {
		echo json_encode(array('error'=>'Idea not added!'));
	}
    exit;
}

//idea favorites
add_action( 'wp_ajax_idea_favorits', 'idea_favorits_callback' );

function idea_favorits_callback(){
	$meta = get_post_meta($_POST['post_id'],'idea_favorites');
	$user_id = get_current_user_id();
	if($meta){
		if(!in_array($user_id,$meta) && $_POST['status']){
			add_post_meta($_POST['post_id'],'idea_favorites',$user_id);
		}
		if(in_array($user_id,$meta) && !$_POST['status']){
			delete_post_meta($_POST['post_id'],'idea_favorites',$user_id);
		}
	} else {
		add_post_meta($_POST['post_id'],'idea_favorites',$user_id);
	}
	echo json_encode(array('success'=>true));
	exit;
}


//idea comments
add_action( 'wp_ajax_add_comment', 'add_comment_callback' );
function add_comment_callback(){
	global $current_user;
	$commentdata = array(
		'comment_post_ID'		=> $_POST['post_id'],
		'comment_content'		=> $_POST['text'],
		'user_id' 				=> $current_user->ID,
		'comment_author_email'	=> $current_user->user_email,
		'comment_author_url'	=> site_url(),
	);
	wp_new_comment( $commentdata );
	echo json_encode(array('html'=>'Comment successfully added!'));
	exit;
}

add_action('wp_ajax_edit_ideas', 'edit_idea_callback');
function edit_idea_callback(){
	global $current_user;
    $post = get_post( $_POST['idea_id'] );
    if( $post->post_author != $current_user->ID ) exit();

	$my_post = array(
	    'ID'           => $_POST['idea_id'],
	    'post_content' => $_POST['idea_text']
	);
	wp_update_post( $my_post );
	exit;
}

add_action('wp_ajax_edit_comments', 'edit_comments_callback');
function edit_comments_callback(){
	global $current_user;
	$comment_id = get_comment( $_POST['comment_id'] ); 
	if( $comment_id->user_id != $current_user->ID ) exit();

	$my_post = array(
	    'comment_ID'      => $_POST['comment_id'],
	    'comment_content' => $_POST['comment_text']
	);
	wp_update_comment( $my_post );
	exit;
}

function is_author_idea() {
	global $current_user;
    $post = get_post(get_the_ID());
    if( $post->post_author == $current_user->ID ) return true;
    return false;
}

function add_attachment($file_input,$post_id,$type){
	$file = wp_handle_upload($file_input,array('test_form' => false));
	if(isset($file['error'])){
		echo json_encode(array('html'=>$file['error']));
		exit;
	}
	$attach_id = wp_insert_attachment(
		array(
			'post_title' => $file_input['name'],
			'post_content' => '',
			'post_type' => 'attachment',
			'post_parent' => $post_id,
			'post_mime_type' => $file_input['type'],
			'guid' => $file['url']
		),
		$file['file']
	);
	$attach_data = wp_generate_attachment_metadata( $attach_id, $file['file'] );
	wp_update_attachment_metadata( $attach_id, $attach_data );

	update_post_meta($post_id,'idea_'.$type.'_id',$attach_id);
	update_post_meta($post_id,'idea_'.$type.'_url',$file['url']);

	return true;
}

//Frontend functions
function add_idea_button(){ 
	require( plugin_dir_path( __FILE__ ).'templates/idea_button.php' );
}

function add_idea_modal($current_campaign=false){
	require( plugin_dir_path( __FILE__ ).'templates/idea_modal.php' );
}

add_filter('pre_get_posts','ideasSearchFilter');
function ideasSearchFilter($query) {
	$post_type = $_GET['type'];
	if ($post_type && $query->is_search) {
        $query->set('post_type', $post_type);
	}
    return $query;
};

//ideas sorter
add_filter('pre_get_posts', 'ideasOrdering');
function ideasOrdering($wp_query) {
	if($_GET['sort'] && $wp_query->query['post_type']=='ideas'){
		switch ($_GET['sort']) {
			case 'newest':
				$wp_query->set('orderby', 'date');
				break;
			case 'oldest':
				$wp_query->set('oldest', 'date');
				$wp_query->set('order', 'ASC');
				break;
			case 'recent':
				add_filter( 'posts_clauses', 'ideas_intercept_query_clauses', 20, 1 );
				break;
			case 'voted':
				$wp_query->set('orderby','meta_value');
				$wp_query->set('meta_key','_item_likes');
				break;
			case 'answers':
				$wp_query->set('orderby','comment_count');
				break;
		}
	}
}

function ideas_intercept_query_clauses( $pieces ) {
    global $wpdb;
    $pieces['fields'] = "wp_posts.*,
    (
        select max(comment_date)
        from " . $wpdb->comments ." wpc
        where wpc.comment_post_id = wp_posts.id AND wpc.comment_approved = 1
    ) as mcomment_date";
    $pieces['orderby'] = "mcomment_date desc";
    return $pieces;
}


//Plugin info
add_filter('plugins_api','ideas_plugin_info',10,3);
function ideas_plugin_info($false, $action, $arg){
    if($arg->slug === 'ideas'){
    	exit ('<div style="margin:40px">Ideas Plugin. Any info here.</div>');
    }
    return false;
}





