<?php get_header(); ?>

<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.idea_favorite').on('click',function(){
            if($('.favorite-star').hasClass('fa-star-o')){
                var status = 1
            } else {
                var status = 0
            }
            var data = {
                action: 'idea_favorits',
                status: status,
                post_id: $('#post_id').val()
            };
            jQuery.post( "<?php echo admin_url('admin-ajax.php') ?>" , data, function(response) {
                if(response.success){
                    var favorites_count = $('.favorites_count').text()
                    if($('.favorite-star').hasClass('fa-star-o')){
                        $('.favorite-star').removeClass('fa-star-o')
                        $('.favorite-star').addClass('fa-star')
                        $('.favorites_count').text(parseInt(favorites_count)+1)
                    } else {
                        $('.favorite-star').addClass('fa-star-o')
                        $('.favorite-star').removeClass('fa-star')
                        $('.favorites_count').text(favorites_count-1)
                    }
                }
            }, 'json');
        })
        
        $('.comment_popup').on('click',function(){
            $('#comment_modal').modal('show');
            var comment_id = $(this).data('commentid');
            var comment_text = $(".comment_text_"+comment_id).html();
            tinyMCE.get("editor_comment").setContent(comment_text);

            $('.edit-comment-button').on('click',function(){
               $.ajax({
                    type: "POST",
                    url: '<?php echo admin_url("admin-ajax.php") ?>',
                    data: {
                        action: 'edit_comments',
                        comment_id: comment_id,
                        comment_text: tinyMCE.get("editor_comment").getContent()
                    },
                    success: function() {
                        $('#comment_modal').modal("hide");
                        $(".comment_text_"+comment_id).html(tinyMCE.get("editor_comment").getContent());
                    }
                })
            });
        })

        $('.idea_popup').on('click',function(){
            $('#idea_modal').modal('show');
            var idea_id = $(this).data('ideaid');
            var idea_text = $('.article-content').html();
            tinyMCE.get("editor_idea").setContent(idea_text);

            $('.edit-idea-button').on('click',function(){
               $.ajax({
                    type: "POST",
                    url: '<?php echo admin_url("admin-ajax.php") ?>',
                    //dataType: 'json',
                    data: {
                        action: 'edit_ideas',
                        idea_id: idea_id,
                        idea_text: tinyMCE.get("editor_idea").getContent()
                    },
                    success: function() {
                        $('#idea_modal').modal("hide");
                        $('.article-content').html(tinyMCE.get("editor_idea").getContent());
                    }
                })
            });
        })

    })
</script>

<?php if(is_user_logged_in ()): ?>
<div class="modal fade" id="comment_modal" tabindex="-1" role="dialog"  style="display:none; outline: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Comment</h4>
      </div>
      <div class="modal-body">
        <?php wp_editor( "", "editor_comment" ); ?>
      </div>
      <div class="modal-footer" style="margin-top:0">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary edit-comment-button">Save</button>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>

<?php if(is_user_logged_in () && is_author_idea() ): ?>
    <div class="modal fade" id="idea_modal" tabindex="-1" role="dialog"  style="display:none; outline: none;">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit Idea</h4>
          </div>
          <div class="modal-body">
            <?php wp_editor( "", "editor_idea" ); ?>
          </div>
          <div class="modal-footer" style="margin-top:0">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary edit-idea-button">Save</button>
          </div>
        </div>
      </div>
    </div>
<?php endif; ?>


<?php
//Specific class for post listing */
if ( kleo_postmeta_enabled() ) {
    $meta_status = ' with-meta';
    add_filter( 'kleo_main_template_classes', create_function( '$cls','$cls .= "'.$meta_status.'"; return $cls;' ) );
}

/* Related posts logic */
$related = sq_option( 'related_posts', 1 );
if ( ! is_singular('post') ) {
    $related = sq_option( 'related_custom_posts', 0 );
}
//post setting
if(get_cfield( 'related_posts') != '' ) {
    $related = get_cfield( 'related_posts' );
}
?>

<?php get_template_part( 'page-parts/general-title-section' ); ?>

<?php get_template_part( 'page-parts/general-before-wrap' );?>

<div class="ideas-list single_idea">
<?php /* Start the Loop */ ?>
<?php while ( have_posts() ) : the_post(); ?>
    <?php 
        $args = array(
        'post_type'  => 'campaigns',
        'meta_query' => array(
            array(
                    'key'     => 'campaign_ideas',
                    'value'   => get_the_ID(),
                    'compare' => '=',
                ),
            ),
        );
    $query = new WP_Query( $args );
    $campaign = $query->get_posts();
    ?>
    <?php if(isset($campaign[0])&&!empty($campaign[0])): ?>
        <div class="idea_campaign_info top_box">
            <?php $cmp_image_id = get_post_meta($campaign[0]->ID,'campaign_image_id',true) ?>
            <?php if($cmp_image_id): ?>
                <div class="pull-left idea_campaign_img">
                    <a href="<?php echo wp_get_attachment_url( $cmp_image_id ) ?>" rel="prettyPhoto">
                        <?php echo wp_get_attachment_image( $cmp_image_id ) ?>
                    </a>
                </div>
            <?php endif ?>
            <div>
                <a href="<?php  echo site_url() ?>/campaigns/<?php echo $campaign[0]->post_name ?>">
                    <div style="font-size:16px;text-transform: uppercase;"><?php echo $campaign[0]->post_title ?></div    >
                </a>
                <?php 
                    $due_date = get_post_meta($campaign[0]->ID,'campaign_end_date',true);
                    $due_date = new DateTime($due_date);
                    $now = new DateTime();
                    $interval = $due_date->diff($now); 
                ?>
                <div>Time left: <?php echo $interval->format('%m month, %d days') ?></div>
                <div>Ideas count: <?php echo count(get_post_meta($campaign[0]->ID,'campaign_ideas',false)) ?></div>
                <div style="text-align:justify; margin-top:20px">
                    <?php echo wp_trim_words( $campaign[0]->post_content,22 ) ?> 
                </div>
            </div>
        </div>
    <?php endif ?>

    <div class="row">
        <div class="col-md-2">
            <div class="sabai-col-xs-2 sabai-questions-side" user_id style="width: 100%;">
                <?php if(function_exists('kleo_item_likes')): ?>
                    <div class="sabai-questions-vote-count idea_vote" user_id=<?php the_author_meta( 'ID' ); ?> >
                        <?php kleo_item_likes(get_the_ID()) ?>            
                    </div>
                <?php endif ?>
                <div class="sabai-questions-answer-count">
                    <a href="#comments" style="cursor:pointer">
                        <?php $user_comments = get_comments(array('author__in'=>array(get_current_user_id()),'post_id'=>get_the_ID())); ?>
                        <span class="sabai-number" <?php echo count($user_comments)?'style="color:#00E000"':'' ?> >
                            <div class="comments_count_number" style="display:inline"><?php echo get_comments_number() ?></div>
                            <i class="fa fa-comments"></i>
                            <input type='hidden' class="post_id" value="<?php echo get_the_ID() ?>">
                        </span>
                    </a>
                </div>
                <?php 
                    $idea_status = get_post_meta($post->ID, 'idea_status', true);
                    switch ($idea_status) {
                        case 'in discussion':
                            $idea_status_class = 'btn-info';
                            break;
                        case 'selected':
                            $idea_status_class = 'btn-primary';
                            break;
                        case 'rejected':
                            $idea_status_class = 'btn-danger';
                            break;
                        case 'in project':
                            $idea_status_class = 'btn-success';
                            break;
                        default:
                            $idea_status_class = '';
                    } 
                ?>
                <?php if($idea_status): ?>
                    <div class="text-center <?php echo $idea_status_class ?> status_tag"><?php echo $idea_status ?></div> 
                <?php endif ?>
                <div class="sabai-questions-view-count">
                    <?php $views_count = get_post_meta(get_the_ID(),'views_count')[0] ?>
                    <span class="sabai-number"><?php echo $views_count?$views_count:0 ?></span> views  
                    <?php if(is_user_logged_in()): ?>
                        <span class="idea_favorite">
                            <?php $favorites_meta = get_post_meta(get_the_id(),'idea_favorites') ?>
                            <span class="text-center"><i class="favorite-star fa-lg fa <?php echo in_array(get_current_user_id(),$favorites_meta)?'fa-star':'fa-star-o' ?>"></i>
                            </span>
                            <span class="text-center favorites_count"><?php echo count($favorites_meta) ?></span>
                            <input type="hidden" id="post_id" value="<?php the_ID() ?>"> 
                        </span>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div>
                <?php $author_id = get_the_author_meta('ID'); ?>
                <span class="idea-avatar"><?php echo get_avatar( $author_id, '32' );  ?></span>
            </div>

            <?php get_template_part( 'content', get_post_format() ); ?>

            <div style="margin-bottom: 20px">
                <div class="sabai-questions-activity sabai-questions-activity-inline">
                    <ul class="sabai-entity-activity">
                        <li>
                            <a href="<?php echo bp_core_get_user_domain( $author_id ); ?>" class="sabai-user sabai-user-with-thumbnail" rel="nofollow" data-popover-url="http://marylink.appteka.cc/sabai/user/profile/jcantenot">
                                <?php echo bp_core_get_user_displayname($author_id) ?>
                            </a> 
                            posted 
                            <span title="<?php get_the_date(); ?>">
                            <?php echo human_time_diff( strtotime( get_the_date('Y-m-d H:i:s')), time() ); ?>
                            ago
                            </span>
                        </li>
                        <li>
                            <i class="fa fa-clock-o"></i>
                            last active 
                            <?php echo human_time_diff( strtotime( get_user_meta( $author_id, 'last_activity', true )), time() ); ?>
                            ago
                        </li>
                        <?php if(is_user_logged_in () && is_author_idea() ): ?>
                            <li>
                                <a class="idea_popup" data-ideaid="<?php the_ID(); ?>" style="cursor:pointer">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                    <div class="pull-right">
                        <?php $tags = wp_get_post_tags( get_the_ID()) ?>
                        <?php if(count($tags)):?>
                            <?php foreach($tags as $tag):?>
                                <div class="idea_tag"><?php echo $tag->name ?></div>
                            <?php endforeach ?>
                        <?php endif ?>
                    </div>                                          
                </div>
            </div>
            <?php get_template_part( 'page-parts/posts-social-share' ); ?>
        </div>
    </div>

    <?php 
    if( $related == 1 ) {
        get_template_part( 'page-parts/posts-related' );
    }
    ?>

    <?php
    if ( sq_option( 'post_navigation', 1 ) == 1 ) :
        // Previous/next post navigation.
        kleo_post_nav();
    endif;
    ?>
    <!-- Begin Attachments -->
    <?php $idea_image_id = get_post_meta(get_the_ID(),'idea_image_id',true) ?>
    <?php $idea_file_id = get_post_meta(get_the_ID(),'idea_file_id',true) ?>
    <?php if($idea_image_id || $idea_file_id): ?>
        <div class="hr-title hr-long"><abbr>Attachments</abbr></div>
        <div class="text-center" style="margin:20px 0">
            <?php if($idea_image_id): ?>
                <div>
                    <a href="<?php echo wp_get_attachment_url( $idea_image_id ) ?>" rel="prettyPhoto">
                        <?php echo wp_get_attachment_image( $idea_image_id ) ?>
                    </a>
                </div>
            <?php endif ?>
            <?php if($idea_file_id): ?>
                <?php $file_type = explode('/',get_post_mime_type( $idea_file_id )) ?>
                <?php $file_url = wp_get_attachment_url( $idea_file_id ) ?>
                <div class="attach_file_link">
                    <a href="<?php echo $file_url ?>">
                    <?php ini_set('display_errors', 1) ?>
                        <?php if(is_file(getcwd().'/wp-content/plugins/ideas/assets/img/file_types/'.$file_type[1].'.png')):?>
                            <img src="<?php echo get_site_url().'/wp-content/plugins/ideas/assets/img/file_types/'.$file_type[1].'.png'?>" title="<?php echo $file_url ?>">
                        <?php else: ?>
                            <img src="<?php echo get_site_url().'/wp-content/plugins/ideas/assets/img/file_types/label.png'?>" title="<?php echo $file_url ?>">
                        <?php endif ?>
                    </a>
                </div>
            <?php endif ?>
        </div>
    <?php endif ?>
    
    <!-- End Attachments -->

    <?php
    $idea_youtube = get_post_meta($post->ID, 'idea_youtube', true);
    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $idea_youtube, $match);
    $video_id = $match[1];
    if( !empty( $idea_youtube ) ) : ?>
        <div class="hr-title hr-long"><abbr>YouTube</abbr></div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-10">
                
                <iframe width="600" height="300" src="https://www.youtube.com/embed/<?php echo $video_id; ?>" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    <?php endif; ?>

    <!-- Begin Comments -->
    <?php comments_template( '', true ); ?>
    <!-- End Comments -->

<?php endwhile; ?>
</div>

<?php get_template_part('page-parts/general-after-wrap');?>

<?php get_footer(); ?>