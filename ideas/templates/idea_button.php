<div class="btn btn-info add-idea-button" data-toggle="modal" data-target="#ideaModal">
	<i class="fa fa-lightbulb-o"> Add Idea</i>
</div>
<div class="idea_loading_block">
	Please wait<img src="<?php echo plugins_url( 'ideas/assets/img/loading.gif' ) ?>">
</div>
<div class="idea_info_block"><span></span><i class="fa fa-times"></i></div>