
<script type="text/javascript">

  function previewImage() {
    var preview = document.querySelector('#idea_image_preview')
    var file    = document.querySelector('#idea_image').files[0]
    var reader  = new FileReader()
    document.querySelector('.reset_image').style.display = 'none'

    reader.onloadend = function () {
      preview.src = reader.result;
    }
    if (file && file.type.split('/')[0] == 'image') {
      reader.readAsDataURL(file)
      document.querySelector('.reset_image').style.display = 'block'
    } else {
      document.querySelector('#idea_image').value = ''
      preview.src = "<?php echo plugins_url( 'ideas/assets/img/noimage.png' ) ?>"
    }
  }


jQuery(document).ready(function($){

    <?php $ideas = get_posts(array('post_type'=>'ideas')); ?>
    <?php if($ideas): ?>
      <?php 
        $autocomplete = '';
        foreach ($ideas as $key=>$value):
          if($key!=0){
            $autocomplete.=',';
          }
          $autocomplete.='"'.$value->post_title.'"';
        endforeach 
      ?>
      $( ".idea_title" ).autocomplete({
        source: [ <?php echo $autocomplete ?> ]
      });
    <?php endif ?>

    function resetErrors(){
      $('#idea_title,#idea_text').removeClass('input_error')
      $('.max-size').removeClass('file_error')
      $('.form_error').remove()
    }

    function resetImagePreview(){
      $('#idea_image_preview').attr('src',"<?php echo plugins_url( 'ideas/assets/img/noimage.png' ) ?>")
      $(this).hide()
      $('#idea_image').val('')
    }

    //validation start
    function formValidation(){
      var form_errors = 0
      var image = $('#idea_image').get(0).files[0]
      var file  = $('#idea_attachment').get(0).files[0]

      resetErrors()

      if($('#idea_title').val()==''){
        $('#idea_title').addClass('input_error')
        $('#idea_title').prev().append('<span class="form_error"> Field is required</span>')
        form_errors ++
      }

      if($('#idea_text').val()==''){
        $('#idea_text').addClass('input_error')
        $('#idea_text').prev().append('<span class="form_error"> Field is required</span>')
        form_errors ++
      }

      if(image){
        if(image.size > 5000000){
          $('.max-size.img').addClass('file_error')
          form_errors ++
        }
      }
      if(file){
        if(file.size > 100000000){
          $('.max-size.attch').addClass('file_error')
          form_errors ++
        }
      }
      if(form_errors>0){
        return false
      }
      return true
    }
    //validation end

    $('.reset_image').on('click',function(){
      resetImagePreview()
      $(this).hide()
    })

    $('#ideaModal').on('hide.bs.modal',function(){
      resetErrors()
    })
    $('.idea_info_block i').on('click',function(){
      $('.idea_info_block').hide()
      $('.idea_info_block span').html('')
    })

		$('.submit-idea-button').on('click',function(){  
      var reader  = new FileReader()
      var form = new FormData($('#add_idea_form').get(0))
      form.append( 'action', 'add_idea' )
      if(formValidation()){
        $('#ideaModal').modal('hide')
        $('.idea_loading_block').show()
        $.ajax({            
          type: "POST",
          url: '<?php echo admin_url("admin-ajax.php") ?>',
          data: form,
          contentType: false,
          processData: false,
          dataType: "json",
          success: function ( response ) {
            resetImagePreview()     
            $('#add_idea_form').get(0).reset()
            $('.idea_info_block span').html(response.html)
            $('.idea_loading_block').hide()
            $('.idea_info_block').show()
          }
        })
      }  	
		})

})

</script>
<div class="modal fade" id="ideaModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display:none">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add Idea</h4>
      </div>
      <div class="modal-body">
        <form id="add_idea_form" enctype="multipart/form-data">
            <div class="form-group">
                <label>Idea Title*</label>
                <input name="title" type="text" id="idea_title" class="form-control idea_title">
            </div>
            <div class="form-group">
                <label>Idea Content*</label>
                <textarea name="text" id="idea_text" class="form-control" style="max-width:100%"></textarea>
            </div>
            <div class="form-group">
                <label>Video (youtube)</label>
                <input name="youtube" type="text" class="form-control">
            </div>
            <?php if(post_type_exists('campaigns')): ?>
              <?php if(!$current_campaign): ?>
                <?php $campaigns = get_posts(array('post_type'=>'campaigns')) ?>
                <?php if(!empty($campaigns)): ?>
                  <div class="form-group">
                    <label>Campaign</label>
                    <select name="idea_campaign" class="form-control">
                      <option value="">Select campaign</option>
                      <?php foreach ($campaigns as $key => $val): ?>
                        <option value="<?php echo $val->ID ?>"><?php echo $val->post_title ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                <?php endif ?>
              <?php else: ?>
                <input type="hidden" name="idea_campaign" value="<?php echo $current_campaign ?>">
              <?php endif ?>
            <?php endif ?>
            <div class="form-group">
            	<div class="pull-left">
                <img src="<?php echo plugins_url( 'ideas/assets/img/noimage.png' ) ?>" id="idea_image_preview" style="height: 115px; margin-right: 20px;">
                <i class="reset_image fa fa-times" style="display:none"> Reset image</i>
              </div>
              <label>Upload image - <span class="max-size img">Max file size 5MB</span></label>
            	<input name="image" type="file" id="idea_image" onchange="previewImage()"> 
            </div>
            <div class="form-group">
            	<label>Upload attachment - <span class="max-size attch">Max file size 100MB</span></label>
            	<input name="attachment" id="idea_attachment" type="file">
            </div>
        </form>
        <div class="form_errors"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary submit-idea-button">Add</button>
      </div>
    </div>
  </div>
</div>