<?php
/*
Plugin Name: Alex Karev Location
Description: Custom content types for themes developed by Alex Karev
Version: 1.0
Author: Alex Karev
*/

function location_load_assets(){
	wp_enqueue_style( 'location_metabox-styles', plugins_url('/assets/css/metaboxes.css?v='.time(),__FILE__));
	wp_enqueue_style( 'location_jquery-select2', plugins_url('/assets/css/select2.css?v='.time(),__FILE__));
	wp_enqueue_script( 'location_select2-js', plugins_url('/assets/js/select2.min.js?v='.time(),__FILE__), array('jquery'), time(), false);
	wp_enqueue_script('location_custom-js', plugins_url('/assets/js/metaboxes.js?v='.time(),__FILE__), 'jquery-ui-core', '1.0', true);
}

function create_pt_location() {
	register_post_type( 'location', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	array('labels' => array(
			'name' => __('Location'), /* This is the Title of the Group */
			'singular_name' => __('Location'), /* This is the individual type */
			'all_items' => __('All Location'), /* the all items menu item */
			'add_new' => __('Add New Location'), /* The add new menu item */
			'add_new_item' => __('Add New Location'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit Location'), /* Edit Display Title */
			'new_item' => __('New Location'), /* New Display Title */
			'view_item' => __('View Location'), /* View Display Title */
			'search_items' => __('Search Location'), /* Search Product Title */ 
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), 
			'description' => __( 'Location for '.get_bloginfo() ), /* Product Description */
			'public' => true,
			'menu_icon' => 'dashicons-location-alt',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'our-physicians', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title','post-options','editor','page-attributes', 'thumbnail', 'author',),
	 	) /* end of options */
	); /* end of register post type */
}

function location_meta_boxes(){
	add_meta_box( 'events_meta_box',
        'Location settings',
        'display_location_meta_box',
        'location', 
        'normal', 
        'high'
    );
}

function display_location_meta_box(){
	global $post;
	$locationTitle = get_post_meta($post->ID, 'location_title', true);
	$locationSpecialties = get_post_meta($post->ID, 'location_specialties', true);
	$specialties = get_posts( array (
		'post_type'	=> 'specialties',
		'posts_per_page' => -1
	));
	$locationTreatments = get_post_meta($post->ID, 'location_treatments', true);
	$treatments = get_posts( array (
		'post_type'	=> 'treatments',
		'posts_per_page' => -1
	));
?>

	

	<table class="form-table">
		<tr>
			<th><label for="location_title">Title</label></th>
			<td>
				<input type="text" name="location_title" id="location_title" value="<?php echo $locationTitle ?>" size="30">
				<br><span class="description">Title for this location member, e.g. MD</span>
			</td>
		</tr>
		<?php if( post_type_exists('specialties') ): ?>
			<tr>
				<th><label for="specialties_list">Specialties</label></th>
				<td>
					<select name="location_specialties[]" id="specialties_list" multiple="multiple">
						<option value="1">Select One</option>
						<?php foreach($specialties as $child ): ?>
							<option value="<?php echo $child->ID ?>", <?php echo $locationSpecialties&&in_array($child->ID,$locationSpecialties)?' selected="selected"':''?> ><?php echo $child->post_title ?></option>;
						<?php endforeach ?>
					</select>
					<br /><span class="description">The services that this location member covers.</span>
				</td>
			</tr>
		<?php endif ?>

		<?php if( post_type_exists('treatments') ): ?>
			<tr>
				<th><label for="treatments_list">Treatments</label></th>
				<td>
					<select name="location_treatments[]" id="treatments_list" multiple="multiple">
						<option value="1">Select One</option>
						<?php foreach($treatments as $child ): ?>
							<option value="<?php echo $child->ID ?>", <?php echo $locationTreatments&&in_array($child->ID,$locationTreatments)?' selected="selected"':''?> ><?php echo $child->post_title ?></option>;
						<?php endforeach ?>
					</select>
					<br /><span class="description">The treatments that this location member can provide.</span>
				</td>
			</tr>
		<?php endif ?>
	</table>
<?php 
}


function save_location_custom_meta( $id, $item ) {
    // Check post type
    if ( $item->post_type == 'location') {
        if ( isset( $_POST['location_title'] ) ) {
            update_post_meta( $id, 'location_title',$_POST['location_title'] );
        }
	    if ( post_type_exists('specialties') ) {
	        if ( !empty($_POST['location_specialties']) ) {
	            update_post_meta( $id, 'location_specialties',$_POST['location_specialties'] );
	        } else {
	        	delete_post_meta($id, 'location_specialties');
	        }
	    }
	    if ( post_type_exists('treatments') ) {
	        if ( !empty($_POST['location_treatments']) ) {
	            update_post_meta( $id, 'location_treatments',$_POST['location_treatments'] );
	            updateLocationLinkedResources($_POST['location_treatments'],$id,'treatment_locations');
	        } else {
	        	delete_post_meta($id, 'location_treatments');
	        }
	    }
	}
}

function updateLocationLinkedResources($linkedIds,$currentId,$type){
    foreach ($linkedIds as $id) {
    	$meta = get_post_meta($id,$type,true);
    	if(empty($meta) || !in_array($currentId,$meta)){
    		$meta[] = $currentId;
    		update_post_meta($id,$type,$meta);
    	}
    }
}

add_action( 'admin_enqueue_scripts','location_load_assets' );
add_action( 'init','create_pt_location' );
add_action( 'add_meta_boxes','location_meta_boxes' );
add_action( 'save_post','save_location_custom_meta',10,2 );


