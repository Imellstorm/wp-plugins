<?php
/*
Plugin Name: Alex Karev Providers
Description: Custom content types for themes developed by Alex Karev
Version: 1.0
Author: Alex Karev
*/

function providers_load_assets(){
	wp_enqueue_style( 'providers_metabox-styles', plugins_url('/assets/css/metaboxes.css?v='.time(),__FILE__));
	wp_enqueue_style( 'providers_jquery-select2', plugins_url('/assets/css/select2.css?v='.time(),__FILE__));
	wp_enqueue_script( 'providers_select2-js', plugins_url('/assets/js/select2.min.js?v='.time(),__FILE__), array('jquery'), time(), false);
	wp_enqueue_script('providers_custom-js', plugins_url('/assets/js/metaboxes.js?v='.time(),__FILE__), 'jquery-ui-core', '1.0', true);
}

function create_pt_providers() {
	register_post_type( 'providers', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	array('labels' => array(
			'name' => __('Providers'), /* This is the Title of the Group */
			'singular_name' => __('Provider'), /* This is the individual type */
			'all_items' => __('All Providers'), /* the all items menu item */
			'add_new' => __('Add New Provider'), /* The add new menu item */
			'add_new_item' => __('Add New Provider'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit Provider'), /* Edit Display Title */
			'new_item' => __('New Provider'), /* New Display Title */
			'view_item' => __('View Provider'), /* View Display Title */
			'search_items' => __('Search Providers'), /* Search Product Title */ 
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), 
			'description' => __( 'providers for '.get_bloginfo() ), /* Product Description */
			'public' => true,
			'menu_icon' => 'dashicons-id',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'our-physicians', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title','post-options','editor','page-attributes', 'thumbnail', 'author',),
	 	) /* end of options */
	); /* end of register post type */
}

function providers_meta_boxes(){
	add_meta_box( 'events_meta_box',
        'Provider settings',
        'display_providers_meta_box',
        'providers', 
        'normal', 
        'high'
    );
}

function display_providers_meta_box(){
	global $post;
	$providersTitle = get_post_meta($post->ID, 'providers_title', true);
	$providersSpecialties = get_post_meta($post->ID, 'provider_specialties', true);
	$specialties = get_posts( array (
		'post_type'	=> 'specialties',
		'posts_per_page' => -1
	));
	$providersTreatments = get_post_meta($post->ID, 'provider_treatments', true);
	$treatments = get_posts( array (
		'post_type'	=> 'treatments',
		'posts_per_page' => -1
	));
?>

	

	<table class="form-table">
		<tr>
			<th><label for="providers_title">Title</label></th>
			<td>
				<input type="text" name="providers_title" id="providers_title" value="<?php echo $providersTitle ?>" size="30">
				<br><span class="description">Title for this providers member, e.g. MD</span>
			</td>
		</tr>
		<?php if( post_type_exists('specialties') ): ?>
			<tr>
				<th><label for="specialties_list">Specialties</label></th>
				<td>
					<select name="provider_specialties[]" id="specialties_list" multiple="multiple">
						<option value="">Select One</option>
						<?php foreach($specialties as $child ): ?>
							<option value="<?php echo $child->ID ?>", <?php echo $providersSpecialties&&in_array($child->ID,$providersSpecialties)?' selected="selected"':''?> ><?php echo $child->post_title ?></option>;
						<?php endforeach ?>
					</select>
					<br /><span class="description">The services that this providers member covers.</span>
				</td>
			</tr>
		<?php endif ?>

		<?php if( post_type_exists('treatments') ): ?>
			<tr>
				<th><label for="treatments_list">Treatments</label></th>
				<td>
					<select name="provider_treatments[]" id="treatments_list" multiple="multiple">
						<option value="">Select One</option>
						<?php foreach($treatments as $child ): ?>
							<option value="<?php echo $child->ID ?>", <?php echo $providersTreatments&&in_array($child->ID,$providersTreatments)?' selected="selected"':''?> ><?php echo $child->post_title ?></option>;
						<?php endforeach ?>
					</select>
					<br /><span class="description">The treatments that this providers member can provide.</span>
				</td>
			</tr>
		<?php endif ?>
	</table>
<?php 
}


function save_providers_custom_meta( $id, $item ) {
    if($item->post_type == 'providers'){
	    if ( isset( $_POST['providers_title'] )) {
	        update_post_meta( $id, 'providers_title',$_POST['providers_title'] );
	    }
	    if (post_type_exists('specialties')) {
		    if ( !empty( $_POST['provider_specialties'] ) ) {
		        update_post_meta( $id, 'provider_specialties',$_POST['provider_specialties'] );
		    } else {
	    		delete_post_meta($id, 'provider_specialties');
	    	}
	    }
	    if (post_type_exists('treatments')) {
		    if ( !empty( $_POST['provider_treatments'] ) ) {
		        update_post_meta( $id, 'provider_treatments',$_POST['provider_treatments'] );
		        updateProviderLinkedResources($_POST['provider_treatments'],$id,'treatment_providers');
		    } else {
	    		delete_post_meta($id, 'provider_treatments');
	    	}
	    }
	}
}

function updateProviderLinkedResources($linkedIds,$currentId,$type){
    foreach ($linkedIds as $id) {
    	$meta = get_post_meta($id,$type,true);
    	if(empty($meta) || !in_array($currentId,$meta)){
    		$meta[] = $currentId;
    		update_post_meta($id,$type,$meta);
    	}
    }
}

add_action( 'admin_enqueue_scripts','providers_load_assets' );
add_action( 'init','create_pt_providers' );
add_action( 'add_meta_boxes','providers_meta_boxes' );
add_action( 'save_post','save_providers_custom_meta',10,2 );



