<?php
/*
Plugin Name: Alex Karev Slideshow
Description: Custom content types for themes developed by Alex Karev
Version: 1.0
Author: Alex Karev
*/

function slideshow_load_assets(){
	wp_enqueue_script('jquery-ui-slider');
	wp_enqueue_style( 'slideshow_metabox-styles', plugins_url('/assets/css/metaboxes.css?v='.time(),__FILE__));
	wp_enqueue_script('slideshow_custom-js', plugins_url('/assets/js/metaboxes.js?v='.time(),__FILE__), 'jquery-ui-core', '1.0', true);
}

function create_pt_slideshow() {
	register_post_type( 'slideshow', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	array('labels' => array(
			'name' => __('Slideshow'), /* This is the Title of the Group */
			'singular_name' => __('Slide'), /* This is the individual type */
			'all_items' => __('All Slides'), /* the all items menu item */
			'add_new' => __('Add New Slide'), /* The add new menu item */
			'add_new_item' => __('Add New Slide'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit Slide'), /* Edit Display Title */
			'new_item' => __('New Slide'), /* New Display Title */
			'view_item' => __('View Slide'), /* View Display Title */
			'search_items' => __('Search Slide'), /* Search Product Title */ 
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), 
			'description' => __( 'Homepage Slides for '.get_bloginfo() ), /* Product Description */
			'public' => true,
			'menu_icon' => 'dashicons-slides',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'slides', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title','post-options','page-attributes'),
	 	) /* end of options */
	); /* end of register post type */
}

function slideshow_meta_boxes(){
	add_meta_box( 'events_meta_box',
        'Slide settings',
        'display_slideshow_meta_box',
        'slideshow', 
        'normal', 
        'high'
    );
}

function display_slideshow_meta_box(){
	global $post;
	$slideUrl = get_post_meta($post->ID, 'slide_url', true);
	$slideId = get_post_meta($post->ID, 'slide_id', true);
	$slideHeadline = get_post_meta($post->ID, 'slide_headline', true);
	$slideText = get_post_meta($post->ID, 'slide_text', true);
?>
    <script>
       jQuery(document).ready(function($){
            $('#upload_slide_button').on('click',function() {
                showMediaUploader($('#slide_url'),$('#slide_id'),$('.slide_img'));
            });
            $('#clear_slide_button').on('click',function() {
            	$('#slide_url').attr('value','');
            	$('#slide_id').attr('value','');
            	$('.slide_img').attr('src','<?php echo plugins_url('/assets/img/noimage.png',__FILE__) ?>' );
            })
        });
    </script>
	<table class="form-table">
		<tr>
			<th><label for="conditions_list">Slide</label></th>
			<td>
				<img src="<?php echo $slideUrl?$slideUrl:plugins_url('/assets/img/noimage.png',__FILE__) ?>" class="slide_img" style="float:left; margin-right:20px; max-width: 100px;">
				<label for="upload_slide">
					<input id="slide_url" type="text" size="36" name="slide_url" value="<?php echo $slideUrl ?>" />
					<div style="margin:10px 0">Enter a URL or upload an image</div>
					<input id="slide_id" type="hidden" size="36" name="slide_id" value="<?php echo $slideId ?>" />
					<input id="upload_slide_button" class="button button-primary" type="button" value="Upload Slide" />
					<input id="clear_slide_button" class="button button-default" type="button" value="Clear" />
					
				</label>
			</td>
		</tr>
		<tr>
			<th><label for="slide_headline">Slide Headline</label></th>
			<td>
				<input type="text" name="slide_headline" id="slide_headline" value="<?php echo $slideHeadline ?>" size="30">
				<br><span class="description">The headline for this slide.</span>
			</td>
		</tr>
		<tr>
			<th><label for="conditions_list">Text</label></th>
			<td>
				<?php wp_editor($slideText, 'slide_text', array('textarea_rows' => 25) ); ?>
			</td>
		</tr>
	</table>
<?php 
}


function save_slideshow_custom_meta( $id, $item ) {
    // Check post type
    if ( $item->post_type == 'slideshow' ) {
        // Store data in post meta table if present in post data
        if ( isset($_POST['slide_url']) ) {
            update_post_meta( $id, 'slide_url',$_POST['slide_url'] );
        }
        if ( isset($_POST['slide_id']) ) {
            update_post_meta( $id, 'slide_id',$_POST['slide_id'] );
        }
        if ( isset($_POST['slide_headline']) ) {
            update_post_meta( $id, 'slide_headline',$_POST['slide_headline'] );
        }
        if ( isset($_POST['slide_text']) ) {
            update_post_meta( $id, 'slide_text',$_POST['slide_text'] );
        }
    }
}

add_action( 'admin_enqueue_scripts','slideshow_load_assets' );
add_action( 'init','create_pt_slideshow' );
add_action( 'add_meta_boxes','slideshow_meta_boxes' );
add_action( 'save_post','save_slideshow_custom_meta',10,2 );



