<?php
/*
Plugin Name: Alex Karev Specialties
Description: Custom content types for themes developed by Alex Karev
Version: 1.0
Author: Alex Karev
*/

function create_pt_specialties() {
	register_post_type( 'specialties', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	array('labels' => array(
			'name' => __('Specialties'), /* This is the Title of the Group */
			'singular_name' => __('Specialty'), /* This is the individual type */
			'all_items' => __('All Specialties'), /* the all items menu item */
			'add_new' => __('Add New Specialty'), /* The add new menu item */
			'add_new_item' => __('Add New Specialty'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit Specialty'), /* Edit Display Title */
			'new_item' => __('New Specialty'), /* New Display Title */
			'view_item' => __('View Specialty'), /* View Display Title */
			'search_items' => __('Search Specialty'), /* Search Product Title */ 
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), 
			'description' => __( 'Specialties for '.get_bloginfo() ), /* Product Description */
			'public' => true,
			'menu_icon' => 'dashicons-star-filled',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'specialties', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title','post-options','editor','page-attributes', 'thumbnail'),
	 	) /* end of options */
	); /* end of register post type */
}

add_action( 'init','create_pt_specialties' );


