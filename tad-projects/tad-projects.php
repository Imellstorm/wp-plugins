<?php
/*
Plugin Name: TAD Projects
Description: Custom content types for themes developed by Alex Karev
Version: 1.3
Author: Alex KArev
*/

function tad_projects_load_assets(){
	wp_enqueue_style( 'tad_projects_metabox-styles', plugins_url('/assets/css/metaboxes.css?v='.time(),__FILE__));
	wp_enqueue_script('tad_projects_sortable', plugins_url('/assets/js/sortable.min.js?v='.time(),__FILE__), '', '1.0', false);
}

function tad_projects_front_assets(){
	wp_enqueue_script( 'tad_projects_lightbox-js', plugins_url('/assets/js/lightbox.js?v='.time(),__FILE__), array('jquery'), time(), true);
	wp_enqueue_style( 'tad_projects_lightbox-styles', plugins_url('/assets/css/lightbox.css?v='.time(),__FILE__));
}

add_action( 'wp_footer', 'js_lib_init', 100 );

function js_lib_init(){
?>
	<script>
	    lightbox.option({
	      'resizeDuration': 200,
	      'wrapAround': true
	    })
	</script>
<?php
}

function create_pt_tad_projects() {
	register_post_type( 'tad_projects', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	array('labels' => array(
			'name' => __('Projects'), /* This is the Title of the Group */
			'singular_name' => __('Project'), /* This is the individual type */
			'all_items' => __('All Projects'), /* the all items menu item */
			'add_new' => __('Add New Project'), /* The add new menu item */
			'add_new_item' => __('Add New Project'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit Project'), /* Edit Display Title */
			'new_item' => __('New Project'), /* New Display Title */
			'view_item' => __('View Project'), /* View Display Title */
			'search_items' => __('Search tad_projects'), /* Search Product Title */ 
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), 
			'description' => __( 'tad_projects for '.get_bloginfo() ), /* Product Description */
			'public' => true,
			'menu_icon' => 'dashicons-exerpt-view',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'projects', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title','post-options','editor','thumbnail'),
			//'taxonomies' => array( 'post_tag' ),

	 	) /* end of options */
	); /* end of register post type */
}

add_theme_support( 'post-thumbnails' ); 

add_action( 'init', 'create_projects_tax' );

function create_projects_tax() {
	$args = array(
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		//'rewrite'           => array( 'slug' => 'projects_category' ),
	);
	register_taxonomy( 'projects_category', array( 'tad_projects' ), $args );

	$args = array(
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
	);
	register_taxonomy( 'projects_tag', array( 'tad_projects' ), $args );
}

function tad_projects_meta_boxes(){
	add_meta_box( 'tad_projects_meta_box',
        'Project content',
        'display_tad_projects_meta_box',
        'tad_projects', 
        'normal', 
        'high'
    );
}

function display_tad_projects_meta_box($post){
	$project_image_urls = esc_html( get_post_meta( $post->ID, 'project_image_urls', true ) );
    $project_image_id 	= esc_html( get_post_meta( $post->ID, 'project_image_id', true ) );
    $project_image_title= esc_html( get_post_meta( $post->ID, 'project_image_title', true ) );
    $project_image_desc = esc_html( get_post_meta( $post->ID, 'project_image_desc', true ) );

    $project_image_urls = json_decode(htmlspecialchars_decode($project_image_urls));
    $project_image_id 	= json_decode(htmlspecialchars_decode($project_image_id));
    $project_image_title= json_decode(htmlspecialchars_decode($project_image_title));
    $project_image_desc = json_decode(htmlspecialchars_decode($project_image_desc));
    ?>
    <script>
    	function showMediaUploader(element){
		    var mediaUploader;
		    // If the uploader object has already been created, reopen the dialog
		     if (mediaUploader) {
		      mediaUploader.open();
		      return;
		    }
		    // Extend the wp.media object
		    mediaUploader = wp.media.frames.file_frame = wp.media(
		      {
		        title: 'Choose Image',
		        button: {
		        text: 'Choose Image'
		      }, 
		      multiple: true 
		    });
		    mediaUploader.on('select', function() {
		      selection = mediaUploader.state().get('selection')
		      selection.map( function( attachment ) {
		      	attachment = attachment.toJSON()
		      	block_html = jQuery.parseHTML( '<?php include( plugin_dir_path( __FILE__ ).'/image-block.php') ?>' )
		        jQuery(block_html).find('img').attr('src',attachment.url)
		        jQuery(block_html).find('.project_image_url').attr('value',attachment.url)
		        jQuery(block_html).find('.project_image_id').attr('value',attachment.id)
		        element.prepend(block_html)
		       });
		    });
		    // Open the uploader dialog
		    mediaUploader.open();
		}

       jQuery(document).ready(function($){
            $('#add_Project_image').on('click',function(){               
                showMediaUploader($('#project_images_list'));
            });
            
            $('body').on('click','.remove_Project_image_ico',function(){
                $(this).parent().parent().remove();
            })

            project_images_list = document.getElementById("project_images_list");
			var sortable = Sortable.create(project_images_list);
        });
    </script>
    <span>Shortcode [tad-project-gallery id="<?php echo $post->ID ?>"]</span>
    <input id="add_Project_image" type="button" value="Add Project image" style="margin:20px 10px; display:block">
    <div id="project_images_list" class="list-group">
	    <?php  
	    if(count($project_image_urls)):?>
	    	
		        <?php foreach ($project_image_urls as $key => $Project_image_url): ?>
		            <?php if(!empty($Project_image_url) && isset($project_image_id[$key])): ?>
		            	<?php include( plugin_dir_path( __FILE__ ).'/image-block.php') ?>
		        	<?php endif ?>
		        <?php endforeach ?>
	        </div>
	    <?php endif ?>
	</div>
<?php }

function save_tad_projects_custom_meta( $id, $item ) {
	if( $item->post_type == 'tad_projects' ){
        delete_post_meta($id,'project_image_urls');
        delete_post_meta($id,'project_image_id');
        delete_post_meta($id,'project_image_title');
        delete_post_meta($id,'project_image_desc');
        if ( isset( $_POST['project_image_urls'] ) && !empty($_POST['project_image_urls']) ) {
            update_post_meta( $id, 'project_image_urls', json_encode($_POST['project_image_urls']) );
            update_post_meta( $id, 'project_image_id', json_encode($_POST['project_image_id']) );
            update_post_meta( $id, 'project_image_title', json_encode($_POST['project_image_title']) );
            update_post_meta( $id, 'project_image_desc', json_encode($_POST['project_image_desc']) );
        }
	}
}

function updatetad_projectsLinkedResources($linkedIds,$currentId,$type){
    foreach ($linkedIds as $id) {
    	$meta = get_post_meta($id,$type,true);
    	if(empty($meta) || !in_array($currentId,$meta)){
    		$meta[] = $currentId;
    		update_post_meta($id,$type,$meta);
    	}
    }
}

function tad_projects_shortcode( $atts ){ 
	ob_start();
	if(isset($atts['id']) && !empty($atts['id'])){
		$images_ids 	= get_post_meta($atts['id'], 'project_image_id', true);
		$images_titles 	= get_post_meta($atts['id'], 'project_image_title', true);
		$images_descs 	= get_post_meta($atts['id'], 'project_image_desc', true);
		$images_ids 	= json_decode($images_ids);
		$images_titles 	= json_decode($images_titles);
		$images_descs 	= json_decode($images_descs);
	} ?>
	<?php if(count($images_ids)): ?>
		<div class="row">
			<?php foreach ($images_ids as $key=>$image_id): ?>
				<div class="col-md-4 gallery-imgs-cont">
					<div class="gallery-box">
						<a href="<?php echo wp_get_attachment_image_src( $image_id )[0] ?>" data-lightbox="project_gallery_images" data-title="<?php echo isset($images_titles[$key])?$images_titles[$key].'<br><i>'.$images_descs[$key].'</i>':'' ?>">
							<img src="<?php echo wp_get_attachment_image_src( $image_id )[0] ?>" class="gallery-img">
						</a>
					</div>
					<div class="caption">
						<div><?php echo isset($images_titles[$key])?$images_titles[$key]:'' ?></div>
						<div class="desc"><?php echo isset($images_descs[$key])?$images_descs[$key]:'' ?></div>
					</div>
				</div>
			<?php endforeach ?>
		</div>
 	<?php endif ?>
 	<?php return ob_get_clean(); ?>
<?php }

add_action( 'admin_enqueue_scripts','tad_projects_load_assets' );
add_action( 'wp_enqueue_scripts','tad_projects_front_assets' );
add_action( 'init','create_pt_tad_projects' );
add_action( 'add_meta_boxes','tad_projects_meta_boxes' );
add_action( 'save_post','save_tad_projects_custom_meta',10,2 );
add_shortcode( 'tad-project-gallery', 'tad_projects_shortcode' );
