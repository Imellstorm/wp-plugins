<?php
class plugin_updater{

    function __construct($update_path, $plugin_slug, $plugin_path, $current_version){
        $this->update_path = $update_path;
        $this->plugin_slug = $plugin_slug;
        $this->plugin_path = $plugin_path;

        $new_version_info       = file_get_contents($update_path);
        $this->new_version_info = json_decode($new_version_info);

        if(isset($this->new_version_info->version) && $this->new_version_info->version > $current_version){
            add_filter ('pre_set_site_transient_update_plugins', array($this,'check_update'));
            add_filter('plugins_api',array($this,'check_info'),10,3);
        }
    }

    function check_update($transient){
        $obj = new stdClass();
        $obj->slug = $this->plugin_slug;
        $obj->new_version = $this->new_version_info->version;
        $obj->package = $this->new_version_info->download_path;
        $transient->response[$this->plugin_path] = $obj;
        return $transient;
    }

    function check_info($false, $action, $arg){
        if($arg->slug === $this->plugin_slug){
            $obj = new stdClass();
            $obj->requires = $this->new_version_info->requires;
            $obj->tested = $this->new_version_info->tested;
            $obj->last_updated = $this->new_version_info->last_updated;
            $obj->sections = (array)$this->new_version_info->sections;
            return $obj;
        }
        return false;
    }
}
?>