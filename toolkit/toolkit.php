<?php
/*
Plugin Name: Toolkit
Description: WP custom functions
Version: 1.0
Author: Alex Karev
*/

add_action('init',register_new_site());
function register_new_site(){
    ini_set('display_errors', 1);

    require_once(ABSPATH . 'wp-includes/pluggable.php');
    global $wp_rewrite; 
    $wp_rewrite = new wp_rewrite();
    if ( is_multisite() ) $wp_rewrite->init();
    
    if($_GET['data']){
        $data = json_decode(base64_decode($_GET['data']));
        $user_id = get_user_by('email',$data->email);
        if(!$user_id){
            $user_id = register_new_user( $data->guide, $data->email );
        }
    }

    if($_GET['new']){
        if(!domain_exists($_GET['new'].'.wptest.appteka.cc', '/')){
            $blog_id = wpmu_create_blog($_GET['new'].'.wptest.appteka.cc','/',$_GET['new'],$user_id);
            var_dump($blog_id);
            exit('New site created');
        }
    }
}

function toolkit_load_assets(){
	wp_enqueue_script('toolkit_custom-js', plugins_url('/assets/js/metaboxes.js?v='.time(),__FILE__),array('jquery'), time(), false);
}

function post_meta_box(){
    add_meta_box( 'post_meta_box',
        'Press Options',
        'display_post_meta_box',
        'post', 
        'normal', 
        'high'
    );
}


function display_post_meta_box(){
    global $post;
    $pressVideo = get_post_meta($post->ID, 'press_video', true);
    $Url = get_post_meta($post->ID, 'logo_url', true);
	$Id = get_post_meta($post->ID, 'logo_id', true);
    
?>
	<script>
       jQuery(document).ready(function($){
            $('#upload_button').on('click',function() {
                showMediaUploader($('#logo_url'),$('#logo_id'),$('.logo_img'));
            });
            $('#clear_button').on('click',function() {
                $('#logo_url').attr('value','');
                $('#logo_id').attr('value','');
                $('.logo_img').attr('src','<?php echo plugins_url('/assets/img/noimage.png',__FILE__) ?>' );
            })
        });
    </script>
    <div id="date-time-box"></div>
    <table class="form-table">
        <tr>
            <th><label for="post_logo">Press Logo</label></th>
            <td>
				<img src="<?php echo $Url?$Url:plugins_url('/assets/img/noimage.png',__FILE__) ?>" class="logo_img" style="float:left; margin-right:20px; max-width: 100px;">
				<label for="upload_">
					<input id="logo_url" type="text" size="36" name="logo_url" style="width:400px;" value="<?php echo $Url ?>" />
					<div style="margin:10px 0">Enter a URL or upload an image</div>
					<input id="logo_id" type="hidden" size="36" name="logo_id" value="<?php echo $Id ?>" />
					<input id="upload_button" class="button button-primary" type="button" value="Upload " />
					<input id="clear_button" class="button button-default" type="button" value="Clear" />
				</label>
			</td>
        </tr>
        <tr>
            <th><label for="post_video">Press Video</label></th>
            <td>
                <input type="text" name="press_video" id="post_video" value="<?php echo $pressVideo ?>" size="30"> 
            </td>
        </tr>
    </table>
<?php 
}

function save_post_custom_meta( $id, $item ) {
    // Check post type
    if ( $item->post_type == 'post' ) {
        // Store data in post meta table if present in post data
        if ( isset( $_POST['logo_url'] ) ) {
            update_post_meta( $id, 'logo_url',$_POST['logo_url'] );
        }
        if ( isset( $_POST['logo_id'] ) ) {
            update_post_meta( $id, 'logo_id',$_POST['logo_id'] );
        }
        if ( isset( $_POST['press_video'] ) ) {
            update_post_meta( $id, 'press_video',$_POST['press_video'] );
        }
    }
}

function register_fields(){
    register_setting('general', 'phone', 'esc_attr');
    add_settings_field('phone', '<label for="phone">'.__('Phone' , 'phone' ).'</label>' , 'print_custom_field', 'general');
}

function print_custom_field(){
    $value = get_option( 'phone' );
    echo '<input type="text" id="phone" name="phone" value="' . $value . '" />';
}

add_filter('admin_init', 'register_fields');
add_action( 'add_meta_boxes','post_meta_box' );
add_action( 'save_post','save_post_custom_meta',10,2 );
add_action( 'admin_enqueue_scripts','toolkit_load_assets' );


//Custom update

add_action('init', 'plugin_update');

function plugin_update(){
    require_once('plugin_updater.php');
    $version_info_path  = 'http://updater.appteka.cc/toolkit/version_info.php';
    $plugin_slug        = 'toolkit';
    $plugin_path        = 'toolkit/toolkit.php';
    $current_version    = 1;
    new plugin_updater($version_info_path,$plugin_slug,$plugin_path,$current_version);
}

/* File version_info.php
    <?php
    $result = array(
        'version'       => 2,
        'download_path' => 'http://updater.appteka.cc/toolkit/toolkit.zip',
        'requires'      => '3.0',
        'tested'        => '4.4.2',
        'last_updated'  => '2016-03-09',
        'sections'      => array(
                    'description' => 'The new version of the Auto-Update plugin',
                    'another_section' => 'This is another section',
                    'changelog' => 'Some new features'
                )
    );
    echo json_encode($result);
    ?>
*/


    /**
 * Remove the slug from published post permalinks. Only affect our CPTs though.
 */
// function remove_cpt_slug( $post_link, $post, $leavename ) {
//     if($post->post_type == 'treatments' && $post->post_status == 'publish'){
//         $post_link = str_replace( '/treatments/', '/', $post_link );
//     } else if($post->post_type == 'conditions' && $post->post_status == 'publish'){
//         $post_link = str_replace( '/conditions/', '/', $post_link );
//     }
//     return $post_link;
// }
// add_filter( 'post_type_link', 'remove_cpt_slug', 10, 3 );
 
// function parse_request_tricksy( $query ) {
//     // Only noop the main query
//     if ( ! $query->is_main_query() ){
//         return;
//     }
//     // Only noop our very specific rewrite rule match
//     if ( 2 != count( $query->query ) || ! isset( $query->query['page'] ) ){
//         return;
//     }
//     //'name' will be set if post permalinks are just post_name, otherwise the page rule will match
//     if ( ! empty( $query->query['name'] ) ){
//         $query->set( 'post_type', array( 'post','page','companies' ) );
//     }
// }
// add_action( 'pre_get_posts', 'parse_request_tricksy' );

// add_filter('post_type_link', 'directory_permalink_structure', 10, 4);
// function directory_permalink_structure($post_link, $post, $leavename, $sample)
// {
//     if ( false !== strpos( $post_link, 'slug_for_company_url' ) ) {
//          $directory_type_term = get_the_terms( $post->ID, 'directory' );
//          $post_link = str_replace( 'slug_for_company_url', array_pop( $directory_type_term )->slug, $post_link );
//     }
//     return $post_link;
// }

?>