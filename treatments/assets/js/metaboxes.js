jQuery(document).ready(function($){	
	var concernsList = $("#concerns_list").select2();
	$("#specialties_list").select2();
	$("#providers_list").select2();
	$("#locations_list").select2();
	$("#galleries_list").select2();

	$('#add_concern_button').on('click',function(){
		var newConcern = $('#add_concern_input').val();
		var addedConcerns = concernsList.val();
		
		if( $('#add_concern').is(":visible") && newConcern!='' && $('#concerns_list option:contains("'+newConcern+'")' ).length == 0 ){
			if( $('#concerns_list option[value="'+newConcern+'"]' ).length == 0){
				$('#concerns_list').append('<option value="'+newConcern+'">'+newConcern+'</option>');
			}
			if(addedConcerns!=null){
				addedConcerns.push(newConcern)
			} else {
				addedConcerns = newConcern;
			}
			concernsList.val(addedConcerns).trigger("change");
		} else {
			$('#add_concern').show();
			$('#add_concern_button').addClass('button-primary');
			$('#add_concern_button').val('Create New Concern');
		}
	});
});