<?php
/*
Plugin Name: Alex Karev Treatments
Description: Custom content types for themes developed by Alex Karev
Version: 1.0
Author: Alex Karev
*/

function treatments_load_assets(){
	wp_enqueue_style( 'treatments_metabox-styles', plugins_url('/assets/css/metaboxes.css?v='.time(),__FILE__));
	wp_enqueue_style( 'treatments_jquery-select2', plugins_url('/assets/css/select2.css?v='.time(),__FILE__));
	wp_enqueue_script( 'treatments_select2-js', plugins_url('/assets/js/select2.min.js?v='.time(),__FILE__), array('jquery'), time(), false);
	wp_enqueue_script('treatments_custom-js', plugins_url('/assets/js/metaboxes.js?v='.time(),__FILE__), 'jquery-ui-core', '1.0', true);
}

function create_pt_treatments() {
	register_post_type( 'treatments', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	array('labels' => array(
			'name' => __('Treatments'), /* This is the Title of the Group */
			'singular_name' => __('Treatment'), /* This is the individual type */
			'all_items' => __('All Treatments'), /* the all items menu item */
			'add_new' => __('Add New Treatment'), /* The add new menu item */
			'add_new_item' => __('Add New Treatment'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit Treatment'), /* Edit Display Title */
			'new_item' => __('New Treatment'), /* New Display Title */
			'view_item' => __('View Treatment'), /* View Display Title */
			'search_items' => __('Search Treatments'), /* Search Product Title */ 
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), 
			'description' => __( 'Treatments for '.get_bloginfo() ), /* Product Description */
			'public' => true,
			'menu_icon' => 'dashicons-shield',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'treatments', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'page',
			'hierarchical' => true,
			'supports' => array( 'title','post-options','editor','page-attributes', 'thumbnail', 'author',),
			'taxonomies' => array('post_tag'),
	 	) /* end of options */
	); /* end of register post type */
}


add_action( 'init', 'create_treatments_cat_tax' );

function create_treatments_cat_tax() {
	$labels = array(
			'name'              => _x( 'Treatments Сategories', 'taxonomy general name' ),
			'singular_name'     => _x( 'Treatments Сategory', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Treatments Сategories' ),
			'all_items'         => __( 'All Treatments Сategories' ),
			'parent_item'       => __( 'Parent Treatments Сategory' ),
			'parent_item_colon' => __( 'Parent Treatments Сategory:' ),
			'edit_item'         => __( 'Edit Treatments Сategory' ),
			'update_item'       => __( 'Update Treatments Сategory' ),
			'add_new_item'      => __( 'Add New Treatments Сategory' ),
			'new_item_name'     => __( 'New Treatments Сategory Name' ),
			'menu_name'         => __( 'Treatments Сategories' ),
		);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'treatments_category' ),
	);

	register_taxonomy( 'treatments_category', array( 'treatments' ), $args );
}

function treatments_meta_boxes(){
	add_meta_box( 'events_meta_box',
        'Treatment information',
        'display_treatments_meta_box',
        'treatments', 
        'normal', 
        'high'
    );
}

function display_treatments_meta_box(){
	global $post;
	$treatmentConcerns = get_post_meta($post->ID, 'treatment_concerns', true);
	$concerns = get_posts( array (
		'post_type'	=> 'concerns',
		'posts_per_page' => -1
	));
	$treatmentSpecialties = get_post_meta($post->ID, 'treatment_specialties', true);
	$specialties = get_posts( array (
		'post_type'	=> 'specialties',
		'posts_per_page' => -1
	));
	$treatmentProviders = get_post_meta($post->ID, 'treatment_providers', true);
	$providers = get_posts( array (
		'post_type'	=> 'providers',
		'posts_per_page' => -1
	));
	$treatmentLocations = get_post_meta($post->ID, 'treatment_locations', true);
	$locations = get_posts( array (
		'post_type'	=> 'location',
		'posts_per_page' => -1
	));
	$treatmentGalleries = get_post_meta($post->ID, 'treatment_galleries', true);
	$galleries = get_posts( array (
		'post_type'	=> 'galleries',
		'posts_per_page' => -1
	));

?>
	<table class="form-table">
		<?php if( post_type_exists('concerns') ): ?>
			<tr>
				<th><label for="concerns_list">Concerns</label></th>
				<td>
					<select name="treatment_concerns[]" id="concerns_list" multiple="multiple">
						<option value="">Select One</option>
						<?php foreach($concerns as $child ): ?>
							<option value="<?php echo $child->ID ?>", <?php echo $treatmentConcerns&&in_array($child->ID,$treatmentConcerns)?' selected="selected"':''?> ><?php echo $child->post_title ?></option>;
						<?php endforeach ?>
					</select>
					<br /><span class="description">The specific concerns that are treated by this particular treatment.</span>
					<div style="margin-top:20px">
						<div id="add_concern" style="display:none; margin-bottom:20px;">
							<input id="add_concern_input" type="text" class="large-text" placeholder="New Concern" />
						</div>
						<input id="add_concern_button" class="button" type="button" value="Add New Concern" />
					</div>
				</td>
			</tr>
		<?php endif ?>

		<?php if( post_type_exists('specialties') ): ?>
			<tr>
				<th><label for="specialties_list">Specialties</label></th>
				<td>
					<select name="treatment_specialties[]" id="specialties_list" multiple="multiple">
						<option value="">Select One</option>
						<?php foreach($specialties as $child ): ?>
							<option value="<?php echo $child->ID ?>", <?php echo $treatmentSpecialties&&in_array($child->ID,$treatmentSpecialties)?' selected="selected"':''?> ><?php echo $child->post_title ?></option>;
						<?php endforeach ?>
					</select>
					<br /><span class="description">The services that this staff member covers.</span>
				</td>
			</tr>
		<?php endif ?>

		<?php if( post_type_exists('providers') ): ?>
			<tr>
				<th><label for="providers_list">Providers</label></th>
				<td>
					<select name="treatment_providers[]" id="providers_list" multiple="multiple">
						<option value="">Select One</option>
						<?php foreach($providers as $child ): ?>
							<option value="<?php echo $child->ID ?>", <?php echo $treatmentProviders&&in_array($child->ID,$treatmentProviders)?' selected="selected"':''?> ><?php echo $child->post_title ?></option>;
						<?php endforeach ?>
					</select>
					<br /><span class="description"></span>
				</td>
			</tr>
		<?php endif ?>

		<?php if( post_type_exists('location') ): ?>
			<tr>
				<th><label for="locations_list">Locations</label></th>
				<td>
					<select name="treatment_locations[]" id="locations_list" multiple="multiple">
						<option value="">Select One</option>
						<?php foreach($locations as $child ): ?>
							<option value="<?php echo $child->ID ?>", <?php echo $treatmentLocations&&in_array($child->ID,$treatmentLocations)?' selected="selected"':''?> ><?php echo $child->post_title ?></option>;
						<?php endforeach ?>
					</select>
					<br /><span class="description"></span>
				</td>
			</tr>
		<?php endif ?>

		<?php if( post_type_exists('galleries') ): ?>
			<tr>
				<th><label for="galleries_list">Galleries</label></th>
				<td>
					<select name="treatment_galleries[]" id="galleries_list" multiple="multiple">
						<option value="">Select One</option>
						<?php foreach($galleries as $child ): ?>
							<option value="<?php echo $child->ID ?>", <?php echo $treatmentGalleries&&in_array($child->ID,$treatmentGalleries)?' selected="selected"':''?> ><?php echo $child->post_title ?></option>;
						<?php endforeach ?>
					</select>
					<br /><span class="description"></span>
				</td>
			</tr>
		<?php endif ?>
	</table>
<?php 
}


function save_treatments_custom_meta( $id, $item ) {
	if($item->post_type == 'treatments'){
		if (post_type_exists('concerns')) {
		    if (!empty($_POST['treatment_concerns'])) {
		        update_post_meta( $id, 'treatment_concerns',$_POST['treatment_concerns'] );
		        updateTreatmentLinkedResources($_POST['treatment_concerns'],$id,'concern_treatments');
		    } else {
		    	delete_post_meta($id, 'treatment_concerns');
		    }
		}
		if (post_type_exists('specialties')) {
		    if (!empty( $_POST['treatment_specialties'])) {
		        update_post_meta( $id, 'treatment_specialties',$_POST['treatment_specialties'] );
		    } else {
		    	delete_post_meta($id, 'treatment_specialties');
		    }
		}
		if (post_type_exists('providers')) {
		    if ( !empty( $_POST['treatment_providers'])) {
		        update_post_meta( $id, 'treatment_providers',$_POST['treatment_providers'] );
		        updateTreatmentLinkedResources($_POST['treatment_providers'],$id,'provider_treatments');
		    } else {
		    	delete_post_meta($id, 'treatment_providers');
		    }
		}
		if (post_type_exists('location')) {
		    if (!empty( $_POST['treatment_locations'])) {
		        update_post_meta( $id, 'treatment_locations',$_POST['treatment_locations'] );
		        updateTreatmentLinkedResources($_POST['treatment_locations'],$id,'location_treatments');
		    } else {
		    	delete_post_meta($id, 'treatment_locations');
		    }
		}
		if (post_type_exists('galleries')) {
	    	if (!empty( $_POST['treatment_galleries'])) {
		        update_post_meta( $id, 'treatment_galleries',$_POST['treatment_galleries'] );
		        updateTreatmentLinkedResources($_POST['treatment_galleries'],$id,'gallery_treatments');
		    } else {
		    	delete_post_meta($id, 'treatment_galleries');
		    }
		}
	}
}

function updateTreatmentLinkedResources($linkedIds,$currentId,$type){
    foreach ($linkedIds as $id) {
    	if( !is_numeric($id) && !empty($id) ){
    		$id = createNewConcern($id,$currentId);
    	}
    	$meta = get_post_meta($id,$type,true);
    	if(empty($meta) || !in_array($currentId,$meta)){
    		$meta[] = $currentId;
    		update_post_meta($id,$type,$meta);
    	}
    }
}

function createNewConcern($title,$currentId){
	$id = wp_insert_post(array(
		'post_title'	=> $title,
		'post_type'		=> 'concerns',
		'post_status'   => 'publish',
	));
	$meta = get_post_meta($currentId,'treatment_concerns',true);
	$meta[] = $id;
	update_post_meta($currentId,'treatment_concerns',$meta);
	return $id;
}

function treatments_shortcode( $atts ){ 
	$args = array(
		'post_type'       	=> 'treatments',
		'posts_per_page'  	=> -1,
	);
	if($_GET['category']){
		$categories = explode(';', $_GET['category']);
		foreach ($categories as $item) {
			$terms[] = $item;
		}
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'treatments_category',
				'field'    => 'slug',
				'terms'    => $terms,
			),
		);
	}
	new WP_Query( $args );
	$loop = new WP_Query( $args ); ?>
	<?php if ( $loop->have_posts() ):  ?>
		<ul class=“elements-list”>
			<?php while ( $loop->have_posts() ) : $loop->the_post() ?>
				<li><a href="<?php echo the_permalink(); ?>"><?php the_title() ?></a></li>
			<?php endwhile ?>
		</ul>
	<?php else: ?>
		'No Treatments found'
	<?php endif ?>
<?php }

function related_concerns_shortcode(){
	$postId = get_the_ID();
	$treatments = get_post_meta($postId, 'treatment_concerns', true);
	if(!empty($treatments)): ?>
		<ul>
			<?php foreach ($treatments as $id): ?>
				<?php $item = get_post($id) ?>
				<li><a href="<?php echo site_url().'/'.$item->post_name?>"><?php echo $item->post_title ?></a></li>
			<?php endforeach ?>
		</ul>
	<?php endif ?>
<?php }


add_action( 'admin_enqueue_scripts','treatments_load_assets' );
add_action( 'init','create_pt_treatments' );
add_action( 'add_meta_boxes','treatments_meta_boxes' );
add_action( 'save_post','save_treatments_custom_meta',10,2 );
add_shortcode( 'treatments', 'treatments_shortcode' );
add_shortcode( 'related_concerns', 'related_concerns_shortcode' );

if(!function_exists('related_galleries_shortcode')){
	function related_galleries_shortcode(){
		$galleries = get_post_meta(get_the_ID(),'treatment_galleries'); ?>
		<?php if(isset($galleries[0]) && !empty($galleries[0])): ?>
			<div vlass="row">
				<?php foreach ($galleries[0] as $val): ?>
					<?php $gallery = get_post($val); ?>
					<div class="col-md-4">
						<a href="<?php echo site_url() ?>/galleries/<?php echo $gallery->post_name ?>" class="btn btn-blue"><?php echo $gallery->post_title ?></a>
					</div>
				<?php endforeach ?>
			</div>
		<?php endif;
	}
	add_shortcode( 'related_galleries', 'related_galleries_shortcode' );
}


