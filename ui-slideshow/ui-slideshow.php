<?php
/*
Plugin Name: UI slideshow
Description: Custom content types slick slider
Version: 1.0
Author: Alex Karev
*/

function ui_slideshow_load_assets(){
    global $post;

    wp_enqueue_media( array(
        'post' => $post->ID,
    ) );

	wp_enqueue_style( 'ui-slideshow-styles', plugins_url('/assets/css/ui-slideshow-admin.css?v='.time(),__FILE__));
	wp_enqueue_script('ui-slideshow_sortable', plugins_url('/assets/js/sortable.min.js?v='.time(),__FILE__), '', '1.0', false);

    // подключаем IRIS
    wp_enqueue_script( 'wp-color-picker' );
    wp_enqueue_style( 'wp-color-picker' );
}

function ui_slideshow_front_assets(){
	wp_enqueue_script( 'ui_slideshow_slick', plugins_url('/assets/js/slick.min.js?v='.time(),__FILE__), array('jquery'), time(), true);
	wp_enqueue_style( 'slick', plugins_url('/assets/css/slick.css?v='.time(),__FILE__));
    wp_enqueue_style( 'slick-theme', plugins_url('/assets/css/slick-theme.css?v='.time(),__FILE__));
    wp_enqueue_style( 'ui-slideshow-styles', plugins_url('/assets/css/ui-slideshow.css?v='.time(),__FILE__));
}

add_action( 'admin_enqueue_scripts','ui_slideshow_load_assets' );
add_action( 'wp_enqueue_scripts','ui_slideshow_front_assets' );


add_action( 'wp_footer', 'js_lib_init', 100 );

function js_lib_init(){
    ?>
    <script>

        jQuery('.slideshows').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
        });


        if(jQuery('.slideshows .slick-active').find('video').length > 0){
            jQuery('.slideshows .slick-active').find('video').get(0).play();
        }


        jQuery('.slideshows').on('afterChange', function(event, slick, currentSlide, nextSlide){
            jQuery('.slideshows .slick-slide video').each(function() {
                    if(jQuery(this).length > 0){
                        jQuery(this).get(0).pause();
                    }
            });

            if(jQuery('.slideshows .slick-active').find('video').length > 0){
                jQuery('.slideshows .slick-active').find('video').get(0).play();
            }
        });

    </script>
    <?php
}


function create_pt_ui_slideshow() {
	register_post_type( 'ui_slideshows', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	array('labels' => array(
			'name' => __('Slideshows'), /* This is the Title of the Group */
			'singular_name' => __('Slideshows'), /* This is the individual type */
			'all_items' => __('All slideshows'), /* the all items menu item */
			'add_new' => __('Add New Slideshow'), /* The add new menu item */
			'add_new_item' => __('Add New Slideshow'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit Slideshow'), /* Edit Display Title */
			'new_item' => __('New Slideshow'), /* New Display Title */
			'view_item' => __('View Slideshow'), /* View Display Title */
			'search_items' => __('Search ui_slideshows'), /* Search Product Title */
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), 
			'description' => __( 'ui_slideshows for '.get_bloginfo() ), /* Product Description */
			'public' => true,
			'menu_icon' => 'dashicons-exerpt-view',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'sliders', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
            'supports' => array( 'title','post-options','editor'),
//			'supports' => array( 'title','post-options','editor','thumbnail'),
			//'taxonomies' => array( 'post_tag' ),

	 	) /* end of options */
	); /* end of register post type */
}

add_action( 'init','create_pt_ui_slideshow' );

add_theme_support( 'post-thumbnails' ); 

add_action( 'init', 'create_slides_tax' );

function create_slides_tax() {
	$args = array(
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		//'rewrite'           => array( 'slug' => 'projects_category' ),
	);
	register_taxonomy( 'slides_category', array( 'ui_slideshows' ), $args );

	$args = array(
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
	);
	register_taxonomy( 'slides_tag', array( 'ui_slideshows' ), $args );
}

function ui_slideshows_meta_boxes(){
	add_meta_box( 'ui_slideshows_meta_box',
        'Sliders content',
        'display_ui_slideshows_meta_box',
        'ui_slideshows',
        'normal', 
        'high'
    );
}

add_action( 'add_meta_boxes','ui_slideshows_meta_boxes' );


function display_ui_slideshows_meta_box($post){
	$project_image_urls = esc_html( get_post_meta( $post->ID, 'project_image_urls', true ) );
    $project_image_id 	= esc_html( get_post_meta( $post->ID, 'project_image_id', true ) );
    $project_image_title= esc_html( get_post_meta( $post->ID, 'project_image_title', true ) );
    $project_image_desc = esc_html( get_post_meta( $post->ID, 'project_image_desc', true ) );

    $project_title_btn1 = esc_html( get_post_meta( $post->ID, 'project_title_btn1', true ) );
    $project_url_btn1 = esc_html( get_post_meta( $post->ID, 'project_url_btn1', true ) );
    $project_title_btn2 = esc_html( get_post_meta( $post->ID, 'project_title_btn2', true ) );
    $project_url_btn2 = esc_html( get_post_meta( $post->ID, 'project_url_btn2', true ) );
    $project_title_btn3 = esc_html( get_post_meta( $post->ID, 'project_title_btn3', true ) );
    $project_url_btn3 = esc_html( get_post_meta( $post->ID, 'project_url_btn3', true ) );

    $project_color = esc_html( get_post_meta( $post->ID, 'project_color', true ) );

    $project_image_urls = json_decode(htmlspecialchars_decode($project_image_urls));
    $project_image_id 	= json_decode(htmlspecialchars_decode($project_image_id));
    $project_image_title= json_decode(htmlspecialchars_decode($project_image_title));
    $project_image_desc = json_decode(htmlspecialchars_decode($project_image_desc));
    $project_title_btn1 = json_decode(htmlspecialchars_decode($project_title_btn1));
    $project_url_btn1 = json_decode(htmlspecialchars_decode($project_url_btn1));
    $project_title_btn2 = json_decode(htmlspecialchars_decode($project_title_btn2));
    $project_url_btn2 = json_decode(htmlspecialchars_decode($project_url_btn2));
    $project_title_btn3 = json_decode(htmlspecialchars_decode($project_title_btn3));
    $project_url_btn3 = json_decode(htmlspecialchars_decode($project_url_btn3));

    $project_color = json_decode(htmlspecialchars_decode($project_color));
    ?>
    <script>
    	function showMediaUploader(element){
		    var mediaUploader;
		    // If the uploader object has already been created, reopen the dialog
		     if (mediaUploader) {
		      mediaUploader.open();
		      return;
		    }
		    // Extend the wp.media object
		    mediaUploader = wp.media.frames.file_frame = wp.media(
		      {
		        title: 'Choose Image',
		        button: {
		        text: 'Choose Image'
		      }, 
		      multiple: true 
		    });
		    mediaUploader.on('select', function() {
		      selection = mediaUploader.state().get('selection')
		      selection.map( function( attachment ) {
		      	attachment = attachment.toJSON()
                block_html = jQuery.parseHTML( '<?php include( plugin_dir_path( __FILE__ ).'/image-block.php') ?>' )

                  if ((attachment.mime == 'image/jpeg') || (attachment.mime == 'image/png') || (attachment.mime == 'image/gif') ){

                      jQuery(block_html).find('.wrong-format').hide();
                          jQuery(block_html).find('.project_images_list_item').append('<img class="project_image" src="'+attachment.url+'">');
                      jQuery(block_html).find('.project_image_url').attr('value',attachment.url)
                      jQuery(block_html).find('.project_image_id').attr('value',attachment.id)
                      element.prepend(block_html)
                  }


                  if ((attachment.mime == 'video/mpeg') || (attachment.mime == 'video/mp4') || (attachment.mime == 'video/quicktime') ){

                      jQuery(block_html).find('.wrong-format').hide();
                      jQuery(block_html).find('.project_images_list_item').append('<video class="item-video" playsinline ' +
                          'autoplay muted loop> <source src="'+attachment.url+'" type="video/webm"> <source src="'+attachment.url+'" type="video/mp4"> </video>');
                      jQuery(block_html).find('.project_image_url').attr('value',attachment.url)
                      jQuery(block_html).find('.project_image_id').attr('value',attachment.id)
                      element.prepend(block_html)
                  }

		       });
		    });
		    // Open the uploader dialog
		    mediaUploader.open();
		};

       jQuery(document).ready(function($){
            $('#add_Project_image').on('click',function(){               
                showMediaUploader($('#project_images_list'));
            });
            
            $('body').on('click','.remove_Project_image_ico',function(){
                $(this).parent().parent().remove();
            })

           // List with handle
           Sortable.create(project_images_list, {
               handle: '.project_images_list_item',
               animation: 150
           });



            $('#slider_background_color').wpColorPicker();

        });
    </script>

    <span>Shortcode [ui_slideshows id="<?php echo $post->ID ?>"]</span>
    <input id="add_Project_image" type="button" value="Add Project image" style="margin:20px 10px; display:block">

    <input id="slider_background_color" name="project_color" type="text" value="<?php echo isset($project_color)?$project_color:"" ?>">

    <div id="project_images_list" class="list-group">
	    <?php  
	    if(count($project_image_urls)):?>
	    	
		        <?php foreach ($project_image_urls as $key => $Project_image_url): ?>
		            <?php if(!empty($Project_image_url) && isset($project_image_id[$key])): ?>
		            	<?php include( plugin_dir_path( __FILE__ ).'/image-block.php') ?>
		        	<?php endif ?>
		        <?php endforeach ?>
	        </div>
	    <?php endif ?>
	</div>
<?php }

function display_mime_type ($image_id){

    $type = get_post_mime_type($image_id);
    $url = wp_get_attachment_url( $image_id );

    $str = <<<EOT
<video class="item-video" playsinline autoplay muted loop>
        <source src="$url" type="video/webm">
        <source src="$url" type="video/mp4">
</video>
EOT;

    switch ($type) {
        case 'image/jpeg':
        case 'image/png':
        case 'image/gif':
            return '<img class="project_image" src="'.wp_get_attachment_image_src( $image_id, 'full' )[0].'" >'; break;
        case 'video/mpeg':
        case 'video/mp4':
        case 'video/quicktime':
            return $str; break;

        default:
            return '<div class="wrong-format"> Wrong format</div>'; break;
    }
}

function save_ui_slideshows_custom_meta( $id, $item ) {
	if( $item->post_type == 'ui_slideshows' ){
        delete_post_meta($id,'project_image_urls');
        delete_post_meta($id,'project_image_id');
        delete_post_meta($id,'project_image_title');
        delete_post_meta($id,'project_image_desc');
        delete_post_meta($id,'project_title_btn1');
        delete_post_meta($id,'project_url_btn1');
        delete_post_meta($id,'project_title_btn2');
        delete_post_meta($id,'project_url_btn2');
        delete_post_meta($id,'project_title_btn3');
        delete_post_meta($id,'project_url_btn3');
        delete_post_meta($id,'project_color');
        if ( isset( $_POST['project_image_urls'] ) && !empty($_POST['project_image_urls']) ) {
            update_post_meta( $id, 'project_image_urls', json_encode($_POST['project_image_urls']) );
            update_post_meta( $id, 'project_image_id', json_encode($_POST['project_image_id']) );
            update_post_meta( $id, 'project_image_title', json_encode($_POST['project_image_title']) );
            update_post_meta( $id, 'project_image_desc', json_encode($_POST['project_image_desc']) );
            update_post_meta( $id, 'project_title_btn1', json_encode($_POST['project_title_btn1']) );
            update_post_meta( $id, 'project_url_btn1', json_encode($_POST['project_url_btn1']) );
            update_post_meta( $id, 'project_title_btn2', json_encode($_POST['project_title_btn2']) );
            update_post_meta( $id, 'project_url_btn2', json_encode($_POST['project_url_btn2']) );
            update_post_meta( $id, 'project_title_btn3', json_encode($_POST['project_title_btn3']) );
            update_post_meta( $id, 'project_url_btn3', json_encode($_POST['project_url_btn3']) );
            update_post_meta( $id, 'project_color', json_encode($_POST['project_color']) );
        }
	}
}

add_action( 'save_post','save_ui_slideshows_custom_meta',10,2 );


function updatetad_projectsLinkedResources($linkedIds,$currentId,$type){
    foreach ($linkedIds as $id) {
    	$meta = get_post_meta($id,$type,true);
    	if(empty($meta) || !in_array($currentId,$meta)){
    		$meta[] = $currentId;
    		update_post_meta($id,$type,$meta);
    	}
    }
}

function ui_slideshows_shortcode( $atts ){
	ob_start();
	if(isset($atts['id']) && !empty($atts['id'])){
		$images_ids 	= get_post_meta($atts['id'], 'project_image_id', true);
		$images_titles 	= get_post_meta($atts['id'], 'project_image_title', true);
		$images_descs 	= get_post_meta($atts['id'], 'project_image_desc', true);
        $images_title_btn1 	= get_post_meta($atts['id'], 'project_title_btn1', true);
        $images_url_btn1 	= get_post_meta($atts['id'], 'project_url_btn1', true);
        $images_title_btn2 	= get_post_meta($atts['id'], 'project_title_btn2', true);
        $images_url_btn2 	= get_post_meta($atts['id'], 'project_url_btn2', true);
        $images_title_btn3 	= get_post_meta($atts['id'], 'project_title_btn3', true);
        $images_url_btn3 	= get_post_meta($atts['id'], 'project_url_btn3', true);

        $project_color 	= get_post_meta($atts['id'], 'project_color', true);

        $images_ids 	= json_decode($images_ids);
		$images_titles 	= json_decode($images_titles);
		$images_descs 	= json_decode($images_descs);
        $images_title_btn1 	= json_decode($images_title_btn1);
        $images_url_btn1 	= json_decode($images_url_btn1);
        $images_title_btn2 	= json_decode($images_title_btn2);
        $images_url_btn2 	= json_decode($images_url_btn2);
        $images_title_btn3 	= json_decode($images_title_btn3);
        $images_url_btn3 	= json_decode($images_url_btn3);

        $project_color 	= json_decode($project_color);

        $project_image_urls = esc_html( get_post_meta( $atts['id'], 'project_image_urls', true ) );
        $project_image_urls = json_decode(htmlspecialchars_decode($project_image_urls));
	} ?>
	<?php if(count($images_ids)): ?>
		<div class="row slideshows" style="background-color: <?php echo (isset($project_color))? $project_color:'transparent'?>">
			<?php foreach ($images_ids as $key=>$image_id): ?>
				<div class="slide-item">
					<div class="gallery-box">
                        <?php echo display_mime_type($image_id)?>

                        <div class="sl_button_cont">
                            <?php if($images_url_btn1[$key]){?>
                                <a <?php echo (isset($project_color))? 'style="background-color:'.$project_color.'"':'';?> class="sl-button btn1" href="<?php echo $images_url_btn1[$key]?$images_url_btn1[$key]:''; ?>" > <?php echo $images_title_btn1[$key]?$images_title_btn1[$key]:''; ?> </a>
                            <?php }?>

                            <?php if($images_url_btn2[$key]){?>
                                <a <?php echo (isset($project_color))? 'style="background-color:'.$project_color.'"':'';?> class="sl-button btn2" href="<?php echo $images_url_btn2[$key]?$images_url_btn2[$key]:''; ?>" > <?php echo $images_title_btn2[$key]?$images_title_btn2[$key]:''; ?> </a>
                            <?php }?>

                            <?php if($images_url_btn3[$key]){?>
                                <a <?php echo (isset($project_color))? 'style="background-color:'.$project_color.'"':'';?> class="sl-button btn3" href="<?php echo $images_url_btn3[$key]?$images_url_btn3[$key]:''; ?>" > <?php echo $images_title_btn3[$key]?$images_title_btn3[$key]:''; ?> </a>
                            <?php }?>
                        </div>

                        <div class="caption"  <?php echo (isset($project_color))? 'style="background-color:'.$project_color.'"':'';?>">
                            <div class="title"><?php echo isset($images_titles[$key])?$images_titles[$key]:'' ?></div>
                            <div class="desc"><?php echo isset($images_descs[$key])?$images_descs[$key]:'' ?></div>
                        </div>
					</div>
				</div>
			<?php endforeach ?>
		</div>
 	<?php endif ?>
 	<?php return ob_get_clean(); ?>
<?php }


add_shortcode( 'ui_slideshows', 'ui_slideshows_shortcode' );
