<?php
/*
Plugin Name: SEO for woocommerce
Description: Add H1 and description to any woocommerce pages
Version: 1.0
Author: Alex Karev
*/

function create_pt_woocom_seo() {
	register_post_type( 'woocom_seo', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	array('labels' => array(
			'name' => __('Woocom seo'), /* This is the Title of the Group */
			'singular_name' => __('Woocom seo'), /* This is the individual type */
			'all_items' => __('All woocom seo'), /* the all items menu item */
			'add_new' => __('Add New woocom seo'), /* The add new menu item */
			'add_new_item' => __('Add New woocom seo'), /* Add New Display Title */
			'edit' => __( 'Edit' ), /* Edit Dialog */
			'edit_item' => __('Edit woocom seo'), /* Edit Display Title */
			'new_item' => __('New woocom seo'), /* New Display Title */
			'view_item' => __('View woocom seo'), /* View Display Title */
			'search_items' => __('Search woocom seo'), /* Search Product Title */ 
			'not_found' =>  __('Nothing found in the Database.'), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __('Nothing found in Trash'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), 
			'description' => __( 'woocom seo for '.get_bloginfo() ), /* Product Description */
			'public' => true,
			'menu_icon' => 'dashicons-plus',
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite'	=> array( 'slug' => 'woocom seo', 'with_front' => true ), /* you can specify its url slug */
			'has_archive' => true, /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title','post-options','page-attributes', 'thumbnail', 'author',),
			'taxonomies' => array( 'post_tag' ),

	 	) /* end of options */
	); /* end of register post type */
}
add_action( 'init','create_pt_woocom_seo' );


function woocom_seo_meta_boxes(){
	add_meta_box( 'woocom_seo_meta_box',
        'Woocom seo settings',
        'display_woocom_seo_meta_box',
        'woocom_seo', 
        'normal', 
        'high'
    );
}
add_action( 'add_meta_boxes','woocom_seo_meta_boxes' );
add_action( 'save_post','save_concerns_custom_meta',10,2 );


function display_woocom_seo_meta_box(){
	global $post;
	$woo_com_h1 = get_post_meta($post->ID, 'woocom_seo_h1', true);
	$woo_com_desc = get_post_meta($post->ID, 'woo_com_desc', true);
?>
	<table class="form-table">
		<tr>
			<th><label for="galleries_list">H1 text</label></th>
			<td>
				<input type="text" name="h1" style="width:100%" value="<?php echo $woo_com_h1 ?>">
			</td>
		</tr>
		<tr>
			<th><label for="galleries_list">Description</label></th>
			<td>
				<textarea name="woo_com_desc" style="width:100%; min-height: 200px"><?php echo $woo_com_desc ?></textarea>
			</td>
		</tr>
	</table>
<?php 
}

function save_concerns_custom_meta( $id, $item ) {
	if( $item->post_type == 'woocom_seo' ){	
    	if(!empty($_POST['woocom_seo_h1'])) {
        	update_post_meta( $id, 'woocom_seo_h1',$_POST['woocom_seo_h1'] );
		}
	}
	if( $item->post_type == 'woocom_seo' ){	
    	if(!empty($_POST['woo_com_desc'])) {
        	update_post_meta( $id, 'woo_com_desc',$_POST['woo_com_desc'] );
		}
	}
}


add_action( 'woocommerce_after_main_content','add_desc_on_page' );
function add_desc_on_page(){
	$current_url =  'http://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
	$seo_post_type = get_page_by_title( $current_url, 'OBJECT', 'woocom_seo' );
	if(!empty($seo_post_type)){
		echo  get_post_meta($seo_post_type->ID,'woo_com_desc', true);
	}
}

add_filter('woocommerce_page_title','add_h1_on_page');
function add_h1_on_page($title){
	$current_url =  'http://'.$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
	$seo_post_type = get_page_by_title( $current_url, 'OBJECT', 'woocom_seo' );
	if(!empty($seo_post_type)){
		return  get_post_meta($seo_post_type->ID,'woocom_seo_h1', true);
	}
	return $title;
}