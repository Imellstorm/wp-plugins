<?php
/*
Plugin Name: Woocommerce Sku Search
Description: Woocommerce sku product search
Version: 1.0
Author: Alex Karev
*/

add_action('pre_get_posts','search_filter');
function search_filter($query) { 
  if (!is_admin() && $query->is_main_query() && $query->is_search && $query->query['post_type']=='product') {
    global $wp_query, $wp_the_query;
    $query1 = new WP_Query(
        array(
            'fields' => 'ids',
            'post_type'     => 'product',
            'meta_query'    =>  array(
                array(
                    'key'       => '_sku',
                    'value'     => $query->query['s'],
                    'compare'   => "LIKE"
                )
            )
        )
    );
    $query2 = new WP_Query(
        array(
            'fields' => 'ids',
            'post_type' => 'product',
            's'         => $query->query['s'],
        )
    );

    $wp_query->posts = array_merge( $query1->posts, $query2->posts );
    //populate post_count count for the loop to work correctly
    $wp_query->post_count = $query1->post_count + $query2->post_count;

    storeSearchRequests($query->query['s']);
  }
}

function storeSearchRequests($search){
    $str = date('Y-m-d H:i:s')." query: ".$search."\n";
    $file = get_template_directory().'/search_queries.txt';
    file_put_contents($file, $str, FILE_APPEND | LOCK_EX);
}
?>